module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
    "host": "51.89.16.171",
    "username": "root",
    "password": "diamondR9",
      // pem: './path/to/pem'
      // or neither for authenticate from ssh-agent
    }
  },

  app: {
    // TODO: change app name and path
    name: 'piterbeer.com',
    path: '../',
    volumes: {
      '/var/www/img.piterbeer.com/html/img/uploads': '/uploads'
    },

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://piterbeer.com',
      MONGO_URL: 'mongodb://51.89.16.171:37017/piterbeer',
      // MONGO_URL: 'mongodb://piterbeer:piterbeer12@ds161894.mlab.com:61894/piterbeer',
      // MONGO_URL: 'mongodb://piterbeer:piterbeer12@ds161894.mlab.com:61894/piterbeer',
      // MONGO_OPLOG_URL: 'mongodb://mongodb/local',
      PORT: 3010
    },

    docker: {
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      // image: 'abernix/meteord:node-8.4.0-base',
      image: 'abernix/meteord:node-8.11.2-base',
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  },

  // mongo: {
  //   version: '3.4.1',
  //   servers: {
  //     one: {}
  //   }
  // },

  // (Optional)
  // Use the proxy to setup ssl or to route requests to the correct
  // app when there are several apps

  // proxy: {
  //   domains: 'piterbeer.com,www.piterbeer.com',

  //   ssl: {
  //     // Enable Let's Encrypt
  //     letsEncryptEmail: 'piter-beer@rucompromat.com'
  //   }
  // }
};
