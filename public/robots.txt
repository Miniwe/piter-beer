# Allow crawling of all content
User-agent: *
Disallow: /admin/
Allow: /

User-agent: Yandex
Host: piterbeer.com

Sitemap: http://piterbeer.com/sitemap.xml
