const basePath = '';
// const basePath = window.location.origin;

(function () {
    CKEDITOR.plugins.addExternal('placeholder_select', basePath + '/ckeditor/plugins/placeholder_select/', 'plugin.js');
    CKEDITOR.plugins.addExternal('pagebreak', basePath + '/ckeditor/plugins/pagebreak/', 'plugin.js');
    CKEDITOR.plugins.addExternal('youtube', basePath + '/ckeditor/plugins/youtube/', 'plugin.js');
})();
