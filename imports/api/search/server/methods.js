import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { check } from 'meteor/check'
import { rateLimit } from 'meteor/mcore/utils'
import Brands from '../../brands'
import { ListServices as BeersListServices } from '../../beers/services'
import { ListServices as LocationsListServices } from '../../locations/services'
import { ListServices as BrandsListServices } from '../../brands/services'

const getBeers = (query, params) => {
    const result = {
        beers: BeersListServices.list(query, {
            ...params,
            isActive: true
        }).map((item) => {
            const brand = Brands.findOne(item.brandId)
            return {
                ...item,
                brandTitle: brand ? brand.title : ''
            }
        })
    }
    return result
}

export const searchGlobal = new ValidatedMethod({
    name: 'search.global',
    validate({
        query = {},
        params = {}
    }) {
        check(query, Object)
        check(params, Object)
    },
    run({
        query = {},
        params = {}
    }) {
        const result = {
            ...getBeers(query, params),
            locations: LocationsListServices.list(query, {
                ...params,
                isActive: true
            }),
            brands: BrandsListServices.list(query, {
                ...params,
                isActive: true
            }),
        }
        return result
    }
})

export const searchBeers = new ValidatedMethod({
    name: 'search.beers',
    validate({
        query = {},
        params = {}
    }) {
        check(query, Object)
        check(params, Object)
    },
    run({
        query = {},
        params = {}
    }) {
        return getBeers(query, params)
    }
})

export const searchLocations = new ValidatedMethod({
    name: 'search.locations',
    validate({
        query = {},
        params = {}
    }) {
        check(query, Object)
        check(params, Object)
    },
    run({
        query = {},
        params = {}
    }) {
        const result = {
            locations: LocationsListServices.list(query, params)
        }
        return result
    }
})

export const searchBrands = new ValidatedMethod({
    name: 'search.brands',
    validate({
        query = {},
        params = {}
    }) {
        check(query, Object)
        check(params, Object)
    },
    run({
        query = {},
        params = {}
    }) {
        const result = {
            brands: BrandsListServices.list(query, params)
        }
        return result
    }
})

rateLimit({
    methods: [
        searchGlobal,
        searchBeers,
        searchBrands,
        searchLocations,
    ],
    limit: 5,
    timeRange: 1000,
})
