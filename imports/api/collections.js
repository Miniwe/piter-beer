export { default as attributes } from 'meteor/mcore/api/attributes'
export { default as users } from 'meteor/mcore/api/users'
export { default as rights } from 'meteor/mcore/api/rights'
export { default as pages } from 'meteor/mcore/api/pages'
export { default as roles } from 'meteor/mcore/api/roles'
export { default as texts } from 'meteor/mcore/api/texts'

export { default as towns } from './towns'
export { default as news } from './news'
export { default as beers } from './beers'
export { default as locations } from './locations'
export { default as brands } from './brands'
export { default as tags } from './tags'
