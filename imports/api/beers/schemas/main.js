import SimpleSchema from 'simpl-schema'
import uniforms from 'uniforms-semantic'
import { slugify } from 'meteor/mcore/utils'
import SelectField from 'meteor/mcore/ui/uniforms/Select'
import UploadField from 'meteor/mcore/ui/uniforms/Upload'
import AttributeField from 'meteor/mcore/ui/uniforms/Attribute'
import BaseMainSchema from 'meteor/mcore/api/collection/schemas/main'
import AttributeSchema from 'meteor/mcore/api/attributes/schemas/attribute'
import { ListServices } from '../services'
import Brands from '/imports/api/brands'
import Locations from '/imports/api/locations'

const MainSchema = BaseMainSchema
    .pick(
        '_id',
        'title',
        'description'
    )
    .extend({
        alias: {
            type: String,
            label: 'Alias',
            optional: true,
            unique: 'true',
            autoValue() {
                const { value } = this
                const title = this.field('title').value
                if (title) {
                    return ListServices.getAlias(slugify(title))
                }
                return value
            },
        },
        brandId: {
            type: String,
            label: 'Brand',
            regEx: SimpleSchema.RegEx.Id,
            optional: true,
            uniforms: {
                component: SelectField,
                required: false,
                multiple: false,
                search: 'brands.staticList',
                allowAdditions: false,
                placeholder: 'Select brand',
                options: () => Brands.ids(),
                transformMultiple(id) {
                    const item = Brands.findOne(id)
                    const res = item ? {
                        value: item._id,
                        text: item.title,
                    } : {
                        value: id,
                        text: id,
                    }
                    return res
                }
            },
        },
        locations: {
            type: Array,
            label: 'Locations With Prices',
            optional: true,
            defaultValue: [],
            uniforms: {
                className: 'ui secondary segment',
            }
        },
        'locations.$': {
            type: Object,
            uniforms: {
                className: 'inline-grouped',
            }
        },
        'locations.$.locationId': {
            type: String,
            label: 'Location',
            regEx: SimpleSchema.RegEx.Id,
            uniforms: {
                className: 'twelve wide',
                component: SelectField,
                required: false,
                multiple: false,
                search: 'locations.staticList',
                allowAdditions: false,
                placeholder: 'Select location',
                options: () => Locations.ids(),
                transformMultiple(id) {
                    const item = Locations.findOne(id)
                    const res = item ? {
                        value: item._id,
                        text: item.title,
                    } : {
                        value: id,
                        text: id,
                    }
                    return res
                }
            }
        },
        'locations.$.price': {
            type: Number,
            defaultValue: 0,
            uniforms: {
                className: 'four wide',
            }
        },
        attributes: {
            type: Array,
            label: 'Attributes',
            optional: true,
            uniforms: {
                className: 'ui secondary segment',
            },

        },
        'attributes.$': {
            type: AttributeSchema,
            uniforms: {
                component: AttributeField,
            },
        },
        order: {
            type: Object,
            optional: true,
            uniforms: () => null
        },
        'order.price': {
            type: Number,
            optional: true,
            defaultValue: 0
        },
        imageUrl: {
            type: String,
            optional: true
        },
        images: {
            type: Array,
            label: 'Image',
            maxCount: 10,
            uniforms: {
                component: UploadField,
                viewMode: 'card',
                placeholder: 'Image',
                fileLocator: 'beers.images',
                multiple: true,
                accept: 'image/*',
            },
            optional: true,
        },
        'images.$': {
            type: String,
        },
        isComments: {
            type: Boolean,
            label: 'Is Comments',
            defaultValue: true,
        },
    }).extend(BaseMainSchema.pick(
        'isActive',
        'createdAt',
        'updatedAt'
    ))


export default MainSchema
