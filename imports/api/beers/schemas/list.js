import uniforms from 'uniforms-semantic'
import ListText from 'meteor/mcore/ui/uniforms/ListText'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'
import BaseListSchema from 'meteor/mcore/api/collection/schemas/list'

const ListSchema = BaseListSchema
    .pick('title', 'isActive')
    .extend({
        order: {
            type: Object,
            label: 'Price',
            uniforms: {
                component: ListText,
                filter: false,
                sort: true,
                width: '3',
                transform: (value = {}) => {
                    return value.price
                }
            }
        },
        'order.price': {
            type: Number,
            uniforms: () => null
        }
        // attributes: {
        //     type: Array,
        //     label: 'Price',
        //     uniforms: {
        //         component: ListText,
        //         filter: false,
        //         sort: false,
        //         width: '3',
        //         transform: (value) => {
        //             const priceAttr = AttributesListServices.getPriceAttr()
        //             if (priceAttr) {
        //                 const attr = value.find(({
        //                     attributeId
        //                 }) => attributeId === priceAttr._id)
        //                 return attr ? attr.attributeValue : 0
        //             }
        //             return 0
        //         }
        //     }
        // },
        // 'attributes.$': {
        //     type: Object,
        //     uniforms: () => null
        // }
    })

export default ListSchema
