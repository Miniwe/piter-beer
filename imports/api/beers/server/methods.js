import { prefix } from '..'
import CollectionMethods from 'meteor/mcore/api/collection/server/methods'
import {
    ListServices, ValidationServices,
} from '../services'

const Methods = new CollectionMethods({
    prefix
});

((...methodsList) => {
    methodsList.forEach((method) => {
        Methods.register(method, {
            validate: ValidationServices[method].bind(ValidationServices),
            run: ListServices[method].bind(ListServices)
        })
    })
})('upsert', 'remove', 'list', 'staticList', 'setActive', 'setInactive', 'removeSelected')
