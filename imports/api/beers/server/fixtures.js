import BaseCollectionFixtures from 'meteor/mcore/api/collection/server/fixtures'
import { faker } from 'meteor/practicalmeteor:faker'
import sampleSize from 'lodash/sampleSize'
import Collection from '..'
import Brands from '../../brands'
import Locations from '../../locations'
import Attributes from 'meteor/mcore/api/attributes'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'

const itemsGroup = ['abstract', 'animals', 'business', 'cats', 'city', 'food',
    'night', 'life', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport'
]

class CollectionFixtures extends BaseCollectionFixtures {
    getImgUrl = (id) => {
        const v = Math.random()
        if (v > 0.75) {
            return `https://picsum.photos/${Math.random() > 0.3 ? '320/240' : '240/320'}/?${id}`
        } else if (v > 0.5) {
            return `https://loremflickr.com/${Math.random() > 0.3 ? '320/240' : '240/320'}/beer`
        } else if ( v > 0.25) {
            return `https://fakeimg.pl/${Math.random() > 0.3 ? '320x240' : '240x320'}/?text=${faker.random.arrayElement(itemsGroup)}`
        } else {
            return null
        }
    }

    setAdditional = () => {
        const attributes = Attributes.list({ isActive: true, group: 'beers' })
        this.attributesValues = attributes.map(({ _id, params }) => ({
            attributeId: _id,
            attributeValue: (({ paramsType, paramsData }) => {
                switch (paramsType) {
                case 'string': {
                    return faker.commerce.product()
                    break
                }
                case 'number': {
                    return faker.random.number({
                        min: paramsData.min,
                        max: paramsData.mx,
                    })
                    break
                }
                case 'set': {
                    const { multiple, items } = paramsData
                    return multiple ? sampleSize(items, faker.random.number({
                        min: 0,
                        max: items.length,
                    })).join(', ') : faker.random.arrayElement(items)
                    break
                }
                case 'directory': {
                    const { multiple, directory, directoryItems = [] } = paramsData
                    return multiple ? sampleSize(directoryItems, faker.random.number({
                        min: 0,
                        max: directoryItems.length,
                    })).join(', ') : faker.random.arrayElement(directoryItems)
                    break
                }
                case 'boolean': {
                    return Math.random() > 0.5
                    break
                }
                default: {
                    return null
                }
                }
            })(params)
        }))
        this.brands = Brands.ids()
        this.locations = Locations.ids()
    }

    makeFakeData = () => {
        const product = faker.commerce.product()
        const material = faker.commerce.productMaterial()
        const priceId = AttributesListServices.getPriceId()
        const attributesShort = sampleSize(this.attributesValues, faker.random.number(this.attributesValues.length))
        const randomPrice = Math.random() * 800
        let middlePrice = randomPrice
        const locs = sampleSize(this.locations, faker.random.number({
            min: 0,
            max: 10
        })).map(locationId => {
            const rand = Math.random()
            const price = rand < 0.4 ? randomPrice * (1 + (Math.random() > 0.5 ? 1 : -1) * rand) : randomPrice
            middlePrice = (middlePrice + price) / 2
            return { locationId, price: price.toFixed(2) }
        })
        const attrPriceIndex = attributesShort.findIndex(({
            attributeId
        }) => priceId === attributeId)
        if (attrPriceIndex > -1) {
            attributesShort[attrPriceIndex].attributeValue = middlePrice.toFixed(2)
        } else {
            attributesShort.push({
                attributeId: priceId,
                attributeValue: middlePrice.toFixed(2),
            })
        }
        const item = {
            alias: product.toLowerCase() + '_' + material.toLowerCase(),
            title: product + ' ' + material,
            brandId: faker.random.arrayElement(this.brands),
            attributes: attributesShort,
            locations: locs,
            isActive: Math.random() > 0.3,
            website: Math.random() > 0.5 ? faker.internet.url() : '',
            imageUrl: this.getImgUrl()
        }
        return item
    }
}

const fixtures = new CollectionFixtures({ collection: Collection })
fixtures.setAdditional()

export default fixtures
