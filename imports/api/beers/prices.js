import BaseCollection from 'meteor/mcore/api/collection'

const prefix = 'prices'

class _Collection extends BaseCollection {
}

const Collection = new _Collection(prefix)

export default Collection
