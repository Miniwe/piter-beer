import sampleSize from 'lodash/sampleSize'
import uniqBy from 'lodash/uniqBy'
import {
    formatDate,
    stripTags
} from 'meteor/mcore/utils'
import Attributes from 'meteor/mcore/api/attributes'
import Attribute from 'meteor/mcore/api/attributes/model'
import Files from 'meteor/mcore/api/files'
import Locations from '/imports/api/locations'
import Brands from '/imports/api/brands'
import Prices from '/imports/api/beers/prices'
import Collection from '..'
import { ListServices as CommonListServices } from 'meteor/mcore/api/collection/services'


class ListServices {
    static defaultList() {
        return Collection.find().fetch()
    }

    static clear() {
        return Collection.remove({})
    }

    static setActive = (items = []) => {
        items.forEach((_id) => {
            Collection.update({ _id }, { $set: { isActive: true } }, { selector: { type: 'main' } })
        })
    }

    static setInactive = (items = []) => {
        items.forEach((_id) => {
            Collection.update({ _id }, { $set: { isActive: false } }, { selector: { type: 'main' } })
        })
    }

    static removeSelected = (items = []) => {
        items.forEach((_id) => {
            Collection.remove(_id)
        })
    }

    static recordsCount() {
        return Collection.find().count()
    }

    static remove(query) {
        return Collection.remove(query)
    }

    static upsert(doc) {
        const result = Collection.upsert(
            { _id: doc._id },
            { $set: doc },
            { selector: { type: 'main' } }
        )
        this.updatePrices(result.insertedId ? result.insertedId : doc._id, doc.locations)
        return result
    }

    static getHeadData(path, model) {
        if (model) {
            return {
                title: `${model.title} | Пиво`,
                description: `${stripTags(model.description)}`.replace(/\s+/g, ' '),
                keywords: '',
                url: `${Meteor.settings.public.domain}${path}`
            }
        } else {
            return {}
        }
    }

    static getModel(id) {
        return Collection.findOne({
            $or: [{
                _id: id
            }, {
                alias: id
            }]
        })
    }

    static updatePrices = (beerId, locations = []) => {
        Prices.remove({ beerId })
        const list = uniqBy(locations, 'locationId')
        list.forEach(location => Prices.rawCollection().insert({
            ...location,
            beerId
        }))
        Meteor.call('addSyncPricesTask')
    }

    static update(_id, $set) {
        return Collection.upsert(
            { _id },
            { $set },
            { selector: { type: 'main' } }
        )
    }

    static staticList = ({query = {}, params = {}}) => {
        return Collection
            .list(query, { limit: 1e4, ...params })
            .map(({ _id, title }) => ({ _id, title }))
    }

    static getSubcollections(items, all = false) {
        const subcollectionsList = {
            attributes: {
                collection: Attributes,
                ids: ({ attributes = [] }) => attributes.map(({ attributeId }) => attributeId),
            },
            locations: {
                collection: Locations,
                ids: ({ locations = [] }) => locations.map(({ locationId }) => locationId),
            },
            brands: {
                collection: Brands,
                ids: ({ brandId }) => (brandId ? [brandId] : []),
            },
            images: {
                collection: Files.collection,
                ids: ({ images = [], logo = [] }) => [
                    ...images,
                    ...logo
                ],
            }
        }

        return CommonListServices.getSubcollections(
            items, subcollectionsList, all
        )
    }

    static cursor(query = {}, params = {}, allSubcollections = false) {
        const items = Collection.find(this.prepareQuery(query), this.prepareParams(params))
        return [
            items,
            ...this.getSubcollections(items, allSubcollections)
        ]
    }

    static cursorSingle(query = {}, params = {}) {
        return this.cursor(query, params, true)
    }

    static list(query = {}, params = {}) {
        const list = this.cursor(query, params)[0].fetch()

        return list.map((item, index) => ({
            ...item,
            index,
            pubDate: formatDate(item.updatedAt)
        }))
    }

    static getAlias(alias) {
        const count = Collection.find({
            alias: new RegExp(`${alias}(-\\d+)?`, 'gi'),
        }).count()

        return (count > 1) ? `${alias}-${count}` : alias
    }

    static prepareQuery(query = {}) {
        const preparedQuery = CommonListServices.prepareQuery(query)

        const attrsList = query.attributes ? Object.keys(query.attributes) : []
        if (attrsList.length > 0) {
            preparedQuery.$and = []
            const attributes = Attributes.find({
                _id: {
                    $in: attrsList
                }
            }).fetch()
            attributes.forEach((attribute) => {
                const {
                    _id
                } = attribute
                preparedQuery.$and.push(new Attribute(attribute).prepareValueQuery(query.attributes[_id]))
            })
            if (!preparedQuery.$and.length) {
                delete preparedQuery.$and
            }
        }

        if (query.locations) {
            preparedQuery.locations = query.locations
        }

        if (query.similar && query.similar.length) {
            preparedQuery._id = {
                $nin: query.similar
            }
            const similar = Collection.find({
                _id: {
                    $in: query.similar
                }
            }).fetch()

            const compareParams = similar.reduce((params, item, index) => {
                if (!item.attributes || !item.attributes.length) {
                    return params
                }
                const atts = item.attributes.filter(
                    att => att.attributeValue && [
                        '6G2SLW9FTmDin5xfP', //alc
                        'rPoZvqyvC7ko9gn74', //den
                        'XPgTC3DJ7FsAt479q', //price
                    ].indexOf(att.attributeId) > -1
                )
                atts.forEach((param) => {
                    if (index > 0) {
                        params[param.attributeId] = (params[param.attributeId] + param.attributeValue) / 2
                    } else {
                        params[param.attributeId] = param.attributeValue
                    }
                })
                return params
            }, {})

            const $all = []
            Object.keys(compareParams).forEach((key) => {
                const delta = key === 'XPgTC3DJ7FsAt479q' ? 100 : 2
                const value = compareParams[key]
                $all.push({
                    attributes: {
                        $elemMatch: {
                            attributeId: key,
                            attributeValue: {
                                $gte: parseFloat(value) - delta,
                                $lte: parseFloat(value) + delta
                            }
                        }
                    }
                })
            })
            if ($all.length > 0) {
                preparedQuery.$and = $all
            }
        }

        if (query.brandId) {
            preparedQuery.brandId = query.brandId
        }

        if (query.random) {
            if (Meteor.isServer) {
                preparedQuery._id = {
                    $in: sampleSize(Collection.ids(), query.random)
                }
            }
        }

        return preparedQuery
    }

    static prepareParams(params = {}) {
        const preparedParams = CommonListServices.prepareParams(params)

        return preparedParams
    }
}

export default ListServices
