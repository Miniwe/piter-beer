import Files from 'meteor/mcore/api/files'
import Collection from '..'
import { ItemServices as CommonItemServices } from 'meteor/mcore/api/collection/services'

class ItemServices {
    static getItem(id) {
        const item = Collection.findOne(id)
        return {
            ...item
        }
    }

    static getTestItem() {
        const item = Collection.find({}, { limit: 1 }).fetch().shift()
        return {
            ...item
        }
    }

    static getLast() {
        const last = Collection.findOne({}, {
            sort: {
                updatedAt: -1
            }
        })
        return last ? { id: last._id, title: last.title } : { id: null, title: 'n/a' }
    }

    static updateImageUrl(_id, images = []) {
        let imageUrl = ''
        if (images.length > 0) {
            const file = Files.findOne({ _id: { $in: images } })
            if (file) {
                // get image static link
                imageUrl = file.link()
                // in Files set postId to locator
                Files.assignToObject(file._id, 'beer', _id)
            }
        }
        return imageUrl
    }

    static unassignFile(id, fileId, locator) {
        // c_onsole.log('un asssign file', id, fileId, locator)
    }
}

export default ItemServices
