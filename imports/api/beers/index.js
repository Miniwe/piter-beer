import BaseCollection from 'meteor/mcore/api/collection'
import * as schemas from './schemas'
import {
    ItemServices, ListServices
} from './services'
import Model from './model'

const prefix = 'beers'

class _Collection extends BaseCollection {
    transform(item) {
        return new Model(item)
    }

    list(query, params) {
        return this.find(ListServices.prepareQuery(query), ListServices.prepareParams(params))
    }

    options() {
        return [
            { value: 'setActive', text: 'Set Active' },
            { value: 'setInactive', text: 'Set Inactive' },
            { value: 'removeSelected', text: 'Remove Selected' },
        ]
    }
}

const Collection = new _Collection(prefix)
Collection.attachSchemes(schemas)

Collection.before.upsert((userId, selector, modifier) => {
    modifier.$set = modifier.$set || {}
    const {
        _id,
        images,
        locations = [],
        attributes = [],
    } = modifier.$set

    if (images.length) {
        modifier.$set.imageUrl = ItemServices.updateImageUrl(_id, images)
    }
})

export {
    prefix
}
export default Collection
