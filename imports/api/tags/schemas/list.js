import BaseListSchema from 'meteor/mcore/api/collection/schemas/list'

const ListSchema = BaseListSchema.pick().extend({})

export default ListSchema
