import BaseMainSchema from 'meteor/mcore/api/collection/schemas/main'
import { ListServices } from '../services'
import { slugify } from 'meteor/mcore/utils'

const MainSchema = BaseMainSchema
    .omit('alias', 'description')
    .extend({
        alias: {
            type: String,
            label: 'Alias',
            optional: true,
            unique: 'true',
            autoValue() {
                const { value } = this
                const title = this.field('title').value
                if (!value && title) {
                    return ListServices.getAlias(slugify(title))
                }
                return value
            },
        },
        count: {
            type: Number,
            label: 'Locations Count',
            optional: true,
            uniforms: () => null,
            defaultValue: 0,
        },
    })


export default MainSchema
