import { formatDate } from 'meteor/mcore/utils'
import Collection from '..'
import Locations from '../../locations'
import { ListServices as CommonListServices } from 'meteor/mcore/api/collection/services'
import { slugify } from 'meteor/mcore/utils'

class ListServices {
    static defaultList() {
        return Collection.find().fetch()
    }

    static clear() {
        return Collection.remove({})
    }

    static recordsCount() {
        return Collection.find().count()
    }

    static getTownId(title) {
        const town = Collection.findOne({ title })
        if (town) {
            return town._id
        } else {
            const newTown = {
                title,
                alias: slugify(title),
                isActive: true
            }
            return Collection.insert(newTown, {
                selector: {
                    type: 'main'
                }
            })
        }
    }

    static upsert(doc) {
        return Collection.upsert(
            { _id: doc._id },
            { $set: doc },
            { selector: { type: 'main' } }
        )
    }

    static staticList = ({query = {}, params = {}}) => {
        return Collection
            .list(query, { limit: 1e4, ...params })
            .map(({ _id, title }) => ({ _id, title }))
    }

    static remove = (id) => {
        return Collection.remove(id)
    }

    static cursor(query = {}, params = {}, allSubcollections = false) {
        return Collection.find(this.prepareQuery(query), this.prepareParams(params))
    }

    static cursorSingle(query = {}, params = {}) {
        return this.cursor(query, params, true)
    }

    static list(query = {}, params = {}) {
        const list = this.cursor(query, params).fetch()

        return list.map((item, index) => ({
            ...item,
            index,
            pubDate: formatDate(item.updatedAt)
        }))
    }

    static getAlias(alias) {
        const count = Collection.find({
            alias: new RegExp(`${alias}(-\\d+)?`, 'gi'),
        }).count()
        return (count > 0) ? `${alias}-${count}` : alias
    }

    static prepareQuery(query = {}) {
        const preparedQuery = CommonListServices.prepareQuery(query)
        return preparedQuery
    }

    static prepareParams(params = {}) {
        const preparedParams = CommonListServices.prepareParams(params)
        return preparedParams
    }

    static locationsCount(townId) {
        const count = Locations.find({ 'address.townId': townId }).count()
        Collection.update(townId, { $set: { count } }, { selector: { type: 'main' } })
    }

    static updateLocationsCounts() {
        Collection.find().fetch().forEach(({ _id }) => this.locationsCount(_id))
    }
}

export default ListServices
