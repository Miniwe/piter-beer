import BaseCollectionFixtures from 'meteor/mcore/api/collection/server/fixtures'
import { faker } from 'meteor/practicalmeteor:faker'
import Collection from '..'

const itemsGroup = ['abstract', 'animals', 'business', 'cats', 'city', 'food',
    'night', 'life', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport'
]

class CollectionFixtures extends BaseCollectionFixtures {
    makeFakeData = () => {
        const product = faker.commerce.product()
        const material = faker.commerce.productMaterial()
        const item = {
            alias: product.toLowerCase() + '_' + material.toLowerCase(),
            title: product + ' ' + material,
            isActive: Math.random() > 0.3,
            imageUrl: `https://fakeimg.pl/300x100/?text=${faker.random.arrayElement(itemsGroup)}`
        }
        return item
    }
}

export default new CollectionFixtures({ collection: Collection })
