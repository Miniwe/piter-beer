import SimpleSchema from 'simpl-schema'
import SelectField from 'meteor/mcore/ui/uniforms/Select'
import Towns from '/imports/api/towns'

const addressTypes = {
    postaladdress: 'Postal Address',
    addressstring: 'Address String',

}
const geoTypes = {
    geocoordinates: 'Geo Coordinates',
    searchstring: 'Search String'
}

const AddressSchema = new SimpleSchema({
    addressType: {
        type: String,
        optional: true,
        defaultValue: 'postaladdress',
        uniforms: {
            className: 'four wide',
            component: SelectField,
            required: false,
            multiple: false,
            placeholder: 'Select address type',
            options: () => Object.keys(addressTypes),
            transform(id) {
                const res = addressTypes[id] ? {
                    value: id, text: addressTypes[id]
                } : {
                    value: id, text: id
                }
                return res
            }
        }
    },
    postalCode: {
        type: String,
        optional: true,
        uniforms: {
            className: 'four wide',
        },
    },
    townId: {
        type: String,
        label: 'Town',
        optional: true,
        uniforms: {
            className: 'eight wide',
            component: SelectField,
            required: false,
            multiple: false,
            search: 'towns.staticList',
            allowAdditions: false,
            placeholder: 'Select town',
            options: () => Towns.ids(),
            transformMultiple(id) {
                const item = Towns.findOne(id)
                const res = item ? {
                    value: item._id,
                    text: item.title,
                } : {
                    value: id,
                    text: id,
                }
                return res
            }
        },
    },
    streetAddress: {
        type: String,
        optional: true,
        uniforms: {
            className: 'ten wide',
        },
    },
    addressCountry: {
        type: String,
        optional: true,
        uniforms: () => null,
    },
    addressLocality: {
        type: String,
        optional: true,
        uniforms: () => null,
    },

    addressRegion: {
        type: String,
        optional: true,
        uniforms: () => null,
    },
    metro: {
        type: String,
        optional: true,
        uniforms: {
            className: 'six wide',
        },
    },
})

const GeolocationSchema = new SimpleSchema({
    geoType: {
        type: String,
        optional: true,
        defaultValue: 'geocoordinates',
        uniforms: {
            component: SelectField,
            required: false,
            multiple: false,
            placeholder: 'Select geo type',
            options: () => Object.keys(geoTypes),
            transform(id) {
                const res = geoTypes[id] ? {
                    value: id, text: geoTypes[id]
                } : {
                    value: id, text: id
                }
                return res
            }
        }
    },
    latitude: {
        type: Number,
        optional: true
    },
    longitude: {
        type: Number,
        optional: true
    },
    zoom: {
        type: String,
        optional: true,
        defaultValue: 12
    },
    searchString: {
        type: String,
        optional: true
    }
})


export {
    AddressSchema,
    GeolocationSchema,
}
