import SimpleSchema from 'simpl-schema'
import uniforms from 'uniforms-semantic'
import { slugify } from 'meteor/mcore/utils'
import SelectField from 'meteor/mcore/ui/uniforms/Select'
import UploadField from 'meteor/mcore/ui/uniforms/Upload'
import AttributeField from 'meteor/mcore/ui/uniforms/Attribute'
import AddressField from 'meteor/mcore/ui/uniforms/Address'
import LongTextField from 'meteor/mcore/ui/uniforms/Long'
import RichTextEdit from 'meteor/mcore/ui/uniforms/RichTextEdit'
import BaseMainSchema from 'meteor/mcore/api/collection/schemas/main'
import AttributeSchema from 'meteor/mcore/api/attributes/schemas/attribute'
import {
    AddressSchema,
    GeolocationSchema,
    // RatingSchema,
} from './address'
import OpeningHoursSchema from './openingHours'
import { ListServices } from '../services'
import Beers from '/imports/api/beers'

const MainSchema = BaseMainSchema
    .omit('alias', 'description')
    .extend({
        alias: {
            type: String,
            label: 'Alias',
            optional: true,
            unique: 'true',
            autoValue() {
                const {
                    value
                } = this
                const title = this.field('title').value
                if (!value && title) {
                    return ListServices.getAlias(slugify(title))
                }
                return value
            },
        },
        description: {
            type: String,
            label: 'Description',
            optional: true,
            uniforms: LongTextField,
        },
        fullDescription: {
            type: String,
            label: 'Main Description',
            optional: true,
            uniforms: {
                component: RichTextEdit
            }
        },
        imageUrl: {
            type: String,
            optional: true
        },
        images: {
            type: Array,
            label: 'Image',
            maxCount: 10,
            uniforms: {
                component: UploadField,
                viewMode: 'card',
                placeholder: 'Image',
                fileLocator: 'locations.images',
                multiple: true,
                accept: 'image/*',
            },
            optional: true,
        },
        'images.$': {
            type: String,
        },
        logo: {
            type: Array,
            label: 'Logo',
            maxCount: 1,
            uniforms: {
                component: UploadField,
                viewMode: 'card',
                placeholder: 'Image',
                fileLocator: 'locations.logo',
                multiple: false,
                accept: 'image/*',
            },
            optional: true,
        },
        'logo.$': {
            type: String,
        },
        address: {
            type: AddressSchema,
            optional: true,
            label: false,
            uniforms: {
                className: 'inline-grouped ui secondary segment'
            }
        },
        geolocation: {
            type: GeolocationSchema,
            optional: true,
            uniforms: {
                component: AddressField,
            },
        },
        beers: {
            type: Array,
            label: 'Beers With Prices',
            optional: true,
            defaultValue: [],
            uniforms: {
                className: 'ui secondary segment'
            }
        },
        'beers.$': {
            type: Object,
            label: false,
            uniforms: {
                className: 'inline-grouped',
            }
        },
        'beers.$.beerId': {
            type: String,
            label: 'Beer',
            regEx: SimpleSchema.RegEx.Id,
            uniforms: {
                className: 'twelve wide',
                component: SelectField,
                required: false,
                multiple: false,
                search: 'beers.staticList',
                allowAdditions: false,
                placeholder: 'Select beer',
                options: () => Beers.ids(),
                transformMultiple(id) {
                    const item = Beers.findOne(id)
                    const res = item ? {
                        value: item._id,
                        text: item.title,
                    } : {
                        value: id,
                        text: id,
                    }
                    return res
                }
            }
        },
        'beers.$.price': {
            type: Number,
            defaultValue: 0,
            uniforms: {
                className: 'four wide',
            }
        },
        attributes: {
            type: Array,
            label: 'Attributes',
            optional: true,
            uniforms: {
                className: 'ui secondary segment',
            },
        },
        'attributes.$': {
            type: AttributeSchema,
            uniforms: {
                component: AttributeField,
            },
        },
        openingHours: {
            type: Array,
            optional: true,
            uniforms: {
                className: 'ui secondary segment',
            },
        },
        'openingHours.$': {
            type: OpeningHoursSchema,
            uniforms: {
                className: 'inline-grouped',
            },
        },
        count: {
            type: Number,
            label: 'Beers Count',
            optional: true,
            uniforms: () => null,
            defaultValue: 0,
        },
    })

export default MainSchema
