import SimpleSchema from 'simpl-schema'

const DetailScema = new SimpleSchema({
    title: {
        type: String
    },
    value: {
        type: String,
        optional: true
    }
})

export default DetailScema
