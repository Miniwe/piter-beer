import SimpleSchema from 'simpl-schema'

const OpeningHoursSchema = new SimpleSchema({
    dayOfWeek: {
        type: String,
        optional: true,
        uniforms: {
            className: 'four wide'
        }
    },
    opens: {
        type: String,
        optional: true,
        uniforms: {
            className: 'three wide'
        }
    },
    closes: {
        type: String,
        optional: true,
        uniforms: {
            className: 'three wide'
        }
    },
    more: {
        type: String,
        optional: true,
        uniforms: {
            className: 'six wide'
        }
    }
})

export default OpeningHoursSchema
