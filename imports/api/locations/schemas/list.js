import uniforms from 'uniforms-semantic'
import ListText from 'meteor/mcore/ui/uniforms/ListText'
import BaseListSchema from 'meteor/mcore/api/collection/schemas/list'

const ListSchema = BaseListSchema
    .pick('title', 'isActive')
    .extend({
        address: {
            type: String,
            label: 'Address Street',
            uniforms: {
                component: ListText,
                transform: value => (value ? value.streetAddress : JSON.stringify(value)),
                width: '3',
            },
        },
        count: {
            type: Number,
            label: 'Beers Count',
            uniforms: {
                component: ListText,
                filter: false,
                sort: true,
            }
        },
    })

export default ListSchema
