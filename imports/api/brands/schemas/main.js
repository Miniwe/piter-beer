import SimpleSchema from 'simpl-schema'
import uniforms from 'uniforms-semantic'
import BaseMainSchema from 'meteor/mcore/api/collection/schemas/main'
import UploadField from 'meteor/mcore/ui/uniforms/Upload'
import AttributeField from 'meteor/mcore/ui/uniforms/Attribute'
import SelectField from 'meteor/mcore/ui/uniforms/Select'
import AttributeSchema from 'meteor/mcore/api/attributes/schemas/attribute'
import {
    formatDate, slugify
} from 'meteor/mcore/utils'
import { ListServices } from '../services'
import News from '../../news'

const MainSchema = BaseMainSchema
    .pick(
        '_id',
        'title',
        'description'
    )
    .extend({
        alias: {
            type: String,
            label: 'Alias',
            optional: true,
            unique: 'true',
            autoValue() {
                const { value } = this
                const title = this.field('title').value
                if (!value && title) {
                    return ListServices.getAlias(slugify(title))
                }
                return value
            },
        },
        imageUrl: {
            type: String,
            optional: true
        },
        images: {
            type: Array,
            label: 'Image',
            maxCount: 1,
            uniforms: {
                component: UploadField,
                viewMode: 'card',
                placeholder: 'Image',
                fileLocator: 'brands.images',
                multiple: false,
                accept: 'image/*',
            },
            optional: true,
        },
        'images.$': {
            type: String,
            regEx: SimpleSchema.RegEx.Id,
        },
        website: {
            type: String,
            regEx: SimpleSchema.RegEx.Url,
            optional: true,
        },
        attributes: {
            type: Array,
            label: 'Attributes',
            optional: true,
            uniforms: {
                className: 'ui secondary segment',
            },
        },
        'attributes.$': {
            type: AttributeSchema,
            uniforms: {
                component: AttributeField,
            },
        },
        news: {
            type: Array,
            label: 'News list',
            optional: true,
            uniforms: {
                className: 'ui secondary segment',
            },
            defaultValue: []
        },
        'news.$': Object,
        'news.$.item': {
            type: String,
            label: false,
            regEx: SimpleSchema.RegEx.Id,
            optional: true,
            uniforms: {
                component: SelectField,
                required: false,
                multiple: false,
                search: 'news.staticList',
                allowAdditions: false,
                placeholder: 'Select news',
                options: () => News.ids(),
                transformMultiple(id) {
                    const item = News.findOne(id)
                    const res = item ? {
                        value: item._id,
                        text: `${formatDate(item.pubDate)}: ${item.title}`,
                    } : {
                        value: id,
                        text: id,
                    }
                    return res
                }
            },
        },
        count: {
            type: Number,
            label: 'Beers Count',
            optional: true,
            uniforms: () => null,
            defaultValue: 0,
        },
    })
    .extend(BaseMainSchema.pick(
        'isActive',
        'createdAt',
        'updatedAt'
    ))

export default MainSchema
