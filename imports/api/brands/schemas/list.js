import BaseListSchema from 'meteor/mcore/api/collection/schemas/list'
import ListText from 'meteor/mcore/ui/uniforms/ListText'

const ListSchema = BaseListSchema
    .pick('title', 'isActive')
    .extend({
        count: {
            type: Number,
            label: 'Beers Count',
            uniforms: {
                component: ListText,
                filter: false,
                sort: true,
            }
        }
    })

export default ListSchema
