import BaseCollectionFixtures from 'meteor/mcore/api/collection/server/fixtures'
import { faker } from 'meteor/practicalmeteor:faker'
import Collection from '..'

class CollectionFixtures extends BaseCollectionFixtures {
    makeFakeData = () => {
        const product = faker.commerce.product()
        const material = faker.commerce.productMaterial()
        const item = {
            alias: product.toLowerCase() + '_' + material.toLowerCase(),
            title: product + ' ' + material,
            isActive: Math.random() > 0.3
        }
        return item
    }
}

export default new CollectionFixtures({ collection: Collection })
