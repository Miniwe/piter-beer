import { formatDate, stripTags } from 'meteor/mcore/utils'
import Attributes from 'meteor/mcore/api/attributes'
import Attribute from 'meteor/mcore/api/attributes/model'
import Beers from '/imports/api/beers'
import Files from 'meteor/mcore/api/files'
import Collection from '..'
import News from '../../news'
import { ListServices as CommonListServices } from 'meteor/mcore/api/collection/services'


class ListServices {
    static defaultList() {
        return Collection.find().fetch()
    }

    static clear() {
        return Collection.remove({})
    }

    static recordsCount() {
        return Collection.find().count()
    }


    static getHeadData(path, model) {
        if (model) {
            return {
                title: `${model.title} | Марки`,
                description: `${stripTags(model.description)}`.replace(/\s+/g, ' '),
                keywords: '',
                url: `${Meteor.settings.public.domain}${path}`
            }
        } else {
            return {}
        }
    }

    static getModel(id) {
        return Collection.findOne({
            $or: [{
                _id: id
            }, {
                alias: id
            }, ]
        });
    }

    static remove(query) {
        return Collection.remove(query)
    }

    static upsert(doc) {
        return Collection.upsert(
            { _id: doc._id },
            { $set: doc },
            { selector: { type: 'main' } }
        )
    }

    static staticList = ({query = {}, params = {}}) => {
        return Collection
            .list(query, { limit: 1e4, ...params })
            .map(({ _id, title }) => ({ _id, title }))
    }

    static getSubcollections(items, all = false) {
        const subcollectionsList = {
            attributes: {
                collection: Attributes,
                ids: ({ attributes = [] }) => attributes.map(({ attributeId }) => attributeId),
            },
            news: {
                collection: News,
                ids: ({ news = [] }) => news,
            },
            images: {
                collection: Files.collection,
                ids: ({ images = [], logo = [] }) => [
                    ...images,
                    ...logo
                ],
            }
        }

        return CommonListServices.getSubcollections(
            items, subcollectionsList, all
        )
    }

    static cursor(query = {}, params = {}, allSubcollections = false) {
        const items = Collection.find(this.prepareQuery(query), this.prepareParams(params))
        return [
            items,
            ...this.getSubcollections(items, allSubcollections)
        ]
    }

    static cursorSingle(query = {}, params = {}) {
        return this.cursor(query, params, true)
    }

    static list(query = {}, params = {}) {
        const list = this.cursor(query, params)[0].fetch()

        return list.map((item, index) => ({
            ...item,
            index,
            pubDate: formatDate(item.updatedAt)
        }))
    }

    static getAlias(alias) {
        const count = Collection.find({
            alias: new RegExp(`${alias}(-\\d+)?`, 'gi'),
        }).count()
        return (count > 0) ? `${alias}-${count}` : alias
    }

    static prepareQuery(query = {}) {
        const preparedQuery = CommonListServices.prepareQuery(query)

        const attrsList = query.attributes ? Object.keys(query.attributes) : []
        if (attrsList.length > 0) {
            preparedQuery.$and = []
            const attributes = Attributes.find({
                _id: {
                    $in: attrsList
                }
            }).fetch()
            attributes.forEach((attribute) => {
                const {
                    _id
                } = attribute
                preparedQuery.$and.push(new Attribute(attribute).prepareValueQuery(query.attributes[_id]))
            })
            if (!preparedQuery.$and.length) {
                preparedQuery.$and.push({})
            }
        }

        return preparedQuery
    }

    static prepareParams(params = {}) {
        const preparedParams = CommonListServices.prepareParams(params)

        return preparedParams
    }

    static beersCount(brandId) {
        const count = Beers.find({ brandId }).count()
        Collection.update(brandId, { $set: { count } }, { selector: { type: 'main' } })
    }

    static updateBeersCounts() {
        Collection.find().fetch().forEach(({ _id }) => this.beersCount(_id))
    }
}

export default ListServices
