import BaseListSchema from 'meteor/mcore/api/collection/schemas/list'
import ListText from 'meteor/mcore/ui/uniforms/ListText'
import { formatDate } from 'meteor/mcore/utils'

const ListSchema = BaseListSchema
    .pick('title')
    .extend({
        pubDate: {
            type: Date,
            label: 'Pub Date',
            uniforms: {
                component: ListText,
                transform(value) {
                    return formatDate(value)
                },
                sort: true
            },
        },
    })
    .extend(BaseListSchema.pick('isActive'))

export default ListSchema
