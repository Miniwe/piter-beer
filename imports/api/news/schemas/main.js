import BaseMainSchema from 'meteor/mcore/api/collection/schemas/main'
import ExtEdit from 'meteor/mcore/ui/uniforms/ExtEdit'
import DateTimePicker from 'meteor/mcore/ui/uniforms/DateTimePicker'
import { slugify } from 'meteor/mcore/utils'
import { ListServices } from '../services'

const MainSchema = BaseMainSchema
    .omit('alias')
    .extend({
        alias: {
            type: String,
            label: 'Alias',
            optional: true,
            unique: 'true',
            autoValue() {
                const { value } = this
                const title = this.field('title').value
                if (!value && title) {
                    return ListServices.getAlias(slugify(title))
                }
                return value
            },
        },
        pubDate: {
            type: Date,
            label: 'Pub Date',
            autoValue() {
                const { value = new Date() } = this
                return value
            },
            uniforms: {
                component: DateTimePicker,
            },
            optional: true,
        },
        textId: {
            type: String,
            label: 'MainText',
            optional: true,
            uniforms: {
                component: ExtEdit,
            }
        },
    })


export default MainSchema
