import BaseCollectionFixtures from 'meteor/mcore/api/collection/server/fixtures'
import { faker } from 'meteor/practicalmeteor:faker'
import Collection from '..'

class CollectionFixtures extends BaseCollectionFixtures {
    makeFakeData = () => {
        const town = faker.address.city()
        const item = {
            title: town,
            isActive: Math.random() > 0.2
        }
        return item
    }
}

export default new CollectionFixtures({ collection: Collection })
