import { formatDate } from 'meteor/mcore/utils'
import Texts from 'meteor/mcore/api/texts'
import { ListServices as CommonListServices } from 'meteor/mcore/api/collection/services'
import Collection, { prefix } from '..'

class ListServices {
    static clear(query = {}) {
        return Collection.remove(query)
    }

    static recordsCount(query = {}) {
        return Collection.find(query).count()
    }

    static upsert(doc) {
        const text = Texts.findOne({
            objectId: doc._id,
            typeId: prefix
        })
        let textId
        if (text) {
            Texts.update(text._id, {
                $set: {
                    text: doc.textId
                }
            }, {
                selector: {
                    type: 'main'
                }
            })
            textId = text._id
        } else {
            textId = Texts.insert({
                objectId: 'newText',
                typeId: prefix,
                text: doc.textId
            }, {
                selector: {
                    type: 'main'
                }
            })
        }

        const res = Collection.upsert({
            _id: doc._id
        }, {
            $set: { ...doc, textId }
        }, {
            selector: { type: 'main' }
        })

        if (res.insertedId) {
            Texts.update(textId, {
                $set: { objectId: res.insertedId }
            }, {
                selector: { type: 'main' }
            })
        }

        return res
    }

    static staticList({
        query = {},
        params = {}
    }) {
        return Collection
            .list(query, {
                limit: 1e4,
                ...params
            })
            .map(({ _id, alias, title }) => ({
                _id, alias, title
            }))
    }

    static remove = (id) => {
        Texts.remove({
            objectId: id
        })
        return Collection.remove(id)
    }

    static getSubcollections(items, all = false) {
        const subcollectionsList = {
            texts: {
                collection: Texts,
                ids: ({ textId }) => (textId ? [textId] : []),
            },

        }

        return CommonListServices.getSubcollections(
            items, subcollectionsList, all
        )
    }

    static cursor(query = {}, params = {}, allSubcollections = false) {
        const items = Collection.find(this.prepareQuery(query), this.prepareParams(params))
        return [
            items,
            ...this.getSubcollections(items, allSubcollections)
        ]
    }

    static cursorSingle(query = {}, params = {}) {
        return this.cursor(query, params, true)
    }

    /** @depricated */
    static list(query = {}, params = {}) {
        const list = this.cursor(query, params).fetch()

        return list.map((item, index) => ({
            ...item,
            index,
            pubDate: formatDate(item.updatedAt || new Date())
        }))
    }

    static getAlias(alias) {
        const count = Collection.find({
            alias: new RegExp(`${alias}(-\\d+)?`, 'gi'),
        }).count()
        return (count > 0) ? `${alias}-${count}` : alias
    }

    static prepareQuery(query = {}) {
        const preparedQuery = CommonListServices.prepareQuery(query)
        return preparedQuery
    }

    static prepareParams(params = {}) {
        const preparedParams = CommonListServices.prepareParams(params)
        return preparedParams
    }
}

export default ListServices
