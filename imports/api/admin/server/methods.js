import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { rateLimit } from 'meteor/mcore/utils'
import Pages from 'meteor/mcore/api/pages/services'
import Users from 'meteor/mcore/api/users/services'
import Attributes from 'meteor/mcore/api/attributes/services'
import Texts from 'meteor/mcore/api/texts/services'
import Brands from '/imports/api/brands/services'
import Beers from '/imports/api/beers/services'
import Towns from '/imports/api/towns/services'
import Tags from '/imports/api/tags/services'
import News from '/imports/api/news/services'
import Locations from '/imports/api/locations/services'
import {
    PricesServices,
    CountsServices
} from '../services'

export const addSyncPricesTask = new ValidatedMethod({
    name: 'addSyncPricesTask',
    validate() {},
    run() {
        PricesServices.addCronTask()
    }
})

export const syncPrices = new ValidatedMethod({
    name: 'syncPrices',
    validate() {},
    run() {
        PricesServices.syncPrices()
    }
})

export const updateCounters = new ValidatedMethod({
    name: 'admin.updateCounters',
    validate() {},
    run() {
        CountsServices.updateCounters()
    }
})

export const importAll = new ValidatedMethod({
    name: 'importAll',
    validate() {},
    run() {
        Brands.ListServices.clear()
        Beers.ListServices.clear()
        Towns.ListServices.clear()
        Tags.ListServices.clear()
        Attributes.ListServices.clear()
        Meteor.apply('import.afisha', [], { noRetry: true, wait: true })
    }
})

export const dashboard = new ValidatedMethod({
    name: 'admin.dashboard',
    validate() {},
    run() {
        return {
            beers: {
                total: Beers.ListServices.recordsCount(),
                last: Beers.ItemServices.getLast()
            },
            brands: {
                total: Brands.ListServices.recordsCount(),
                last: Brands.ItemServices.getLast()
            },
            towns: {
                total: Towns.ListServices.recordsCount(),
                last: Towns.ItemServices.getLast()
            },
            tags: {
                total: Tags.ListServices.recordsCount(),
                last: Tags.ItemServices.getLast()
            },
            texts: {
                total: Texts.ListServices.recordsCount(),
                last: Texts.ItemServices.getLast()
            },
            news: {
                total: News.ListServices.recordsCount(),
                last: News.ItemServices.getLast()
            },
            users: {
                total: Users.ListServices.recordsCount(),
                last: Users.ItemServices.getLast()
            },
            attributes: {
                total: Attributes.ListServices.recordsCount(),
                last: Attributes.ItemServices.getLast()
            },
            locations: {
                total: Locations.ListServices.recordsCount(),
                last: Locations.ItemServices.getLast()
            },
            pages: {
                total: Pages.ListServices.recordsCount(),
                last: Pages.ItemServices.getLast()
            },
            // settings: {
            //     total: Settings.ListServices.recordsCount(),
            //     last: Settings.ItemServices.getLast()
            // },
        }
    }
})


rateLimit({
    methods: [
        importAll,
        syncPrices,
        updateCounters,
        addSyncPricesTask,
        dashboard,
    ],
    limit: 5,
    timeRange: 1000,
})
