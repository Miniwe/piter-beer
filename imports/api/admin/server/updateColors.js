import {
    Meteor
} from 'meteor/meteor'
import {
    ValidatedMethod
} from 'meteor/mdg:validated-method'
import {
    rateLimit
} from 'meteor/mcore/utils'
import Beers from '/imports/api/beers'
import Attributes from 'meteor/mcore/api/attributes/services'

// eslint-disable-next-line import/prefer-default-export
export const updateColors = new ValidatedMethod({
    name: 'admin.updateColors',
    validate() {},
    run() {
        const oldAttr = Attributes.ListServices.getAttr('cvet-piva')
        const newAttr = Attributes.ListServices.getAttr('beer-color')

        Beers.find({ 'attributes.attributeId': oldAttr._id }).fetch().map(({ _id, attributes }) => Beers.update(_id, {
            $set: {
                attributes: [
                    ...attributes,
                    {
                        attributeId: newAttr._id,
                        attributeValue: attributes.find(item => item.attributeId === oldAttr._id).attributeValue
                    }
                ]
            }
        }, {
            selector: {
                type: 'main'
            }
        }))
    }
})


rateLimit({
    methods: [
        updateColors,
    ],
    limit: 5,
    timeRange: 1000,
})
