import {
    Meteor
} from 'meteor/meteor'
import {
    ValidatedMethod
} from 'meteor/mdg:validated-method'
import {
    rateLimit
} from 'meteor/mcore/utils'
import Beers from '/imports/api/beers'
import Attributes from 'meteor/mcore/api/attributes/services'

// eslint-disable-next-line import/prefer-default-export
export const updateOrders = new ValidatedMethod({
    name: 'admin.updateOrders',
    validate() {},
    run() {
        const priceAttr = Attributes.ListServices.getPriceAttr()
        Beers.find({
            'attributes.attributeId': priceAttr._id
        }).fetch().map(({
            _id,
            attributes,
            order = {}
        }) => {
            const price = parseFloat(attributes.find(item => item.attributeId === priceAttr._id).attributeValue)
            return Beers.update(_id, {
                $set: {
                    order: {
                        ...order,
                        price
                    }
                }
            }, {
                selector: {
                    type: 'main'
                }
            })
        })
    }
})


rateLimit({
    methods: [
        updateOrders,
    ],
    limit: 5,
    timeRange: 1000,
})
