import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { rateLimit } from 'meteor/mcore/utils'
import {
    AfishaServices,
    RestoServices,
    ZoonServices,
    BrandsServices,
} from '../services'


export const locationsResto = new ValidatedMethod({
    name: 'import.resto',
    validate() {},
    run() {
        // this.unblock()
        return RestoServices.import()
    }
})

export const locationsAfisha = new ValidatedMethod({
    name: 'import.afisha',
    validate() {},
    run() {
        // this.unblock()
        return AfishaServices.import()
    }
})

export const locationsZoon = new ValidatedMethod({
    name: 'import.zoon',
    validate() {},
    run() {
        // this.unblock()
        return ZoonServices.import()
    }
})

export const imagesList = new ValidatedMethod({
    name: 'import.imagesList',
    validate() {},
    run() {
        // this.unblock()
        // return LocationsServices.imagesList()
    }
})

export const importBrands = new ValidatedMethod({
    name: 'import.importBrands',
    validate() {},
    run() {
        // this.unblock()
        return BrandsServices.importList()
    }
})

rateLimit({
    methods: [
        imagesList,
        locationsZoon,
        locationsResto,
        locationsAfisha,
        importBrands,
    ],
    limit: 1,
    timeRange: 5000,
})
