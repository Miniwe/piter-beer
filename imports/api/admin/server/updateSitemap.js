import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { rateLimit } from 'meteor/mcore/utils'
import times from 'lodash/times'
import path from 'path'
import fs from 'file-system'
import Beers from '/imports/api/beers'
import Brands from '/imports/api/brands'
import Locations from '/imports/api/locations'

const SITEMAP_COUNT = Meteor.settings.public.sitemapCount

const getCounts = () =>

Math.ceil((Beers.find({
        isActive: true
    }).count() +
    Brands.find({
        isActive: true
    }).count() +
    Locations.find({
        isActive: true
    }).count()) / SITEMAP_COUNT)

export const updateSitemap = new ValidatedMethod({
    name: 'admin.updateSitemap',
    validate() {},
    run() {
        const meteorRoot = fs.realpathSync(`${process.cwd()}/../`)
        let applicationRoot = fs.realpathSync(`${meteorRoot}/../`)

        // if running on dev mode
        if (path.basename(fs.realpathSync(`${meteorRoot}/../../../`)) === '.meteor') {
            applicationRoot = fs.realpathSync(`${meteorRoot}'/../../../../`)
        }

        const sitemapFile = `${applicationRoot}/public/sitemap.xml`

        fs.truncateSync(sitemapFile, 0, function () {})
        fs.appendFileSync(sitemapFile, '<?xml version="1.0" encoding="UTF-8"?>\n<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')

        const date = new Date().toISOString()
        times(getCounts(), (index) => {
            fs.appendFileSync(sitemapFile, `<sitemap><loc>${Meteor.settings.public.domain}/sitemap-${index}.xml</loc><lastmod>${date}</lastmod></sitemap>\n`)
        })

        fs.appendFileSync(sitemapFile, '</sitemapindex>\n')
    }
})


rateLimit({
    methods: [
        updateSitemap,
    ],
    limit: 5,
    timeRange: 1000,
})
