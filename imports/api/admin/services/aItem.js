import Attributes from 'meteor/mcore/api/attributes'
import { ListServices } from 'meteor/mcore/api/attributes/services'

const GROUP = 'locations'

export const set = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'set'
    }, defaultParams))

    if (attribute.params.paramsData.items.indexOf(attributeValue) < 0) {
        Attributes.update(attribute._id, {
            $set: {
                params: {
                    ...attribute.params,
                    paramsData: {
                        ...attribute.params.paramsData,
                        items: [...attribute.params.paramsData.items, attributeValue],
                    },
                },
            },
        }, {
            selector: {
                type: 'main'
            }
        })
    }
    return {
        attributeId: attribute._id,
        attributeValue,
    }
}

export const setMultiple = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'set'
    }, defaultParams))
    const newValues = []
    attributeValue.forEach((item) => {
        const lowItem = item.toLowerCase()
        if (attribute.params.paramsData.items.indexOf(lowItem) < 0) {
            newValues.push(lowItem)
        }
    })
    if (newValues.length) {
        Attributes.update(attribute._id, {
            $set: {
                params: {
                    ...attribute.params,
                    paramsData: {
                        ...attribute.params.paramsData,
                        items: [...attribute.params.paramsData.items, ...newValues],
                    },
                },
            },
        }, {
            selector: {
                type: 'main'
            }
        })
    }
    return {
        attributeId: attribute._id,
        attributeValue: attributeValue.join(','),
    }
}

export const directory = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'directory'
    }, defaultParams))

    return {
        attributeId: attribute._id,
        attributeValue: (new String(attributeValue)).valueOf(),
    }
}

export const directoryMultiple = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'directory'
    }, defaultParams))

    return {
        attributeId: attribute._id,
        attributeValue: (new String(attributeValue)).valueOf(),
    }
}

export const string = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'string'
    }, defaultParams))

    return {
        attributeId: attribute._id,
        attributeValue: (new String(attributeValue)).valueOf(),
    }
}

export const boolean = (alias, attributeValue, defaultParams) => {
    const attribute = ListServices.getAttr(alias, Object.assign({
        group: GROUP,
        paramsType: 'boolean'
    }, defaultParams))

    return {
        attributeId: attribute._id,
        attributeValue
    }
}

export default GROUP
