
import { Meteor } from 'meteor/meteor'
// import { SyncedCron } from 'meteor/littledata:synced-cron'
import Towns from '/imports/api/towns/services'
import Locations from '/imports/api/locations/services'
import Brands from '/imports/api/brands/services'

// const COUNTS_TASK = 'COUNTS_TASK'

class CountsServices {
    static updateCounters() {
        Towns.ListServices.updateLocationsCounts()
        Brands.ListServices.updateBeersCounts()
        Locations.ListServices.updateBeersCounts()
    }

    static register() {
        // SyncedCron.add({
        //     name: COUNTS_TASK,
        //     schedule: parser => parser.text('every 1 day'),
        //     job() {
        //         Towns.ListServices.updateLocationsCounts()
        //         Brands.ListServices.updateBeersCounts()
        //         Locations.ListServices.updateBeersCounts()
        //     }
        // })

    }
}

export default CountsServices
