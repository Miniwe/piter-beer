import { Meteor } from 'meteor/meteor'
import isEmpty from 'lodash/isEmpty'
const cheerio = require('cheerio')
import fs from 'file-system'
import path from 'path'
import Collection from '/imports/api/locations'
import { ListServices as LocationsListServices } from '/imports/api/locations/services'
import { ListServices as TagsListServices } from '/imports/api/tags/services'
// import { ListServices as TypesListServices } from '/imports/api/types/services'
// import { ListServices as StatesListServices } from '/imports/api/states/services'
import { ListServices as TownsListServices } from '/imports/api/towns/services'
// import { ListServices as AreasListServices } from '/imports/api/areas/services'
import { slugify } from 'meteor/mcore/utils'
import * as aItem from './aItem'

// /**
//  * @description to call
//  *      Meteor.apply('import.locations', [], {noRetry: true, wait: true }, function(err, res) { c_onsole.log(err, res)})
//  */
let counter = 0
const logFile = '/Users/miniwe/Projects/moscow-beer-project/import/noprofile.log'
const srcDir = '/Users/miniwe/Projects/moscow-beer-project/import/zoon.ru/'
const imagesFile = '/Users/miniwe/Projects/moscow-beer-project/import/zoon-images.txt'

const parseSome = (content, filename) => {
    if (!content) {
        fs.appendFileSync(logFile, 'content error' + ' ' + filename)
        return false
    }
    const $ = cheerio.load(content, {xmlMode: true})
    const item = {
        filename,
        attributes: []
    }

    item.attributes.push(aItem.set('sourceType', 'zoon', { name: 'Source Type' }))

    const appLd = $('script[type="application/ld+json"]')
    let profile = false
    try {
        profile = JSON.parse(appLd[0].children[0].data)
    }
    catch (e) {
    }

    if (appLd && appLd.length && profile) {
        const {
            name,
            // description,
            // address = {},
            // geo = {},
            priceRange = '',
            // url = '',
            // telephone = '',
            image = '',
            // acceptsReservations = '',
            // servesCuisine = [],
            // openingHoursSpecification = []
        } = profile

        item.attributes.push(aItem.string('source', filename, { name: 'Source (internal Use)' }))

        item.title = name
        item.alias = LocationsListServices.getAlias(slugify(item.title))
        item.description = $('[data-track-text-action="description"]').children().first().text()
        item.fullDescription = $('[data-track-text-action="description"]').html()
        item.address = {
            addressType: 'postaladdress',
            streetAddress: $('address.iblock').text(),
            addressLocality: $('span.js-header-city').text(),
            addressRegion: $('span.js-header-city').text(),
            postalCode: '',
            addressCountry: 'RUS',
        }

        item.address.townId = item.address.addressLocality ? TownsListServices.getTownId(item.address.addressLocality) : ''

        item.geolocation = {
            geoType: 'geocoordinates',
            latitude: '',
            longitude: '',
            zoom: 10,
            searchString: item.address.streetAddress,
        }
        const scriptText = $('body').html()
        const sData = scriptText.match(new RegExp(/lonlat:\s\[(\d{2}\.\d+),(\d{2}\.\d+)\]/))
        if (sData.length) {
            Object.assign(item.geolocation, {
                longitude: sData[1],
                latitude: sData[2],
            })
        } else {
            throw new Meteor.Error('geo', 'no geo scriptText: ' + filename)
        }

        item.attributes.push(aItem.string('priceRange', priceRange, { name: 'Средний чек' }))

        const jsPhone = $('.service-maininfo .js-phone-number').attr('href')
        item.attributes.push(aItem.string('website', $('.service-website a').attr('href'), { name: 'Website' }))
        item.attributes.push(aItem.string('phone', jsPhone ? jsPhone.split(':').pop() : '', { name: 'Phone' }))

        // item.acceptsReservations = acceptsReservations === 'True'

        $('.service-description-block .params-list dt').each((i, element) => {
            if ($(element).text() === 'Кухня') {
                const cuisine = []
                $(element).parent().find('a').each((i, a) => {
                    cuisine.push($(a).text())
                })
                item.attributes.push(aItem.setMultiple('cuisine', cuisine, {
                    name: 'Serves Cuisine',
                    paramsData: {
                        items: [],
                        multiple: true
                    }
                }))
            }
            if ($(element).text() === 'Тип заведения') {
                item.type = []
                $(element).parent().find('a').each((i, a) => {
                    item.type.push($(a).text())
                })
            }
            if ($(element).text() === 'Услуги') {
                const tags = []
                $(element).parent().find('a').each((i, a) => {
                    tags.push($(a).text())
                })

                item.attributes.push(aItem.directoryMultiple('tags', TagsListServices.updateTags(tags), {
                    name: 'Tags Directory',
                    isFilter: true,
                    paramsType: 'directory',
                    paramsData: {
                        multiple: true,
                        directory: 'tags',
                    },
                }))
            }
        })

        delete item.type // TODO
        // if (openingHoursSpecification.length) {
        //     item.openingHours = openingHoursSpecification.map(oh => ({
        //         dayOfWeek: oh.dayOfWeek,
        //         opens: oh.opens,
        //         closes: oh.closes,
        //     }))
        // }
        item.openingHours = [
            {
                more: $(".service-box-description .uit-cover .gray").text()
            }
        ]

        if (image) {
            item.imageUrl = image
            fs.appendFileSync(imagesFile, image + '\n')
        }
    } else {
        fs.appendFileSync( logFile, filename + '\n' )
        return false
    }

    if (item.title) {
        Collection.insert(item, {
            selector: {
                type: 'main'
            }
        })
        counter++
    }
}

const getAllFiles = dir =>
    fs.readdirSync(dir).reduce((files, file) => {
        const name = path.join(dir, file)
        const isDirectory = fs.statSync(name).isDirectory()
        return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name]
    }, [])

const readSource = (directory) => {
    counter = 0
    let ck  = 0
    const filesArray = getAllFiles(directory).filter( file => file.split('.').pop() == 'html' )

    filesArray.forEach((item) => {
        if ( ck < 1e5 ) {
            const data = fs.readFileSync(item, {
                encoding: 'utf8'
            })
            parseSome(data, item)
            ck++
        }
    })
    return filesArray.length
}

class ZoonServices {
    static clear() {
        Collection.remove({
            'attributes.attributeValue': 'zoon'
        })
    }

    static import() {
        // this.clear()
        // AreasListServices.clear()
        // TagsListServices.clear()
        // TownsListServices.clear()
        // TypesListServices.clear()
        fs.truncateSync(logFile, 0, function () {
        })
        fs.truncateSync(imagesFile, 0, function () {
        })
        const result = {
            files: readSource(srcDir),
            parsed: counter
        }
        return result
        // throw new Meteor.Error('import', 'import call bla bla bla')
    }

    static imagesList() {
        /** @description on future */
    }
}

export default ZoonServices
