
import { Meteor } from 'meteor/meteor'
import Collection from '/imports/api/brands'
import { slugify } from 'meteor/mcore/utils'
import * as aItem from './aItem'

class BrandsServices {
    static clear() {
        Collection.remove({ })
    }

    static importList() {
        this.clear()
        const source = JSON.parse(Assets.getText('brands.json'))
        source.forEach((item, index) => {
            const model = {
                title: item.name,
                imageUrl: item.image,
                attributes: [
                    aItem.string('rank', index, {
                        name: 'Most Popular Beer Rank',
                        group: 'brands',
                    })
                ]
            }
            Collection.insert(model, { selector: { type: 'main' } })
        })
    }
}

export default BrandsServices
