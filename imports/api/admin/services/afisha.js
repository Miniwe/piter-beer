import { Meteor } from 'meteor/meteor'
import isEmpty from 'lodash/isEmpty'
const cheerio = require('cheerio')
import fs from 'file-system'
import path from 'path'
import Collection from '/imports/api/locations'
import { ListServices as LocationsListServices } from '/imports/api/locations/services'
import { ListServices as TagsListServices } from '/imports/api/tags/services'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'
// import { ListServices as StatesListServices } from '/imports/api/states/services'
import { ListServices as TownsListServices } from '/imports/api/towns/services'
// import { ListServices as AreasListServices } from '/imports/api/areas/services'
import { slugify } from 'meteor/mcore/utils'
import * as aItem from './aItem'

// /**
//  * @description to call
//  *      Meteor.apply('import.locations', [], {noRetry: true, wait: true }, function(err, res) { c_onsole.log(err, res)})
//  */
let counter = 0
const logFile = '/Users/miniwe/Projects/moscow-beer-project/import/noprofile.log'
const srcDir = '/Users/miniwe/Projects/moscow-beer-project/import/www.afisha.ru/'
const imagesFile = '/Users/miniwe/Projects/moscow-beer-project/import/afisha-images.txt'

const parseSome = (content, filename) => {
    if (!content) {
        fs.appendFileSync(logFile, 'content error' + ' ' + filename)
        return false
    }
    const $ = cheerio.load(content, {xmlMode: true})
    const item = {
        filename,
        attributes: []
    }

    item.attributes.push(aItem.set('sourceType', 'afisha', { name: 'Source Type' }))

    const appLd = $('script[type="application/ld+json"]')
    let profile = false
    try {
        profile = JSON.parse(appLd[0].children[0].data)
    }
    catch (e) {
    }

    if (appLd && appLd.length && profile) {
        const {
            address = {},
            geo = {},
            priceRange = '',
            url = '',
            telephone = '',
            image = '',
            acceptsReservations = '',
            servesCuisine = [],
            openingHoursSpecification = []
        } = profile

        item.attributes.push(aItem.string('source', profile['@id'], { name: 'Source (internal Use)' }))

        item.title = profile.name
        item.alias = LocationsListServices.getAlias(slugify(item.title))
        item.description = profile.description
        item.address = {}
        if (!isEmpty(address)) {
            const {
                streetAddress = '',
                addressLocality = '',
                postalCode = '',
                addressCountry = '',
            } = address
            item.address = {
                addressType: (address['@type'] || '').toLowerCase(),
                streetAddress,
                addressLocality,
                addressRegion: addressLocality,
                postalCode,
                addressCountry,
            }
            item.address.townId = TownsListServices.getTownId(item.address.addressLocality)
        }

        if (!isEmpty(geo)) {
            const {
                latitude = '',
                longitude = '',
                zoom = 10,
            } = geo
            item.geolocation = {
                geoType: (geo['@type'] || '').toLowerCase(),
                latitude,
                longitude,
                zoom,
                searchString: item.address ? Object.values(item.address).join(' ') : ''
            }
        }

        item.attributes.push(aItem.string('priceRange', priceRange, { name: 'Средний чек' }))
        item.attributes.push(aItem.string('website', url, { name: 'Website' }))
        item.attributes.push(aItem.string('phone', telephone, { name: 'Phone' }))

        item.attributes.push(aItem.boolean('acceptsReservations', acceptsReservations === 'True', { name: 'Accepts Reservations' }))

        item.attributes.push(aItem.setMultiple('cuisine', servesCuisine, {
            name: 'Serves Cuisine',
            paramsData: { items: [], multiple: true }
        }))


        if (openingHoursSpecification.length) {
            item.openingHours = openingHoursSpecification.map(oh => ({
                dayOfWeek: oh.dayOfWeek,
                opens: oh.opens,
                closes: oh.closes,
            }))
        }

        if (image) {
            item.imageUrl = image
            fs.appendFileSync(imagesFile, image + '\n')
        }

        item.fullDescription = $('.introduce_wrapper').remove('.introduce_logo, img').html()

        $('.detail_columns .detail_column').toArray().forEach(detail => {
            const name = $('.detail_name', detail).text()
            if (name) {
                if (name === 'Средний чек') {
                    item.attributes.push(aItem.string('priceRange', $('.detail_sense', detail).text(), { name: 'Средний чек' }))
                } else {
                    item.attributes.push(aItem.string( slugify(name), $('.detail_sense', detail).text(), { name }))
                }
            }
        })


        $('.contact_name').each((i, element) => {
            if ($(element).text() === 'метро') {
                item.address.metro = $(element).parent().find('.contact_sense').text()
            }
        })

        const tags = []
        $(".rest-tags_link").remove('i').each((index, tag) => {
            tags.push($(tag).remove('i').text().trim())
        })
        item.attributes.push(aItem.directoryMultiple('tags', TagsListServices.updateTags(tags), {
            name: 'Tags Directory',
            isFilter: true,
            paramsType: 'directory',
            paramsData: {
                multiple: true,
                directory: 'tags',
            },
        }))


    } else {
        fs.appendFileSync( logFile, filename + '\n' )
        return false
    }

    if (item.title) {
        Collection.insert(item, {
            selector: {
                type: 'main'
            }
        })
        counter++
    }
}

const getAllFiles = dir =>
    fs.readdirSync(dir).reduce((files, file) => {
        const name = path.join(dir, file)
        const isDirectory = fs.statSync(name).isDirectory()
        return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name]
    }, [])

const readSource = (directory) => {
    counter = 0
    let ck  = 0
    const filesArray = getAllFiles(directory).filter( file => file.split('.').pop() == 'html' )

    filesArray.forEach((item) => {
        if ( ck < 1e5 ) {
            const data = fs.readFileSync(item, {
                encoding: 'utf8'
            })
            parseSome(data, item)
            ck++
        }
    })
    return filesArray.length
}

class AfishaServices {
    static clear() {
        Collection.remove({
            'attributes.attributeValue': 'afisha'
        })
    }

    static import() {
        this.clear()
        // AreasListServices.clear()
        TagsListServices.clear()
        TownsListServices.clear()
        AttributesListServices.clear()
        // TypesListServices.clear()
        fs.truncateSync(logFile, 0, function () {
        })
        fs.truncateSync(imagesFile, 0, function () {
        })
        const result = {
            files: readSource(srcDir),
            parsed: counter
        }
        return result
        // throw new Meteor.Error('import', 'import call bla bla bla')
    }

    static imagesList() {
        /** @description on future */
    }
}

export default AfishaServices
