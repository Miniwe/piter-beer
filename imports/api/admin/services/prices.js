
import { Meteor } from 'meteor/meteor'
import { SyncedCron } from 'meteor/littledata:synced-cron'
import map from 'lodash/map'
import Prices from '/imports/api/beers/prices'
import Locations from '/imports/api/locations'
import Beers from '/imports/api/beers'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'

const SYNC_TASK = 'SYNC_TASK'

class PricesServices {
    static clear() {
        Prices.remove({ })
    }

    static syncPrices() {
        const locations = {}
        const beers = {}
        Prices.find().fetch().map(item => {
            const { locationId, beerId, price } = item

            if (locations[locationId]) {
                locations[locationId].items.push({ beerId, price })
            } else {
                locations[locationId] = {
                    items: [{ beerId, price }]
                }
            }
            if (beers[beerId]) {
                if (price > 0) {
                    beers[beerId].total += price
                    beers[beerId].count += 1
                }

                beers[beerId].items.push({ locationId, price })
            } else {
                beers[beerId] = {
                    items: [{ locationId, price }],
                    total: price,
                    count: 1
                }
            }
        })

        map(locations, ({ items }, _id) => {
            Locations.update(_id, { $set: { beers: items } }, { selector: { type: 'main' } })
        })

        const price = AttributesListServices.getPriceAttr()
        map(beers, ({ items, total, count }, _id) => {
            const beer = Beers.findOne({ _id })
            if (beer) {
                const { attributes = [] } = beer
                const beerPriceIndex = attributes.findIndex(({ attributeId }) => attributeId === price._id)
                // const avgFixed = avg.toFixed(2)
                const avgFixed = Math.ceil(total / count)
                if (beerPriceIndex > -1) {
                    attributes[beerPriceIndex].attributeValue = avgFixed
                } else {
                    attributes.push({
                        attributeId: price._id,
                        attributeValue: avgFixed
                    })
                }
                Beers.update(_id, {
                    $set: {
                        attributes,
                        order: {
                            price: avgFixed
                        },
                        locations: items
                    }
                }, { selector: { type: 'main' } })
            }
        })
    }

    static addCronTask() {
        SyncedCron.remove(SYNC_TASK)
        SyncedCron.add({
            name: SYNC_TASK,
            schedule: parser => parser.text('every 1 min'),
            job() {
                PricesServices.syncPrices()
                SyncedCron.remove(SYNC_TASK)
            }
        })
    }
}

export default PricesServices
