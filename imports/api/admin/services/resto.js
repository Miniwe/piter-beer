import { Meteor } from 'meteor/meteor'
import isEmpty from 'lodash/isEmpty'
const cheerio = require('cheerio')
import fs from 'file-system'
import path from 'path'
import Collection from '/imports/api/locations'
import { ListServices as LocationsListServices } from '/imports/api/locations/services'
import { ListServices as TagsListServices } from '/imports/api/tags/services'
// import { ListServices as TypesListServices } from '/imports/api/types/services'
// import { ListServices as StatesListServices } from '/imports/api/states/services'
import { ListServices as TownsListServices } from '/imports/api/towns/services'
// import { ListServices as AreasListServices } from '/imports/api/areas/services'
import { slugify } from 'meteor/mcore/utils'
import * as aItem from './aItem'

// /**
//  * @description to call
//  *      Meteor.apply('import.locations', [], {noRetry: true, wait: true }, function(err, res) { c_onsole.log(err, res)})
//  */
let counter = 0
const logFile = '/Users/miniwe/Projects/moscow-beer-project/import/noprofile.log'
const srcDir = '/Users/miniwe/Projects/moscow-beer-project/import/msk.resto.ru/places/'
const imagesFile = '/Users/miniwe/Projects/moscow-beer-project/import/resto-images.txt'

var decodeHtmlEntity = function (str) {
    return str.replace(/&#(\d+);/g, function (match, dec) {
        return String.fromCharCode(dec)
    })
}

const parseSome = (content, filename) => {
    if (!content) {
        fs.appendFileSync(logFile, 'content error' + ' ' + filename)
        return false
    }
    const $ = cheerio.load(content, {xmlMode: true})
    const item = {
        filename,
        attributes: []
    }

    item.attributes.push(aItem.set('sourceType', 'resto', { name: 'Source Type' }))

    item.title = $("h1[itemprop='name']").text()
    item.alias = LocationsListServices.getAlias(slugify(item.title))

    item.attributes.push(aItem.set('locationType', slugify($('.b-place__info__item.info > a').first().text()), {
        name: 'Location Type'
    }))

    item.description = $('.description').text()
    item.fullDescription = $('.description').html()

    item.address = {
        addressType: 'postaladdress',
        streetAddress: $("span[itemprop='address']").text(),
        addressLocality: $('span.b-regions__current').text(),
        addressRegion: $('span.b-regions__current').text(),
        postalCode: '',
        addressCountry: 'RUS',
    }
    item.address.metro = $('.b-place-card .b-place__info__item.subway .subway-name').text()
    item.address.townId = item.address.addressLocality ? TownsListServices.getTownId(item.address.addressLocality) : ''

    const mapInfo = $('.place-info__map')
    item.geolocation = {
        geoType: 'geocoordinates',
        zoom: 10,
        searchString: item.address.streetAddress,
    }
    if (mapInfo) {
        Object.assign(item.geolocation, {
            latitude: mapInfo.attr('data-lat'),
            longitude: mapInfo.attr('data-lng'),

        })
    }

    item.attributes.push(aItem.string('priceRange', $('span[itemprop=priceRange]').text(), { name: 'Средний чек' }))

    item.attributes.push(aItem.string('website', $('span.js-place-website').attr('data-url'), { name: 'Website' }))
    item.attributes.push(aItem.string('phone', $('.b-place__info__item.phone span.phone-number').toArray().map(el => $(el).text()).join(', '), { name: 'Phone' }))

    item.attributes.push(aItem.directoryMultiple('tags', TagsListServices.updateTags($('.b-place__info__item.features-icons > a').toArray().map(el => $(el).attr('title'))), {
        name: 'Tags Directory',
        isFilter: true,
        paramsType: 'directory',
        paramsData: {
            multiple: true,
            directory: 'tags',
        },
    }))


    item.attributes.push(aItem.setMultiple('cuisine', $('.kitchen > a').toArray().map(el => $(el).text()), {
        name: 'Serves Cuisine',
        paramsData: {
            items: [],
            multiple: true
        }
    }))

    const openingHours = []
    item.openingHours = openingHours
    $('.weekdays').each((i, el) => {
        const wdData = $(el).html().split(':')
        const wdDataText = $(el).text().split(':')
        openingHours.push({
            dayOfWeek: $(`<span>${wdDataText[0]}</span>`).text(),
            opens: $(wdData[1]).text(),
            closes: $(wdData[2]).text(),
        })
    })

    item.attributes.push(aItem.string('source', filename, { name: 'Source (internal Use)' }))

    const image = $('.g-box__image img[itemprop=image]').attr('src')
    if (image) {
        item.imageUrl = image
        fs.appendFileSync(imagesFile, image + '\n')
    }

    if (item.title) {
        Collection.insert(item, {
            selector: {
                type: 'main'
            }
        })
        counter++
    }
}

const getAllFiles = dir =>
    fs.readdirSync(dir).reduce((files, file) => {
        const name = path.join(dir, file)
        const isDirectory = fs.statSync(name).isDirectory()
        return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name]
    }, [])

const readSource = (directory) => {
    counter = 0
    let ck  = 0
    const filesArray = getAllFiles(directory).filter( file => file.split('.').pop() == 'html' )

    filesArray.forEach((item) => {
        if ( ck < 1e5 ) {
            const data = fs.readFileSync(item, {
                encoding: 'utf8'
            })
            parseSome(data, item)
            ck++
        }
    })
    return filesArray.length
}

class RestoServices {
    static clear() {
        Collection.remove({
            'attributes.attributeValue': 'resto'
        })
    }

    static import() {
        this.clear()
        // AreasListServices.clear()
        // TagsListServices.clear()
        // TownsListServices.clear()
        // TypesListServices.clear()
        fs.truncateSync(logFile, 0, function () {
        })
        fs.truncateSync(imagesFile, 0, function () {
        })
        const result = {
            files: readSource(srcDir),
            parsed: counter
        }
        return result
        // throw new Meteor.Error('import', 'import call bla bla bla')
    }

    static imagesList() {
        /** @description on future */
    }
}

export default RestoServices
