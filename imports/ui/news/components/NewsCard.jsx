import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Card } from 'semantic-ui-react'
import { formatDate } from 'meteor/mcore/utils'

class NewsCard extends Component {
    render() {
        const {
            _id,
            alias,
            title,
            pubDate,
            isActive,
        } = this.props

        return (
            <Card
                as={Link}
                to={`/news/${alias}`}
                className="newsCard animated fadeInUp"
                {...(!isActive && {color: 'red'})}
            >
                <Card.Content>
                    <Card.Header className="smallHeader">
                        {title}
                    </Card.Header>
                    <Card.Meta textAlign="center">
                        {formatDate(pubDate)}
                    </Card.Meta>
                </Card.Content>
            </Card>
        )
    }
}

export default NewsCard
