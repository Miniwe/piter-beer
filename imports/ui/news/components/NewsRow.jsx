import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Table } from 'semantic-ui-react'
import { formatDate } from 'meteor/mcore/utils'

class NewsRow extends Component {
    render() {
        const { _id, title, alias, pubDate } = this.props
        return (
            <Table.Row>
                {/* <Table.Cell>{_id}</Table.Cell> */}
                <Table.Cell><Link to={`/news/${alias}`} className="animated fadeInLeft">{title}</Link></Table.Cell>
                <Table.Cell>{formatDate(pubDate)}</Table.Cell>
            </Table.Row>
        )
    }
}

export default NewsRow
