import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import NewsRow from './NewsRow'
import { ucfirst } from 'meteor/mcore/utils'

const cols = [
    'title',
    'pubDate',
]
class NewsTable extends Component {
    state = {
        column: 'title',
        direction: 'ascending',
    }

    isSorted = (col) => {
        const { column, direction } = this.state
        return column === col ? direction : null
    }

    handleSort = (clickedColumn) => {
        const { column, direction } = this.state
        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                direction: 'ascending',
            })

            return
        }

        this.setState({
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    sortItems = (a, b) => {
        const { column, direction } = this.state
        const getCol = this[`get${ucfirst(column)}`]
        if (column) {
            return direction === 'ascending' && getCol(a) > getCol(b) ? 1 : -1
        } else {
            return 0
        }
    }

    getTitle = (item) => {
        return item.title
    }

    render() {
        const { items } = this.props

        return (
            <Table sortable singleLine fixed>
                <Table.Header>
                    <Table.Row>
                        {cols.map(col => (
                            <Table.HeaderCell
                                key={col}
                                onClick={() => this.handleSort(col)}
                                sorted={this.isSorted(col)}
                            >{i18n.__(ucfirst(col))}</Table.HeaderCell>
                        ))}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {items.length > 0 ? items.sort(this.sortItems).map((item, index) => <NewsRow key={item._id} id={index + 1} {...item} />) : (
                        <Table.Row negative>
                            <Table.Cell colSpan={3}>
                                <NoItems />
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        )
    }
}

export default NewsTable
