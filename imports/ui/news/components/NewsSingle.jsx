import React, { Fragment } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Segment,
    Dimmer,
    Image,
    Container,
    Grid,
    Header,
    Loader,
    Divider,
} from 'semantic-ui-react'
import { formatDate } from 'meteor/mcore/utils'
import Texts from 'meteor/mcore/api/texts'
import News from '/imports/api/news'
import HelmetHead from '../../components/HelmetHead'
import Corner from '/imports/ui/components/Corner'
import BrandsList from '../../brands/components/BrandsList'

const NewsSingle = ({ loading, item = { title: i18n.__('N/A') } }) => {

    const {
        _id,
        title,
        description,
        pubDate,
        textId
    } = item
    const text = Texts.findOne(textId) || {}

    return !loading ? (
        <Fragment>
            <HelmetHead title={`${title} | ${i18n.__('News')}`} description={description} />
            <Segment className="skew-block skew-block--white next-skew mb-0">
                <Corner type="headerCorner" />
                <Container>
                    <Header className="singleTitle">
                        {title}
                        <Header.Subheader>
                            {formatDate(pubDate)}
                        </Header.Subheader>
                    </Header>
                    {description ? <Segment basic padded="very" secondary as="blockquote" dangerouslySetInnerHTML={{ __html: description }} /> : null}
                    <div dangerouslySetInnerHTML={{ __html: text ? text.text: '' }} />
                </Container>
            </Segment>
            <BrandsList
                query={{ news: _id }}
                showEmpty={false}
                maxCount={1}
                hasFilters={false}
            />
        </Fragment>
    ) : (
        <Segment className="skew-block skew-block--white next-skew mb-0">
            <Corner type="headerCorner" />
            <Container>
                <Dimmer inverted active>
                    <Loader />
                </Dimmer>
                <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
            </Container>
        </Segment>
    )
}

export default withTracker((props) => {
    const { id } = props
    const subscribers = [
        Meteor.subscribe('news.single', id)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = News.findOne({ $or: [{ _id: id }, { alias: id }] })
    return {
        ...props,
        loading,
        item,
    }
})(NewsSingle)
