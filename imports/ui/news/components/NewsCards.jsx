import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import NewsCard from './NewsCard'

class NewsCards extends Component {
    render() {
        const { items, itemsPerRow = 4, className } = this.props

        return items.length > 0 ? (
            <Card.Group itemsPerRow={itemsPerRow} doubling className={className}>
                {items.map(item => <NewsCard key={item._id} {...item} />)}
            </Card.Group>
        ) : <NoItems />
    }
}

export default NewsCards
