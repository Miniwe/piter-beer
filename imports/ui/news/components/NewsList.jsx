import React, { Component, createElement } from 'react'
import { Link } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Container, Segment, Header, Grid, Icon, Divider, Sidebar,
} from 'semantic-ui-react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import News from '/imports/api/news'
import { handlePage } from '/imports/redux/actions'
import { TRESHOLD, PAGE_SIZE } from '/imports/configs/constants'
import { getBodyHeight } from 'meteor/mcore/utils'
import Corner from '/imports/ui/components/Corner'
import Filters from '../../components/Filters'
import NewsNavbar from './NewsNavbar'
import NewsCards from './NewsCards'
import NewsTable from './NewsTable'
import { isMobile } from 'meteor/mcore/utils'

const MAX_MARKERS = 64
const listViews = {
    grid: {
        component: NewsCards,
        pageSize: 4,
    },
    table: {
        component: NewsTable,
        pageSize: 8,
    },
}

class NewsList extends Component {
    state = { visible: false }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false)
        this.onScroll()
    }

    componentWillUnmount() {
        const { handlePage } = this.props
        window.removeEventListener('scroll', this.onScroll, false)
        handlePage(0)
    }

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })

    onScroll = () => {
        const { handlePage, page, hasMore } = this.props
        const { innerHeight, scrollY } = window
        const bodyHeight = getBodyHeight()
        if ((innerHeight + scrollY) >= (bodyHeight - TRESHOLD) && hasMore) {
            handlePage(page + 1)
        }
    }

    render() {
        const {
            items,
            component = NewsCards,
            hasFilters = true,
            header = i18n.__('Our News'),
            showEmpty = true,
            withBrand
        } = this.props
        const { visible } = this.state


        return showEmpty || items.length ? (
            <Segment className="skew-block skew-block--lightYellow next-skew mb-0">
                <Corner type="headerCorner" />
                <Container>
                    {header ? (
                        <Header as="h2" className="sectionHeader" floated="left">
                            <Link to="/news">
                                {header}
                            </Link>
                        </Header>
                    ) : null}
                    <Divider hidden clearing />
                    {createElement(component, {
                        items,
                        itemsPerRow: hasFilters ? 3 : 4,
                        withBrand
                    })}
                </Container>
            </Segment>
        ) : null
    }
}

const mapDispatchToProps = dispatch => ({
    handlePage(page) { dispatch(handlePage('newsList', page)) },
})

const mapStateToProps = ({ app: { views, filters, pages } }, { constView = 'grid' }) => {
    const { component, pageSize = PAGE_SIZE } = listViews[constView || views.newsList]
    return ({
        filters: filters.newsList,
        view: constView || views.newsList,
        component,
        page: pages.newsList,
        pageSize,
    })
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withTracker((props) => {
        const {
            pageSize,
            maxCount,
            page,
            query = {},
            params = {},
        } = props
        let maxCountX = 1e5


        if (maxCount) {
            params.limit = maxCount
            maxCountX = maxCount
        } else {
            params.limit = pageSize * page
        }
        const subscribers = [
            Meteor.subscribe('news.list', query, params),
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const items = News.list(query, params).fetch()

        return {
            ...props,
            loading,
            hasMore: page * pageSize <= items.length && items.length < maxCountX,
            items,
        }
    }),
)(NewsList)
