import { renderRoutes } from 'react-router-config'
import NewsPage from './pages/NewsPage'
import NewsSinglePage from './pages/NewsSinglePage'

export {
    NewsPage,
    NewsSinglePage,
}

export default ({ route }) => renderRoutes(route.routes, route)
