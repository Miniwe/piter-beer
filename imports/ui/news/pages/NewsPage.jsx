import React, { Component, Fragment } from 'react'
import { ListServices as PagesListServices } from 'meteor/mcore/api/pages/services'
import { withTracker } from 'meteor/react-meteor-data'
import Pages from 'meteor/mcore/api/pages'
import NewsList from '../components/NewsList'
import HelmetHead from '../../components/HelmetHead'

class NewsPage extends Component {
    state = {}

    render() {
        const { item } = this.props
        return (
            <Fragment>
                {item ? <HelmetHead {...{ title: i18n.__('Our News'), ...PagesListServices.getHeadData('/news', item) }} /> : null}
                <NewsList view="grid" hasFilters />
            </Fragment>
        )
    }
}

export default withTracker((props) => {
    const alias = '/news'
    const subscribers = [
        Meteor.subscribe('pages.single', alias)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias })
    return {
        ...props,
        loading,
        item,
    }
})(NewsPage)
