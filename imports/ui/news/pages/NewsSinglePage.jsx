import React, { Component, Fragment } from 'react'
import NewsSingle from '../components/NewsSingle'
import HelmetHead from '../../components/HelmetHead'

class NewsPage extends Component {
    render() {
        const { match: { params }  = {}} = this.props

        return (
            <Fragment>
                <HelmetHead title={i18n.__('News Page')} />
                <NewsSingle {...params} />
            </Fragment>
        )
    }
}

export default NewsPage
