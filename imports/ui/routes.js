import MainLayout from './layouts/MainLayout'

import { AboutPage, StaticPage, ComparePage, IndexPage, NotFoundPage } from './pages'
import News, { NewsSinglePage, NewsPage } from './news'
import Beers, { BeerPage, BeersPage } from './beers'
import Brands, { BrandPage, BrandsPage } from './brands'
import Locations, { LocationPage, LocationsPage } from './locations'

const routes = [{
    component: MainLayout,
    routes: [{
        name: 'root',
        path: '/',
        exact: true,
        component: IndexPage,
        title: 'Index Page',
    },
    {
        name: 'about',
        path: '/about',
        exact: true,
        component: StaticPage,
        title: 'About Page',
    },
    {
        name: 'contacts',
        path: '/contacts',
        exact: true,
        component: StaticPage,
        title: 'Contacts',
    },
    {
        name: 'compare',
        path: '/compare',
        exact: true,
        component: ComparePage,
        title: 'Compare Page',
    },
    {
        name: 'beers',
        path: '/beers/:id?',
        component: Beers,
        title: 'Beers',
        routes: [{
            name: 'beer_page',
            exact: true,
            path: '/beers/:id',
            title: 'Beer',
            component: BeerPage,
        }, {
            name: 'beers_page',
            exact: true,
            path: '/beers',
            title: 'Beers',
            component: BeersPage,
        }]
    },
    {
        name: 'news',
        path: '/news/:id?',
        component: News,
        title: 'News',
        routes: [{
            name: 'news_single_page',
            exact: true,
            path: '/news/:id',
            title: 'News',
            component: NewsSinglePage,
        }, {
            name: 'news_page',
            exact: true,
            path: '/news',
            title: 'News',
            component: NewsPage,
        }]
    },
    {
        name: 'brands',
        path: '/brands/:id?',
        component: Brands,
        title: 'Brands',
        routes: [{
            name: 'brand_page',
            exact: true,
            path: '/brands/:id',
            title: 'Brand',
            component: BrandPage,
        }, {
            name: 'brands_page',
            exact: true,
            path: '/brands',
            title: 'Brands',
            component: BrandsPage,
        }]
    },
    {
        name: 'locations',
        path: '/locations/:id?',
        component: Locations,
        title: 'Locations',
        routes: [{
            name: 'location_page',
            exact: true,
            path: '/locations/:id',
            title: 'Location',
            component: LocationPage,
        }, {
            name: 'locations_page',
            exact: true,
            path: '/locations',
            title: 'Locations',
            component: LocationsPage,
        }]
    },
    {
        name: 'page_not_found',
        path: '/*',
        exact: true,
        component: NotFoundPage,
        title: 'Page Not Found',
    }],
}]

export default routes
