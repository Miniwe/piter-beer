import React, { Component } from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { handleView } from '/imports/redux/actions'

class BeersNavbar extends Component {
    render() {
        const { handleView } = this.props

        return (
            <Menu secondary tabular size="tiny">
                <Menu.Item onClick={() => handleView('grid')}>
                    <Icon name="grid layout" />
                    {i18n.__('Grid')}
                </Menu.Item>
                <Menu.Item onClick={() => handleView('table')}>
                    <Icon name="table" />
                    {i18n.__('Table')}
                </Menu.Item>
                {/* <Menu.Item onClick={() => handleView('map')}>
                    <Icon name="map marker alternate" />
                    {i18n.__('Map')}
                </Menu.Item> */}
            </Menu>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    handleView(mode) { dispatch(handleView('beersList', mode)) },
})

export default connect(null, mapDispatchToProps)(BeersNavbar)
