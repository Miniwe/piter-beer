import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import findIndex from 'lodash/findIndex'
import pick from 'lodash/pick'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'
import isEqual from 'lodash/isEqual'
import find from 'lodash/find'
import { Header, Table, Button, Icon, Segment } from 'semantic-ui-react'
import Attributes from 'meteor/mcore/api/attributes'
import { compareBeer } from '/imports/redux/actions'
import * as Fields from '../../components/Fields'
import NoItems from '../../components/NoItems'

const beerFields = pick(Fields, [
    'mainBeerImage',
    'description',
    'brandTitle',
    'locationsList',
    'website',
])

class BeersCompare extends Component {
    getFields = (items) => {
        const fields = []
        const fieldsList = Object.keys(beerFields)
        items.forEach((item) => {
            fieldsList.forEach((key) => {
                const fieldIndex = findIndex(fields, { key })
                const beerField = beerFields[key]

                if (!isEmpty(item[beerField.itemKey])) {
                    if (fieldIndex > -1) {
                        fields[fieldIndex].count += isEqual(fields[fieldIndex].value, item[beerField.itemKey]) ? 1 : 0
                    } else {
                        fields.push({
                            key,
                            value: item[beerField.itemKey],
                            count: 1,
                            title: beerField.label,
                            renderer: beerField.renderer,
                        })
                    }
                }
            })
        })
        return fields
    }

    getAttrs = (items) => {
        const attrs = []
        items.forEach(({ attributes }) => {
            attributes.forEach(({ attributeId, attributeValue }) => {
                const attrIndex = findIndex(attrs, { attributeId })
                if (attributeValue) {
                    if (attrIndex > -1) {
                        attrs[attrIndex].count += isEqual(attrs[attrIndex].attributeValue, attributeValue) ? 1 : 0
                        //  && isEqual(attrs[attributeId].value, attributeValue)
                    } else {
                        const attrModel = Attributes.findOne(attributeId)
                        if (attrModel && attrModel.isFilter) {
                            attrs.push({
                                attributeId,
                                attributeValue,
                                count: 1,
                                isFilter: attrModel ? attrModel.isFilter : false,
                                title: attrModel ? attrModel.title : attributeId,
                                renderer: Fields.attributeRenderer(attrModel)
                            })
                        }
                    }
                }
            })
        })
        return sortBy(attrs, ['isFilter', 'title'])
    }

    render() {
        const { items, compareBeer } = this.props
        const beerFields = this.getFields(items)
        const attrsFields = this.getAttrs(items)

        return items.length > 0 ? (
            <Table basic="very" columns={items.length + 1} className="compareTable">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="3">{' '}</Table.HeaderCell>
                        {items.map(({ _id, alias, title }) => (
                            <Table.HeaderCell key={_id}>
                                <Segment basic clearing>
                                    <Header as="h3" floated="left">
                                        <Link to={`/beers/${alias}`}>{title}</Link>
                                    </Header>
                                    <Button floated="right" size="mini" basic circular icon="remove" title={'Compare'} onClick={event => compareBeer(event, _id)} />
                                </Segment>
                            </Table.HeaderCell>))}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {/* map(beerFields, ({ key, title, count, renderer }) => (
                        <Table.Row key={key} active={count !== items.length}>
                            <Table.HeaderCell>{title}</Table.HeaderCell>
                            {items.map((item) => {
                                return (
                                    <Table.Cell key={`${item._id}_${key}`}>
                                        {renderer(item)}
                                    </Table.Cell>
                                )
                            })}
                        </Table.Row>
                    )) */}
                    {map(attrsFields, ({ attributeId, title, count, isFilter, renderer }) => (
                        <Table.Row key={attributeId} active={count !== items.length}>
                            <Table.HeaderCell>
                                {isFilter && <Icon name="filter" size="small" color="yellow" />}
                                { title }
                            </Table.HeaderCell>
                            {items.map(({ _id, attributes, }, key) => {
                                const itemAttr = find(attributes, { attributeId })
                                return (
                                    <Table.Cell key={`${_id}_${attributeId}_${key}`}>
                                        {itemAttr ? renderer(itemAttr.attributeValue) : null}
                                    </Table.Cell>
                                )
                            })}
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        ) : <NoItems message={i18n.__('Nothing to compare')} />
    }
}

const mapDispatchToProps = dispatch => ({
    compareBeer(event, id) {
        event.preventDefault()
        dispatch(compareBeer(id))
    }
})

export default connect(null, mapDispatchToProps)(BeersCompare)
