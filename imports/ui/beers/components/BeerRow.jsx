import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Table } from 'semantic-ui-react'
import {
    brand as brandField,
    price as priceField,
} from '../../components/Fields'

class BeerRow extends Component {
    render() {
        const {
            _id,
            title,
            alias,
            brandId,
            order,
            attributes,
            withLocation,
            locations = []
        } = this.props

        const brand = brandField.renderer({ brandId })
        const price = priceField.renderer({ attributes, order })
        const location = withLocation ? locations.find(({locationId}) => locationId === withLocation) : false
        if (!brand._id) {
            Object.assign(brand, { title: brandId } )
        }
        return (
            <Table.Row>
                {/* <Table.Cell>{_id}</Table.Cell> */}
                <Table.Cell><Link to={`/beers/${alias}`} className="animated fadeInLeft">{title}</Link></Table.Cell>
                <Table.Cell>{brand.title}</Table.Cell>
                <Table.Cell>{location && location.price ? location.price : price.value} {price.units}</Table.Cell>
            </Table.Row>
        )
    }
}

export default BeerRow
