import React, { Component } from 'react'
import { Table } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import BeerRow from './BeerRow'
import { ucfirst } from 'meteor/mcore/utils'
import Brands from '/imports/api/brands'

const cols = [
    'title',
    'brand',
    'price'
]
class BeersTable extends Component {
    state = {
        column: 'title',
        direction: 'ascending',
    }

    isSorted = (col) => {
        const { column, direction } = this.state
        return column === col ? direction : null
    }

    handleSort = (clickedColumn) => {
        const { column, direction } = this.state
        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                direction: 'ascending',
            })

            return
        }

        const newDirection = direction === 'ascending' ? 'descending' : 'ascending'
        this.setState({
            direction: newDirection
        })
    }

    sortItems = (a, b) => {
        const { column, direction } = this.state
        const getCol = this[`get${ucfirst(column)}`]
        const condition = direction === 'ascending' ? getCol(a) > getCol(b) : getCol(a) < getCol(b)
        return condition ? 1 : -1
    }

    getTitle = (item) => {
        return item.title
    }

    getPrice = (item) => {
        const priceId = 'XPgTC3DJ7FsAt479q'
        const atr = item.attributes.find(({ attributeId }) => attributeId === priceId)
        return atr ? atr.attributeValue : null
    }

    getBrand = (item) => {
        const brand = Brands.findOne(item.brandId)
        return brand ? brand.title : item.brandId
    }

    render() {
        const { items, withLocation, direction } = this.props
        const sorted = items.sort(this.sortItems)
        return (
            <Table sortable singleLine fixed>
                <Table.Header>
                    <Table.Row>
                        {cols.map(col => (
                            <Table.HeaderCell
                                key={col}
                                onClick={() => this.handleSort(col)}
                                sorted={this.isSorted(col)}
                            >{i18n.__(ucfirst(col))}</Table.HeaderCell>
                        ))}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {sorted.length > 0 ? sorted.map((item, index) => (
                        <BeerRow
                            withLocation={withLocation}
                            key={item._id}
                            id={`row_${index + 1}`}
                            {...item}
                        />
                    )) : (
                        <Table.Row negative>
                            <Table.Cell colSpan={3}>
                                <NoItems />
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        )
    }
}

export default BeersTable
