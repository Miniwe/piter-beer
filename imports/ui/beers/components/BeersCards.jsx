import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import BeerCard from './BeerCard'

class BeersCards extends Component {
    state = {}

    render() {
        const { items, withLocation, itemsPerRow = 4, className, withImg } = this.props
        return items.length > 0 ? (
            <Card.Group itemsPerRow={itemsPerRow} doubling className={className}>
                {items.map(item => (
                    <BeerCard
                        withLocation={withLocation}
                        withImg={withImg}
                        key={item._id}
                        {...item}
                    />
                ))}
            </Card.Group>
        ) : <NoItems />
    }
}

export default BeersCards
