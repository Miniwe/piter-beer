import React, { Component, Fragment } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import { Link } from 'react-router-dom'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import {
    Segment,
    Dimmer,
    Image,
    Container,
    Grid,
    Header,
    Loader,
    Divider,
    Statistic,
    Table,
    Button,
} from 'semantic-ui-react'
import Swiper from 'react-id-swiper'
import { compareBeer } from '/imports/redux/actions'
import Beers from '/imports/api/beers'
import Attributes from 'meteor/mcore/api/attributes'
import {
    brand as brandField,
    mainImageLink as mainImageLinkField,
    imagesList as imagesListField,
} from '../../components/Fields'
import LocationsList from '../../locations/components/LocationsList'
import HelmetHead from '../../components/HelmetHead'
import Comments from '../../components/Comments'
import Corner from '/imports/ui/components/Corner'
import { attributeRenderer } from '../../components/Fields'
import { imagesSwiperParams } from '/imports/configs/constants'
import BeersList from '../components/BeersList'

class BeerSingle extends Component {
    renderAttributes = (_id, attrs) => {
        return attrs.length > 0 ? (
            <Table basic="very" columns={2} className="compareTable">
                <Table.Body>
                    {attrs.map(({ attributeId, attributeValue }) => {
                        const attribute = Attributes.findOne(attributeId)
                        return attribute && attribute.isActive ? {
                            ...attribute,
                            attributeValue
                        } : null
                    }).filter(i => i).sort((a, b) => a.order - b.order).map((attribute, key) => (
                        <Table.Row key={`${_id}_${attribute._id}_${key}`}>
                            <Table.HeaderCell>{attribute.title}</Table.HeaderCell>
                            <Table.Cell>{attributeRenderer(attribute)(attribute.attributeValue)}{' '}</Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        ) : null
    }

    render() {
        const { loading, inCompare, compareBeer, item = { title: 'N/A'} } = this.props
        const {
            _id,
            title,
            locations = [],
            description,
            price,
            website,
            brandId,
            images = [],
            isComments,
            imageUrl,
            attributes = [],
        } = item

        const brandModel = brandField.renderer({ brandId })
        const mainImageLink = mainImageLinkField.renderer({ imageUrl, images })
        const imagesList = imagesListField.renderer({ images })
        const locationsIds = locations.map(({locationId}) => locationId)
        return (
            <Fragment>
                <HelmetHead title={`${title} | ${i18n.__('Beers')}`} />
                {!loading ? (
                    <Fragment>
                        <Segment className="skew-block skew-block--white next-skew mb-0">
                            <Corner type="headerCorner" />
                            <Container>
                                <Grid stackable>
                                    <Grid.Column width={6} textAlign="center">
                                        <Statistic size="mini">
                                            {brandModel.imageUrl ? (
                                                <Fragment>
                                                    <Link to={`/brands/${brandModel.alias}`}>
                                                        <Image size="small" src={brandModel.imageUrl} />
                                                    </Link>
                                                    <Statistic.Label as={Link} to={`/brands/${brandModel.alias}`}>
                                                        {brandModel.title}
                                                    </Statistic.Label>
                                                </Fragment>
                                            ) : (
                                                <Statistic.Value as={Link} to={`/brands/${brandModel.alias}`}>{brandModel.title}</Statistic.Value>
                                            )}
                                        </Statistic>
                                        <Divider section hidden />
                                        {images.length > 1 ? (
                                            <Swiper {...imagesSwiperParams}>
                                                {imagesList.map(image => image ? (
                                                    <div className="swiper-slide-1" key={image._id}>
                                                        <Image centered size="medium" src={image.imageUrl} />
                                                    </div>
                                                ) : null)}
                                            </Swiper>
                                        ) : mainImageLink ? <Image centered size="medium" src={mainImageLink} /> : null}

                                        {price > 0 ? (
                                            <Fragment>
                                                <Divider />
                                                <Statistic>
                                                    <Statistic.Value>{price}</Statistic.Value>
                                                    <Statistic.Label>{i18n.__('price.units')}</Statistic.Label>
                                                </Statistic>
                                            </Fragment>
                                        ) : null}
                                    </Grid.Column>
                                    <Grid.Column width={10}>
                                        <Header className="singleTitle" floated="left">
                                            {title}
                                            {website ? (
                                                <Header.Subheader as="a" href={website} target="_blank">
                                                     {website}
                                                </Header.Subheader>
                                            ) : null}
                                        </Header>
                                        <Button
                                            circular
                                            floated="right"
                                            className="compare"
                                            basic
                                            primary={!inCompare}
                                            icon={inCompare ? 'remove' : 'hand pointer outline'}
                                            onClick={compareBeer}
                                        />

                                        <Divider hidden clearing />
                                        <div className="mainText" dangerouslySetInnerHTML={{ __html: description }} />
                                        {this.renderAttributes(_id, attributes)}
                                    </Grid.Column>
                                </Grid>
                            </Container>
                        </Segment>
                        {/* <BeersList
                            constView="grid"
                            hasFilters={false}
                            header={i18n.__('Similar')}
                            query={{ similar: [_id] }}
                            maxCount={8}
                            withImg
                        /> */}
                        <LocationsList
                            withBeer={_id} query={{_id: {$in: locationsIds}}}
                            hasFilters
                            header={i18n.__('LocationsForBeer')}
                        />

                        {isComments ? (
                            <Segment className="skew-block skew-block--gray next-skew mb-0">
                                <Corner type="headerCorner" />
                                <Container>
                                    <Comments />
                                </Container>
                            </Segment>
                        ) : null}
                    </Fragment>
                ) : (
                    <Segment className="skew-block skew-block--white next-skew mb-0">
                        <Corner type="headerCorner" />
                        <Container>
                            <Dimmer inverted active>
                                <Loader />
                            </Dimmer>
                            <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                        </Container>
                    </Segment>
                )}
            </Fragment>
        )
    }
}

const mapStateToProps = ({ app: { comparedBeers } }, { item: { _id } = {} }) => ({
    inCompare: comparedBeers.indexOf(_id) > -1,
})

const mapDispatchToProps = (dispatch, { item: { _id } = {} }) => ({
    compareBeer(event) {
        event.preventDefault()
        dispatch(compareBeer(_id))
    },
})

export default compose(
    withTracker((props) => {
        const { id } = props
        const subscribers = [
            Meteor.subscribe('beers.single', id)
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const item = Beers.findOne({ $or: [{ _id: id }, { alias: id }] })
        return {
            ...props,
            loading,
            item,
        }
    }),
    connect(mapStateToProps, mapDispatchToProps)
)(BeerSingle)
