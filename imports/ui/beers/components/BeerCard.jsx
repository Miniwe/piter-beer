import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    Card, Statistic, Image,
} from 'semantic-ui-react'
import {
    brand as brandField,
    price as priceField,
    mainImageLink as mainImageLinkField,
    color as colorField,
} from '../../components/Fields'
// import Rating from '../../components/Rating'
import NoBeer from '../../components/NoBeer'

class BeerCard extends Component {
    state = {}

    render() {
        const {
            _id,
            alias,
            title,
            isActive,
            brandId,
            brandTitle = '',
            attributes,
            withLocation,
            imageUrl,
            images = [],
            order,
            withImg = false,
            locations=[],
        } = this.props

        const brand = brandField.renderer({ brandId })
        const price = priceField.renderer({ attributes, order })
        const mainImageLink = mainImageLinkField.renderer({ imageUrl, images })
        const color = colorField.renderer({ attributes })

        const location = withLocation ? locations.find(({ locationId }) => locationId === withLocation) : false
        return (
            <Card
                className="beerCard animated fadeInUp"
                {...(!isActive && {color: 'red'})}
            >
                {withImg ? <Link to={`/beers/${alias}`}>
                    {mainImageLink ? <Image centered src={mainImageLink} /> : <NoBeer {...color} />}
                </Link> : null}
                <Card.Content textAlign="center">

                    <Card.Header as={Link} to={`/beers/${alias}`}>
                        {title}
                    </Card.Header>
                    {/* <Card.Meta className="rating-container">
                        <Rating itemId={_id} />
                    </Card.Meta> */}
                    <Statistic size="mini">
                        <Statistic.Label as={Link} to={`/brands/${brandId}`} className="brand">{brand.title || brandTitle}</Statistic.Label>
                        <Statistic.Value className="price">
                            {location && location.price ? location.price : price.value} {price.units || '₽'}</Statistic.Value>
                    </Statistic>
                </Card.Content>
            </Card>
        )
    }
}

export default BeerCard
