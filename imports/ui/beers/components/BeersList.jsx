import React, { Component, createElement } from 'react'
import { Link } from 'react-router-dom'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Container, Segment, Header, Grid, Icon, Divider, Sidebar
} from 'semantic-ui-react'
import Beers from '/imports/api/beers'
import { handlePage } from '/imports/redux/actions'
import { TRESHOLD, PAGE_SIZE } from '/imports/configs/constants'
import { getBodyHeight } from 'meteor/mcore/utils'
import Corner from '/imports/ui/components/Corner'
import Locations from '/imports/api/locations'
import Filters from '../../components/Filters'
import BeersNavbar from './BeersNavbar'
import BeersCards from './BeersCards'
import BeersTable from './BeersTable'
import BeersCompare from './BeersCompare'
import LocationsMap from '../../locations/components/LocationsMap'
import { isMobile } from 'meteor/mcore/utils'

const MAX_MARKERS = 64
const listViews = {
    grid: {
        component: BeersCards,
        pageSize: 8,
    },
    table: {
        component: BeersTable,
        pageSize: 12,
    },
    index: {
        component: BeersCards,
        pageSize: 3,
    },
    compare: {
        component: BeersCompare,
        pageSize: 4,
    },
    map: {
        component: LocationsMap,
        pageSize: MAX_MARKERS,
    },
}

class BeersList extends Component {
    state = { visible: false }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false)
        this.onScroll()
    }

    componentWillUnmount() {
        const { handlePage } = this.props
        window.removeEventListener('scroll', this.onScroll, false)
        handlePage(0)
    }

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })

    onScroll = () => {
        const { handlePage, page, hasMore } = this.props
        const { innerHeight, scrollY } = window
        const bodyHeight = getBodyHeight()

        if ((innerHeight + scrollY) >= (bodyHeight - TRESHOLD) && hasMore) {
            handlePage(page + 1)
        }
    }

    getLocationsItems = (items) => {
        const locationsIds = items.reduce((list, { locations = [] }) => [...list, ...locations.map(({ locationId }) => locationId)], [])
        const lItems = Locations.find({ _id: { $in: locationsIds } }).fetch()
        return lItems
    }

    render() {
        const {
            items,
            view,
            withImg = false,
            withLocation,
            component = BeersCards,
            hasFilters = true,
            header = i18n.__('BeersInPiter'),
            showEmpty = true
        } = this.props
        const { visible } = this.state

        return showEmpty || items.length ? (
            <Segment className="skew-block skew-block--yellow next-skew mb-0">
                <Corner type="headerCorner" />
                <Sidebar.Pushable as="div">
                    {isMobile() && hasFilters ? (
                        <Sidebar
                            as={Segment}
                            animation="overlay"
                            direction="right"
                            icon="labeled"
                            inverted
                            onHide={this.handleSidebarHide}
                            vertical
                            visible={visible}
                            className="skew-block skew-block--yellow"
                        >
                            <div style={{ textAlign: 'right' }}>
                                <Icon name="close" size="large" link onClick={this.handleHideClick} />
                            </div>
                            <BeersNavbar />
                            <Filters group="beers" textSearch />
                        </Sidebar>
                    ) : null}
                    <Sidebar.Pusher>
                        <Container>
                            {isMobile() && hasFilters ? (
                                <Header icon as="h6" floated="right">
                                    <Icon name="filter" link onClick={this.handleShowClick} />
                                </Header>
                            ) : null}
                            {header ? (
                                withLocation ? (
                                    <Header as="h2" className="sectionHeader darkText" floated="left">
                                        {header}
                                        <Link to="/beers">
                                            <Header.Subheader>
                                                {i18n.__('All Beers')}
                                            </Header.Subheader>
                                        </Link>
                                    </Header>
                                ) : (
                                    <Header as="h2" className="sectionHeader darkText" floated="left">
                                        <Link to="/beers">
                                            {header}
                                        </Link>
                                    </Header>
                                )
                            ) : null}
                            <Divider section hidden clearing />
                            <Grid>
                                <Grid.Column width={hasFilters ? isMobile() ? 16 : 12 : 16 }>
                                    {createElement(component, {
                                        items: view === 'map' ? this.getLocationsItems(items) : items,
                                        itemsPerRow: hasFilters ? 3 : 4,
                                        withLocation,
                                        withImg
                                    })}
                                </Grid.Column>
                                {hasFilters && !isMobile() ? (
                                    <Grid.Column width={4} className="listFiltersCol">
                                        <BeersNavbar />
                                        <Filters group="beers" textSearch />
                                    </Grid.Column>
                                ) : null}
                            </Grid>
                        </Container>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </Segment>
        ) : null
    }
}

const mapDispatchToProps = dispatch => ({
    handlePage(page) { dispatch(handlePage('beersList', page)) },
})

const mapStateToProps = ({ app: { views, filters, pages } }, { constView }) => {
    const { component, pageSize = PAGE_SIZE } = listViews[constView || views.beersList]
    return ({
        filters: filters.beersList,
        view: constView || views.beersList,
        component,
        page: pages.beersList,
        pageSize,
    })
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withTracker((props) => {
        let {
            pageSize = 8,
            maxCount,
        } = props
        const {
            page,
            view,
            query = { },
            params = {},
            filters: { keyword, ...attributes },
            hasFilters = true,
        } = props
        let maxCountX = 1e5

        query.isActive = true
        if (hasFilters) {
            if (keyword) {
                query.keyword = keyword
            }
            if (attributes) {
                query.attributes = Object.assign({}, query.attributes, attributes)
            }
        }

        if (view === 'map') {
            pageSize = MAX_MARKERS
            maxCount = pageSize
        }

        if (maxCount) {
            params.limit = maxCount
            maxCountX = maxCount
        } else {
            params.limit = pageSize * page
        }
        const subscribers = [
            Meteor.subscribe('beers.list', query, params),
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const items = Beers.list(query, params).fetch()
        const hasMore = page * pageSize <= items.length && items.length < maxCountX

        return {
            ...props,
            loading,
            hasMore,
            items
        }
    }),
)(BeersList)

