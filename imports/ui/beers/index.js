import { renderRoutes } from 'react-router-config'
import BeersPage from './pages/BeersPage'
import BeerPage from './pages/BeerPage'

export {
    BeersPage,
    BeerPage,
}

export default ({ route }) => renderRoutes(route.routes, route)
