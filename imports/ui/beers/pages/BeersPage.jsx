import React, { Component, Fragment } from 'react'
import { ListServices as PagesListServices } from 'meteor/mcore/api/pages/services'
import { withTracker } from 'meteor/react-meteor-data'
import Pages from 'meteor/mcore/api/pages'
import BeersList from '../components/BeersList'
import HelmetHead from '../../components/HelmetHead'

class BeersPage extends Component {
    state = {}

    render() {
        const { item } = this.props
        return (
            <Fragment>
                {item ? <HelmetHead {...{ title: i18n.__('BeersInPiter'), ...PagesListServices.getHeadData('/beers', item) }} /> : null}
                <BeersList withImg view="table" hasFilters params={{ sort: 'order.price', order: 1 }} />
            </Fragment>
        )
    }
}

export default withTracker((props) => {
    const alias = '/beers'
    const subscribers = [
        Meteor.subscribe('pages.single', alias)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias })
    return {
        ...props,
        loading,
        item,
    }
})(BeersPage)
