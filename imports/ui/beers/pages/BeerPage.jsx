import React, { Component, Fragment } from 'react'
import BeerSingle from '../components/BeerSingle'
import HelmetHead from '../../components/HelmetHead'

class BeerPage extends Component {
    render() {
        const { match: { params }  = {}} = this.props

        return (
            <Fragment>
                <HelmetHead title={i18n.__('Beer Page')} />
                <BeerSingle {...params} />
            </Fragment>
        )
    }
}

export default BeerPage
