import React, { Component, Fragment } from 'react'
import LocationSingle from '../components/LocationSingle'
import HelmetHead from '../../components/HelmetHead'

class LocationsPage extends Component {
    render() {
        const { match: { params }  = {}} = this.props

        return (
            <Fragment>
                <HelmetHead title={i18n.__('Location Page')} />
                <LocationSingle {...params} />
            </Fragment>
        )
    }
}

export default LocationsPage
