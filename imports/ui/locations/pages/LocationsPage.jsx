import React, { Component, Fragment } from 'react'
import { ListServices as PagesListServices } from 'meteor/mcore/api/pages/services'
import { withTracker } from 'meteor/react-meteor-data'
import Pages from 'meteor/mcore/api/pages'
import LocationsList from '../components/LocationsList'
import HelmetHead from '../../components/HelmetHead'

class LocationsPage extends Component {
    state = {}

    render() {
        const { item } = this.props
        return (
            <Fragment>
                {item ? <HelmetHead {...{ title: i18n.__('PlacesList'), ...PagesListServices.getHeadData('/locations', item) }} /> : null}

                <LocationsList view="table" />
            </Fragment>
        )
    }
}

export default withTracker((props) => {
    const alias = '/locations'
    const subscribers = [
        Meteor.subscribe('pages.single', alias)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias })
    return {
        ...props,
        loading,
        item,
    }
})(LocationsPage)
