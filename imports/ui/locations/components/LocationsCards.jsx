import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import LocationCard from './LocationCard'

class LocationsCards extends Component {
    render() {
        const {
            items,
            itemsPerRow = 4,
            className,
            mode,
            withBeer
        } = this.props

        return items.length > 0 ? (
            <Card.Group itemsPerRow={itemsPerRow} doubling className={className}>
                {items.map(item => <LocationCard key={item._id} {...item} mode={mode} withBeer={withBeer} />)}
            </Card.Group>
        ) : <NoItems />
    }
}

export default LocationsCards
