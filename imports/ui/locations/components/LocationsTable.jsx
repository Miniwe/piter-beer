import React, { Component } from 'react'
import pick from 'lodash/pick'
import { Table } from 'semantic-ui-react'
import NoItems from '/imports/ui/components/NoItems'
import LocationRow from './LocationRow'
import { ucfirst } from 'meteor/mcore/utils'


const cols = [
    'title',
    'location',
    'metro'
]
class LocationTable extends Component {
    state = {
        column: 'title',
        direction: 'ascending',
    }

    isSorted = (col) => {
        const { column, direction } = this.state
        return column === col ? direction : null
    }

    handleSort = (clickedColumn) => {
        const { column, direction } = this.state
        if (column !== clickedColumn) {
            this.setState({
                column: clickedColumn,
                direction: 'ascending',
            })

            return
        }

        this.setState({
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }

    sortItems = (a, b) => {
        const { column, direction } = this.state
        const getCol = this[`get${ucfirst(column)}`]
        const condition = direction === 'ascending' ? getCol(a) > getCol(b) : getCol(a) < getCol(b)
        return condition ? 1 : -1
    }

    getTitle = (item) => {
        return item.title
    }

    getPrice = (item) => {
        const priceId = 'XPgTC3DJ7FsAt479q'
        const atr = item.attributes.find(({ attributeId }) => attributeId === priceId)
        return atr ? atr.attributeValue : null
    }

    getLocation = (item) => {
        const { address = {} } = item
        const addressShort = pick(address, [
            'streetAddress',
            'postalCode',
            'addressLocality',
        ])
        return Object.values(addressShort).join(' ')
    }

    getMetro = (item) => {
        const { address = {} } = item
        return address.metro
    }

    render() {
        const { items, withBeer } = this.props

        return (
            <Table sortable singleLine fixed>
                <Table.Header>
                    <Table.Row>
                        {cols.map(col => (
                            <Table.HeaderCell
                                key={col}
                                onClick={() => this.handleSort(col)}
                                sorted={this.isSorted(col)}
                            >{i18n.__(ucfirst(col))}</Table.HeaderCell>
                        ))}
                        {withBeer ? <Table.HeaderCell onClick={() => this.handleSort('price')}
                            sorted={this.isSorted('price')}>{i18n.__('Price')}</Table.HeaderCell> : null}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {items.length > 0 ? items.sort(this.sortItems).map((item, index) => <LocationRow withBeer={withBeer} key={item._id} id={index + 1} {...item} />) : (
                        <Table.Row negative>
                            <Table.Cell colSpan={2}>
                                <NoItems />
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        )
    }
}

export default LocationTable
