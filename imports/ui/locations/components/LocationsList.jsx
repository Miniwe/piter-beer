import React, { Component, createElement } from 'react'
import { Link } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Container, Segment, Header, Grid, Icon, Divider, Sidebar,
} from 'semantic-ui-react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import Locations from '/imports/api/locations'
import { handlePage } from '/imports/redux/actions'
import { TRESHOLD, PAGE_SIZE } from '/imports/configs/constants'
import { getBodyHeight } from 'meteor/mcore/utils'
import Corner from '/imports/ui/components/Corner'
import Filters from '../../components/Filters'
import LocationsNavbar from './LocationsNavbar'
import LocationsCards from './LocationsCards'
import LocationsTable from './LocationsTable'
import LocationsMap from './LocationsMap'
import { isMobile } from 'meteor/mcore/utils'

const MAX_MARKERS = 64
const listViews = {
    grid: {
        component: LocationsCards,
        pageSize: 8,
    },
    table: {
        component: LocationsTable,
        pageSize: 16,
    },
    map: {
        component: LocationsMap,
        pageSize: MAX_MARKERS,
    },
}

class LocationsList extends Component {
    state = { visible: false }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false)
        this.onScroll()
    }

    componentWillUnmount() {
        const { handlePage } = this.props
        window.removeEventListener('scroll', this.onScroll, false)
        handlePage(0)
    }

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })

    onScroll = () => {
        const { handlePage, page, hasMore } = this.props
        const { innerHeight, scrollY } = window
        const bodyHeight = getBodyHeight()
        if ((innerHeight + scrollY) >= (bodyHeight - TRESHOLD) && hasMore) {
            handlePage(page + 1)
        }
    }

    render() {
        const {
            items,
            component = LocationsCards,
            hasFilters = true,
            header = i18n.__('PlacesList'),
            showEmpty = true,
            withBeer
        } = this.props
        const { visible } = this.state

        return showEmpty || items.length ? (
            <Segment className="skew-block skew-block--green next-skew mb-0">
                <Corner type="headerCorner" />
                <Sidebar.Pushable as="div">
                    {isMobile() && hasFilters ? (
                        <Sidebar
                            as={Segment}
                            animation="overlay"
                            direction="right"
                            icon="labeled"
                            inverted
                            onHide={this.handleSidebarHide}
                            vertical
                            visible={visible}
                            className="skew-block skew-block--green"
                        >
                            <div style={{ textAlign: 'right' }}>
                                <Icon name="close" size="large" link onClick={this.handleHideClick} style={{ color: '#fff' }} />
                            </div>
                            <LocationsNavbar />
                            <Filters group="locations" mode="light" textSearch />
                        </Sidebar>
                    ) : null}
                    <Sidebar.Pusher>
                        <Container>
                            {isMobile() && hasFilters ? (
                                <Header icon as="h6" floated="right">
                                    <Icon name="filter" link onClick={this.handleShowClick} style={{ color: '#fff'}}/>
                                </Header>
                            ) : null}
                            {header ? (
                                withBeer ? (
                                    <Header as="h2" className="sectionHeader lightText" floated="left">
                                        {header}
                                        <Link to="/locations">
                                            <Header.Subheader style={{ color: 'inherit' }}>
                                                {i18n.__('All Locations')}
                                            </Header.Subheader>
                                        </Link>
                                    </Header>
                                ) : (
                                    <Header as="h2" className="sectionHeader lightText" floated="left">
                                        <Link to="/locations">
                                            {header}
                                        </Link>
                                    </Header>
                                )
                            ) : null}
                            <Divider section hidden clearing />
                            <Grid>
                                <Grid.Column width={hasFilters ? isMobile() ? 16 : 12 : 16}>
                                    {createElement(component, {
                                        items,
                                        itemsPerRow: hasFilters ? 3 : 4,
                                        withBeer
                                    })}
                                </Grid.Column>
                                {hasFilters && !isMobile() ? (
                                    <Grid.Column width={4} className="listFiltersCol">
                                        <LocationsNavbar />
                                        <Filters group="locations" mode="light" textSearch />
                                    </Grid.Column>
                                ) : null}
                            </Grid>
                        </Container>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </Segment>
        ) : null
    }
}

const mapDispatchToProps = dispatch => ({
    handlePage(page) { dispatch(handlePage('locationsList', page)) },
})

const mapStateToProps = ({ app: { views, filters, pages } }, { constView }) => {
    const { component, pageSize = PAGE_SIZE } = listViews[constView || views.locationsList]
    return ({
        filters: filters.locationsList,
        view: constView || views.locationsList,
        component,
        page: pages.locationsList,
        pageSize,
    })
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withTracker((props) => {
        let {
            pageSize = 8,
            maxCount,
        } = props
        const {
            page,
            view,
            query = {},
            params = {},
            filters: { keyword, ...attributes },
            hasFilters = true,
        } = props
        let maxCountX = 1e5
        query.isActive = true

        if (hasFilters) {
            if (keyword) {
                query.keyword = keyword
            }
            if (attributes) {
                query.attributes = Object.assign({}, query.attributes, attributes)
            }
        }

        if (view === 'map') {
            pageSize = MAX_MARKERS
            maxCount = pageSize
        }

        if (maxCount) {
            params.limit = maxCount
            maxCountX = maxCount
        } else {
            params.limit = pageSize * page
        }
        const subscribers = [
            Meteor.subscribe('locations.list', query, params),
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const items = Locations.list(query, params).fetch()

        return {
            ...props,
            loading,
            hasMore: page * pageSize <= items.length && items.length < maxCountX,
            items,
        }
    }),
)(LocationsList)

