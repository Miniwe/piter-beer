import React, { Component, Fragment } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import ImageGallery from 'react-image-gallery'

import pick from 'lodash/pick'
import {
    Segment,
    Dimmer,
    Image,
    Container,
    Grid,
    Header,
    Loader,
    Icon,
    Divider,
    Feed,
    Table,
} from 'semantic-ui-react'
import Attributes from 'meteor/mcore/api/attributes'
import Locations from '/imports/api/locations'
import HelmetHead from '../../components/HelmetHead'
import YandexMap from '../../components/YandexMap'
import BeersList from '../../beers/components/BeersList'
import Corner from '/imports/ui/components/Corner'
import {
    mainImageLink as mainImageLinkField,
    logoLink as logoLinkField,
    imagesList as imagesListField,
} from '../../components/Fields'
import Comments from '../../components/Comments'
import { attributeRenderer } from '../../components/Fields'

class LocationSingle extends Component {
    filterAttributes = (_id, attrs = [], special = {}) => {
        const filteredAttrs = {
            filter: [],
            basic: [],
        }
        if (attrs.length > 0) {
            attrs.forEach(({ attributeId, attributeValue }, key) => {
                const attribute = Attributes.findOne(attributeId)
                if (attribute && attribute.isActive) {
                    const attrData = {
                        id: `${_id}_${attributeId}_${key}`,
                        title: attribute.title,
                        value: attributeRenderer(attribute)(attributeValue),
                        srcValue: attributeValue
                    }
                    if (attribute.alias in special) {
                        if (special[attribute.alias]) {
                            filteredAttrs[attribute.alias] = attrData
                        }
                    } else if (attribute.isFilter) {
                        filteredAttrs.filter.push(attrData)
                    } else {
                        filteredAttrs.basic.push(attrData)
                    }
                }
            })
        }
        return filteredAttrs
    }

    renderAttributes = (_id, attrs = []) => {
        return attrs.length > 0 ? (
            <Table basic="very" columns={2} className="compareTable">
                <Table.Body>
                    {attrs.map(({ id, title, value }) => (
                        <Table.Row key={`${id}`}>
                            <Table.HeaderCell width={6}>{title}</Table.HeaderCell>
                            <Table.Cell width={10}>
                                {value}{' '}
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        ) : null
    }

    render() {
        const { loading, item = { title: 'N/A'} } = this.props
        const {
            _id,
            title,
            geolocation = {},
            images = [],
            beers = [],
            logo,
            description,
            fullDescription,
            attributes = [],
            imageUrl = '',
            openingHours = [],
            address = {},
        } = item
        const addressShort = pick(address, [
            'streetAddress',
            'postalCode',
            'addressLocality',
            'metro',
        ])
        const mainImageLink = mainImageLinkField.renderer({ images }) || imageUrl
        const logoLink = logoLinkField.renderer({ logo })

        const imagesList = imagesListField.renderer({ images })
        const filteredAttrs = !loading ? this.filterAttributes(_id, attributes, {
            website: true,
            phone: true,
            priceRange: true,
        }) : {}

        const beersIds = beers.map(({ beerId }) => beerId)
        return !loading ? (
            <Fragment>
                <HelmetHead title={`${title} | ${i18n.__('Locations')}`} description={description} />
                <Segment className="skew-block skew-block--white next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container>
                        <Grid stackable>
                            <Grid.Row>
                                <Grid.Column width={6}>
                                    {geolocation.latitude && geolocation.longitude ? (
                                        <div className="ui large image map-container">
                                            <YandexMap
                                                height={250}
                                                zoom={geolocation.zoom}
                                                center={[
                                                    parseFloat(geolocation.latitude),
                                                    parseFloat(geolocation.longitude),
                                                ]}
                                                markers={[{
                                                    title, coordinates: [parseFloat(geolocation.latitude), parseFloat(geolocation.longitude),]
                                                }]}
                                            />
                                        </div>
                                    ) : null}

                                    <Divider section hidden />
                                    {images.length > 1 ? (
                                        <ImageGallery items={imagesList.map(image => image ? ({
                                            original: image.imageUrl.replace('localhost:3000', 'piterbeer.com'),
                                            thumbnail: image.imageUrl.replace('localhost:3000', 'piterbeer.com')
                                        }) : null).filter( i => i )} />
                                    ) : mainImageLink ? <Image centered size="medium" src={mainImageLink} /> : null}


                                </Grid.Column>
                                <Grid.Column width={10}>
                                    {logoLink ? <Image size="small" floated="right" src={logoLink} /> : null}
                                    <Header className="singleTitle" floated="left">
                                        {title}
                                        {filteredAttrs.website && filteredAttrs.website.value ? (
                                            <Header.Subheader as="a" href={filteredAttrs.website.value} target="_blank">
                                                {filteredAttrs.website.value}
                                            </Header.Subheader>
                                        ) : null}
                                    </Header>
                                    <Divider hidden clearing/>
                                    <div className="mainText" dangerouslySetInnerHTML={{ __html: fullDescription}} />
                                    <Divider hidden clearing/>
                                    <Feed>
                                    {filteredAttrs.priceRange && <Feed.Event>
                                        <Feed.Label>
                                            <Icon name='money' />
                                        </Feed.Label>
                                        <Feed.Content>
                                            <Feed.Date>{i18n.__('PriceRange')}</Feed.Date>
                                            <Feed.Summary>
                                                {filteredAttrs.priceRange.value}
                                            </Feed.Summary>
                                        </Feed.Content>
                                    </Feed.Event>}
                                        <Feed.Event>
                                            <Feed.Label>
                                                <Icon name='map marker alternate' />
                                            </Feed.Label>
                                            <Feed.Content>
                                                <Feed.Date>{i18n.__('Address')}</Feed.Date>
                                                <Feed.Summary>
                                                    {Object.values(addressShort).map((line, index) => (
                                                        <Fragment key={index}>
                                                            {line}
                                                            <br />
                                                        </Fragment>
                                                    ))}
                                                </Feed.Summary>
                                            </Feed.Content>
                                        </Feed.Event>
                                        {filteredAttrs.phone && <Feed.Event>
                                            <Feed.Label>
                                                <Icon name='phone' />
                                            </Feed.Label>
                                            <Feed.Content>
                                                <Feed.Date>{i18n.__('Phone')}</Feed.Date>
                                                <Feed.Summary>
                                                    <a href={`tel:${filteredAttrs.phone.value}`}>{filteredAttrs.phone.value}</a>
                                                </Feed.Summary>
                                            </Feed.Content>
                                        </Feed.Event>}
                                        {openingHours.length && <Feed.Event>
                                            <Feed.Label>
                                                <Icon name='time' />
                                            </Feed.Label>
                                            <Feed.Content>
                                                <Feed.Date>{i18n.__('Opening Hours')}</Feed.Date>
                                                <Feed.Summary>
                                                    {openingHours.map((oh, key) => (
                                                        <Fragment key={key}>
                                                            <a dangerouslySetInnerHTML={{ __html: `<span>${oh.dayOfWeek ? oh.dayOfWeek : ''}</span>` }} />
                                                            {oh.opens ? ' ' : '' }
                                                            {oh.opens ? oh.opens : '' }
                                                            {oh.closes ? <a> : </a> : ''}{' '}
                                                            {oh.closes ? oh.closes : ''}{' '}
                                                            <Feed.Date>{oh.more ? oh.more : ''}</Feed.Date>
                                                            <br/>
                                                        </Fragment>
                                                    ))}
                                                </Feed.Summary>
                                            </Feed.Content>
                                        </Feed.Event>}
                                    </Feed>
                                {this.renderAttributes(_id, filteredAttrs.filter)}
                                {this.renderAttributes(_id, filteredAttrs.basic)}
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Container>
                </Segment>

                <BeersList
                    hasFilters
                    withLocation={_id}
                    query={{ _id: { $in: beersIds } }}
                    header={i18n.__('BeerInLocation')}
                    withImg
                />

                <Segment className="skew-block skew-block--gray next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container>
                        <Comments />
                    </Container>
                </Segment>
            </Fragment>
        ) : (
            <Fragment>
                <Container>
                    <Dimmer inverted active>
                        <Loader />
                    </Dimmer>
                    <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                </Container>
            </Fragment>
        )
    }
}

export default withTracker((props) => {
    const { id } = props
    const subscribers = [
        Meteor.subscribe('locations.single', id)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Locations.findOne({ $or: [{ _id: id }, { alias: id }] })
    return {
        ...props,
        loading,
        item,
    }
})(LocationSingle)
