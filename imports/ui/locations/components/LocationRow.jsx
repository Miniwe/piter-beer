import React, { Component } from 'react'
import pick from 'lodash/pick'
import { Link } from 'react-router-dom'
import { Table } from 'semantic-ui-react'

class LocationRow extends Component {
    render() {
        const {
            _id,
            title,
            alias,
            address = {},
            withBeer,
            beers,
            locationType = ''
        } = this.props
        const addressShort = pick(address, [
            'streetAddress',
            'postalCode',
            'addressLocality',
        ])
        const beer = withBeer ? beers.find(({ beerId }) => beerId === withBeer) : { price: 0 }
        return (
            <Table.Row title={_id}>
            <Table.Cell><Link to={`/locations/${alias}`} className="animated fadeInLeft">{title}</Link></Table.Cell>
            <Table.Cell>{Object.values(addressShort).join(', ')}</Table.Cell>
            <Table.Cell>{address.metro}</Table.Cell>
            {withBeer ? <Table.Cell>{beer.price}</Table.Cell> : null}
            </Table.Row>
        )
    }
}

export default LocationRow
