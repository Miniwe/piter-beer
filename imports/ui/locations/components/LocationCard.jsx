import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    Card, Image, Statistic, Label,
} from 'semantic-ui-react'
import { stripTags } from 'meteor/mcore/utils'

class LocationCard extends Component {
    render() {
        const {
            _id,
            alias,
            title,
            description = '',
            priceRange='',
            locationType='',
            address={},
            beers = [],
            mode = 'light',
            withBeer
        } = this.props

        const addressStr = address.metro || address.addressLocality

        const beer = withBeer ? beers.find(({beerId}) => beerId === withBeer ) : false
        const units = i18n.__('price.units')
        return (
            <Card className="locationCard animated fadeInUp">
                {beer ? <Label className={`selfPrice${mode === 'light' ? '-light' : ''}`} attached='top right'>{beer.price}&nbsp;<span className="units">{units}</span></Label> : null}
                <Link to={`/locations/${alias}`} style={{ textAlign: 'center' }}>
                    <Image centered size="tiny" src={`http://img.piterbeer.com/img/no-location${mode === 'light' ? '-light' : ''}.svg`} />
                </Link>
                <Card.Content className={mode} textAlign="center">
                    <Card.Meta className="rating-container address">
                        {addressStr}
                    </Card.Meta>
                    <Card.Header as={Link} to={`/locations/${alias}`}>{title}</Card.Header>
                    <Card.Description className='no-description'>
                        {stripTags(description).substring(0, 255)}
                    </Card.Description>
                    <Statistic size='mini'>
                        <Statistic.Label className="brand">{locationType}</Statistic.Label>
                        <Statistic.Value className='price'>{priceRange}</Statistic.Value>
                    </Statistic>
                </Card.Content>
            </Card>
        )
    }
}

export default LocationCard
