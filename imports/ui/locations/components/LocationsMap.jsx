import React, { Component } from 'react'
import isEqual from 'lodash/isEqual'
import YandexMap from '../../components/YandexMap'
import NoItems from '../../components/NoItems'

const getMapData = (items) => {
    const markers = []
    let center = null
    items.forEach(({ _id, title, geolocation }) => {
        if (geolocation && geolocation.latitude) {
            if (center) {
                center = {
                    zoom: Math.round((parseInt(geolocation.zoom, 10) + center.zoom) / 2),
                    point: [
                        (geolocation.latitude + center.point[0]) / 2,
                        (geolocation.longitude + center.point[1]) / 2,
                    ]
                }
            } else {
                center = {
                    zoom: parseInt(geolocation.zoom, 10),
                    point: [
                        geolocation.latitude,
                        geolocation.longitude,
                    ]
                }
            }
            markers.push({
                id: _id,
                title,
                body: '',
                coordinates: [
                    geolocation.latitude,
                    geolocation.longitude
                ]
            })
        }
    })
    if (!center) {
        center = {
            zoom: 11,
            point: [59.9483015, 30.3629393]
        }
    }

    return {
        markers,
        center
    }
}

class LocationsMap extends Component {
    state = {
        markers: [],
        items: [],
        center: null
    }

    static getDerivedStateFromProps({ items }, prevState) {
        return !isEqual(items, prevState.items)
            ? { ...getMapData(items), items }
            : null
    }

    render() {
        const { markers, center } = this.state
        const { height } = this.props

        return center ? (
            <YandexMap
                height={height}
                markers={markers}
                center={center.point}
                zoom={center.zoom}
            />
        ) : <NoItems />
    }
}

export default LocationsMap

