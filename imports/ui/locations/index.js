import { renderRoutes } from 'react-router-config'
import LocationsPage from './pages/LocationsPage'
import LocationPage from './pages/LocationPage'

export {
    LocationsPage,
    LocationPage,
}

export default ({ route }) => renderRoutes(route.routes, route)
