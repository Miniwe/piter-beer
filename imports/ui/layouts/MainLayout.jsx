import React, { Fragment } from 'react'
import { renderRoutes } from 'react-router-config'
import { toast, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.min.css'
import 'react-id-swiper/src/styles/css/swiper.css'
import MainHeader from '../components/MainHeader'
import MainFooter from '../components/MainFooter'

const MainLayout = ({ route, location: { pathname } }) => (
    <Fragment>
        {pathname !== '/' && <MainHeader />}
        {renderRoutes(route.routes)}
        <MainFooter />
        <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} />
    </Fragment>
)

export default MainLayout
