import {
    ListServices as AttributesListServices
} from 'meteor/mcore/api/attributes/services'

export const getCountry = (attributes = []) => {
    if (attributes.length) {
        const cAttr = AttributesListServices.getAttr('country')
        if (cAttr) {
            const attr = attributes.find(({
                attributeId
            }) => attributeId === cAttr._id)
            return attr ? attr.attributeValue : 0
        }
    }
    return ''
}
