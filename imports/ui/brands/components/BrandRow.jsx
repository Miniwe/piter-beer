import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Table } from 'semantic-ui-react'
import { getCountry } from '../utils'

class BrandRow extends Component {
    render() {
        const { _id, title, alias, attributes } = this.props
        return (
            <Table.Row>
                {/* <Table.Cell>{_id}</Table.Cell> */}
                <Table.Cell><Link to={`/brands/${alias}`} className="animated fadeInLeft">{title}</Link></Table.Cell>
                <Table.Cell>{getCountry(attributes)}</Table.Cell>
            </Table.Row>
        )
    }
}

export default BrandRow
