import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Card, Image, } from 'semantic-ui-react'
import { mainImageLink as mainImageLinkField, } from '../../components/Fields'
import { getCountry } from '../utils'

class BrandCard extends Component {
    render() {
        const {
            _id,
            alias,
            title,
            imageUrl,
            isActive,
            attributes,
            images = [],
        } = this.props

        const mainImageLink = mainImageLinkField.renderer({ imageUrl, images })

        return (
            <Card
                as={Link}
                to={`/brands/${alias}`}
                className="brandCard animated fadeInUp"
                {...(!isActive && {color: 'red'})}
            >
                <Image size="small" centered src={mainImageLink || '/img/no-img.svg'} />
                <Card.Content>
                    <Card.Header className="smallHeader">
                        {title}
                    </Card.Header>
                    <Card.Meta textAlign="center">
                        {getCountry(attributes)}
                    </Card.Meta>
                </Card.Content>
            </Card>
        )
    }
}

export default BrandCard
