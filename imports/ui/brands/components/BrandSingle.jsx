import React, { Component, Fragment } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Segment,
    Dimmer,
    Image,
    Container,
    Grid,
    Header,
    Loader,
    Icon,
    Divider,
    Table,
} from 'semantic-ui-react'
import Brands from '/imports/api/brands'
import Attributes from 'meteor/mcore/api/attributes'
import HelmetHead from '../../components/HelmetHead'
import BeersList from '../../beers/components/BeersList'
import NewsList from '../../news/components/NewsList'
import Corner from '/imports/ui/components/Corner'
import { mainImageLink as mainImageLinkField, } from '../../components/Fields'
import { attributeRenderer } from '../../components/Fields'

class BrandSingle extends Component {
    filterAttributes = (_id, attrs = [], special = {}) => {
        const filteredAttrs = {
            filter: [],
            basic: [],
        }
        if (attrs.length > 0) {
            attrs.forEach(({ attributeId, attributeValue }, key) => {
                const attribute = Attributes.findOne(attributeId)
                if (attribute && attribute.isActive) {
                    const attrData = {
                        id: `${_id}_${attributeId}_${key}`,
                        title: attribute.title,
                        value: attributeRenderer(attribute)(attributeValue),
                        srcValue: attributeValue
                    }
                    if (attribute.alias in special) {
                        if (special[attribute.alias]) {
                            filteredAttrs[attribute.alias] = attrData
                        }
                    } else if (attribute.isFilter) {
                        filteredAttrs.filter.push(attrData)
                    } else {
                        filteredAttrs.basic.push(attrData)
                    }
                }
            })
        }
        return filteredAttrs
    }

    renderAttributes = (_id, attrs = []) => {
        return attrs.length > 0 ? (
            <Table basic="very" columns={2} className="compareTable">
                <Table.Body>
                    {attrs.map(({ id, title, value }) => (
                        <Table.Row key={`${id}`}>
                            <Table.HeaderCell width={6}>{title}</Table.HeaderCell>
                            <Table.Cell width={10}>
                                {value}{' '}
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        ) : null
    }

    render() {
        const { loading, item = { title: i18n.__('N/A') } } = this.props
        const {
            _id,
            title,
            description,
            imageUrl,
            images,
            website,
            news = [],
            attributes = [],
        } = item

        const mainImageLink = mainImageLinkField.renderer({ imageUrl, images }) || '/img/no-img.svg'
        const filteredAttrs = !loading ? this.filterAttributes(_id, attributes, {
        }) : {}

        const newsIds = news.map(({ item }) => item)

        return !loading ? (
            <Fragment>
                <HelmetHead title={`${title} | ${i18n.__('Brands')}`} description={description} />
                <Segment className="skew-block skew-block--white next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container>
                        <Grid stackable>
                            <Grid.Row>
                                <Grid.Column width={6}>
                                    {mainImageLink ? <Image centered size="medium" src={mainImageLink} /> : null}
                                </Grid.Column>
                                <Grid.Column width={10}>
                                    <Header className="singleTitle">
                                        {title}
                                        {website ? (
                                            <Header.Subheader as="a" href={website} target="_blank">
                                                {website}
                                            </Header.Subheader>
                                        ) : null}
                                    </Header>
                                    <Divider hidden clearing />
                                    <div className="mainText" dangerouslySetInnerHTML={{ __html: description }} />
                                    {this.renderAttributes(_id, filteredAttrs.filter)}
                                    {this.renderAttributes(_id, filteredAttrs.basic)}
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Container>
                </Segment>
                <NewsList query={{ _id: { $in: newsIds } }} showEmpty={false} />
                <BeersList query={{ brandId: _id }} hasFilters={false} withImg />
            </Fragment>
        ) : (
            <Segment className="skew-block skew-block--white next-skew mb-0">
                <Corner type="headerCorner" />
                <Container>
                    <Dimmer inverted active>
                        <Loader />
                    </Dimmer>
                    <Image src='https://react.semantic-ui.com/images/wireframe/short-paragraph.png' />
                </Container>
            </Segment>
        )
    }
}

export default withTracker((props) => {
    const { id } = props
    const subscribers = [
        Meteor.subscribe('brands.single', id)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Brands.findOne({ $or: [{ _id: id }, { alias: id }] })
    return {
        ...props,
        loading,
        item,
    }
})(BrandSingle)
