import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import findIndex from 'lodash/findIndex'
import pick from 'lodash/pick'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'
import isEqual from 'lodash/isEqual'
import find from 'lodash/find'
import { Header, Table, Button } from 'semantic-ui-react'
import Attributes from 'meteor/mcore/api/attributes'
import { compareBrand } from '/imports/redux/actions'
import * as Fields from '../../components/Fields'
import NoItems from '../../components/NoItems'

const brandFields = pick(Fields, [
    'mainImage',
    'description',
    'brandTitle',
    'locationsList',
    'price',
    'website',
])

class BrandsCompare extends Component {
    getAttrs = (items) => {
        const attrs = []
        items.forEach(({ attributes }) => {
            attributes.forEach(({ attributeId, attributeValue }) => {
                const attrIndex = findIndex(attrs, { attributeId })
                if (attrIndex > -1) {
                    attrs[attrIndex].count += isEqual(attrs[attrIndex].attributeValue, attributeValue) ? 1 : 0
                    //  && isEqual(attrs[attributeId].value, attributeValue)
                } else {
                    const attrModel = Attributes.findOne(attributeId)
                    attrs.push({
                        attributeId,
                        attributeValue,
                        count: 1,
                        name: (attrModel) ? attrModel.name : attributeId,
                    })
                }
            })
        })
        return sortBy(attrs, ['name'])
    }

    render() {
        const { items, compareBrand } = this.props
        const attrsFields = this.getAttrs(items)

        return items.length > 0 ? (
            <Table columns={items.length + 1}>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width="3">{' '}</Table.HeaderCell>
                        {items.map(({ _id, title }) => (
                            <Table.HeaderCell key={_id}>
                                <Header as="h3" floated="left">
                                    <Link to={`/brands/${alias}`}>{title}</Link>
                                </Header>
                                <Button floated="right" size="mini" icon="remove" title={i18n.__('Compare')} onClick={event => compareBrand(event, _id)} />
                            </Table.HeaderCell>))}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {map(brandFields, (field, key) => (
                        <Table.Row key={key}>
                            <Table.HeaderCell>{field.label}</Table.HeaderCell>
                            {items.map(item => <Table.Cell key={item._id}>{field.renderer(item)}</Table.Cell>)}
                        </Table.Row>
                    ))}
                    {map(attrsFields, ({ attributeId, name, count }) => (
                        <Table.Row key={attributeId} active={count !== items.length}>
                            <Table.HeaderCell>{name}</Table.HeaderCell>
                            {items.map(({ _id, attributes }, key) => {
                                const itemAttr = find(attributes, { attributeId })
                                return (
                                    <Table.Cell key={`${_id}_${attributeId}_${key}`}>
                                        {itemAttr ? itemAttr.attributeValue : null}
                                    </Table.Cell>
                                )
                            })}
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        ) : <NoItems message={i18n.__('Nothing to compare')} />
    }
}

const mapDispatchToProps = dispatch => ({
    compareBrand(event, id) {
        event.preventDefault()
        dispatch(compareBrand(id))
    }
})

export default connect(null, mapDispatchToProps)(BrandsCompare)
