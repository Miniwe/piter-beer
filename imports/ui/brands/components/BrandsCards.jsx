import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import NoItems from '../../components/NoItems'
import BrandCard from './BrandCard'

class BrandsCards extends Component {
    render() {
        const { items, itemsPerRow = 4, className } = this.props

        return items.length > 0 ? (
            <Card.Group itemsPerRow={itemsPerRow} doubling className={className}>
                {items.map(item => <BrandCard key={item._id} {...item} />)}
            </Card.Group>
        ) : <NoItems />
    }
}

export default BrandsCards
