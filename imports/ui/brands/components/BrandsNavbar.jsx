import React, { Component } from 'react'
import { Menu, Button } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { handleView } from '/imports/redux/actions'

class BrandsNavbar extends Component {
    render() {
        const { handleView } = this.props

        return (
            <Menu tabular secondary size="tiny">
                <Menu.Item onClick={() => handleView('grid')}>
                    {i18n.__('Grid')}
                </Menu.Item>
                <Menu.Item onClick={() => handleView('table')}>
                    {i18n.__('Table')}
                </Menu.Item>
            </Menu>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    handleView(mode) { dispatch(handleView('brandsList', mode)) },
})

export default connect(null, mapDispatchToProps)(BrandsNavbar)
