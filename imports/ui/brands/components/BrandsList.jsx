import React, { Component, createElement } from 'react'
import { Link } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Container, Segment, Header, Grid, Icon, Divider, Sidebar,
} from 'semantic-ui-react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import Brands from '/imports/api/brands'
import { handlePage } from '/imports/redux/actions'
import { TRESHOLD, PAGE_SIZE } from '/imports/configs/constants'
import { getBodyHeight } from 'meteor/mcore/utils'
import Corner from '/imports/ui/components/Corner'
import Filters from '../../components/Filters'
import BrandsNavbar from './BrandsNavbar'
import BrandsCards from './BrandsCards'
import BrandsTable from './BrandsTable'
import { isMobile } from 'meteor/mcore/utils'

const MAX_MARKERS = 64
const listViews = {
    grid: {
        component: BrandsCards,
        pageSize: 4,
    },
    table: {
        component: BrandsTable,
        pageSize: 8,
    },
}

class BrandsList extends Component {
    state = { visible: false }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll, false)
        this.onScroll()
    }

    componentWillUnmount() {
        const { handlePage } = this.props
        window.removeEventListener('scroll', this.onScroll, false)
        handlePage(0)
    }

    handleHideClick = () => this.setState({ visible: false })
    handleShowClick = () => this.setState({ visible: true })
    handleSidebarHide = () => this.setState({ visible: false })

    onScroll = () => {
        const { handlePage, page, hasMore } = this.props
        const { innerHeight, scrollY } = window
        const bodyHeight = getBodyHeight()
        if ((innerHeight + scrollY) >= (bodyHeight - TRESHOLD) && hasMore) {
            handlePage(page + 1)
        }
    }

    render() {
        const {
            items,
            component = BrandsCards,
            hasFilters = true,
            header = i18n.__('Our Brands'),
            showEmpty = true,
            withBeer
        } = this.props
        const { visible } = this.state

        return showEmpty || items.length ? (
            <Segment className="skew-block skew-block--lightYellow next-skew mb-0">
                <Corner type="headerCorner" />
                <Sidebar.Pushable as="div">
                    {isMobile() && hasFilters ? (
                        <Sidebar
                            as={Segment}
                            animation="overlay"
                            direction="right"
                            icon="labeled"
                            inverted
                            onHide={this.handleSidebarHide}
                            vertical
                            visible={visible}
                            className="skew-block skew-block--lightYellow"
                        >
                            <div style={{ textAlign: 'right' }}>
                                <Icon name="close" size="large" link onClick={this.handleHideClick} />
                            </div>
                            <BrandsNavbar />
                            <Filters group="brands" mode="light" textSearch />
                        </Sidebar>
                    ) : null}
                    <Sidebar.Pusher>
                        <Container>
                            {isMobile() && hasFilters ? (
                                <Header icon as="h6" floated="right">
                                    <Icon name="filter" link onClick={this.handleShowClick} />
                                </Header>
                            ) : null}
                            {header ? (
                                withBeer ? (
                                    <Header as="h2" className="sectionHeader" floated="left">
                                        {header}
                                        <Link to="/brands">
                                            <Header.Subheader style={{ color: 'inherit' }}>
                                                {i18n.__('All Brands')}
                                            </Header.Subheader>
                                        </Link>
                                    </Header>
                                ) : (
                                        <Header as="h2" className="sectionHeader" floated="left">
                                            <Link to="/brands">
                                                {header}
                                            </Link>
                                        </Header>
                                    )
                            ) : null}
                            <Divider hidden clearing />
                            <Grid>
                                <Grid.Column width={hasFilters ? isMobile() ? 16 : 12 : 16}>
                                    {createElement(component, {
                                        items,
                                        itemsPerRow: hasFilters ? 3 : 4,
                                        withBeer
                                    })}
                                </Grid.Column>
                                {hasFilters && !isMobile() ? (
                                    <Grid.Column width={4} className="listFiltersCol">
                                        <BrandsNavbar />
                                        <Filters group="brands" textSearch />
                                    </Grid.Column>
                                ) : null}
                            </Grid>
                        </Container>
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </Segment>
        ) : null
    }
}

const mapDispatchToProps = dispatch => ({
    handlePage(page) { dispatch(handlePage('brandsList', page)) },
})

const mapStateToProps = ({ app: { views, filters, pages } }, { constView }) => {
    const { component, pageSize = PAGE_SIZE } = listViews[constView || views.brandsList]
    return ({
        filters: filters.brandsList,
        view: constView || views.brandsList,
        component,
        page: pages.brandsList,
        pageSize,
    })
}


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withTracker((props) => {
        let { pageSize } = props
        const {
            maxCount,
            page,
            query = {},
            params = {},
            filters: { keyword, ...attributes },
            hasFilters = false,
        } = props
        let maxCountX = 1e5

        query.isActive = true
        if (hasFilters) {
            pageSize = 6
            Object.assign(query, {
                keyword,
                attributes,
            })
        }

        if (maxCount) {
            params.limit = maxCount
            maxCountX = maxCount
        } else {
            params.limit = pageSize * page
        }
        const subscribers = [
            Meteor.subscribe('brands.list', query, params),
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const items = Brands.list(query, params).fetch()

        return {
            ...props,
            loading,
            hasMore: page * pageSize <= items.length && items.length < maxCountX,
            items,
        }
    }),
)(BrandsList)
