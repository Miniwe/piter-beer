import { renderRoutes } from 'react-router-config'
import BrandsPage from './pages/BrandsPage'
import BrandPage from './pages/BrandPage'

export {
    BrandsPage,
    BrandPage,
}

export default ({ route }) => renderRoutes(route.routes, route)
