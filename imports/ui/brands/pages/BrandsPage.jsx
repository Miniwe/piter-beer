import React, { Component, Fragment } from 'react'
import { ListServices as PagesListServices } from 'meteor/mcore/api/pages/services'
import { withTracker } from 'meteor/react-meteor-data'
import Pages from 'meteor/mcore/api/pages'
import BrandsList from '../components/BrandsList'
import HelmetHead from '../../components/HelmetHead'

class BrandsPage extends Component {
    state = {}

    render() {
        const { item } = this.props
        return (
            <Fragment>
                {item ? <HelmetHead {...{ title: i18n.__('Our Brands'), ...PagesListServices.getHeadData('/brands', item) }} /> : null}
                <BrandsList view="table" hasFilters />
            </Fragment>
        )
    }
}

export default withTracker((props) => {
    const alias = '/brands'
    const subscribers = [
        Meteor.subscribe('pages.single', alias)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias })
    return {
        ...props,
        loading,
        item,
    }
})(BrandsPage)
