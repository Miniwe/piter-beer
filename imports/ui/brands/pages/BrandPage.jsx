import React, { Component, Fragment } from 'react'
import BrandSingle from '../components/BrandSingle'
import HelmetHead from '../../components/HelmetHead'

class BrandsPage extends Component {
    render() {
        const { match: { params }  = {}} = this.props

        return (
            <Fragment>
                <HelmetHead title={i18n.__('Brand Page')} />
                <BrandSingle {...params} />
            </Fragment>
        )
    }
}

export default BrandsPage
