import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Button, Container, Segment } from 'semantic-ui-react'
import { compareBeer, clearCompared } from '/imports/redux/actions'
import HelmetHead from '../components/HelmetHead'
import BeersList from '../beers/components/BeersList'

class ComparePage extends Component {
    render() {
        const { comparedBeers = [] } = this.props
        return (
            <Fragment>
                <HelmetHead title={i18n.__('Compare')} />
                <BeersList
                    constView="compare"
                    hasFilters={false}
                    header={i18n.__('Compare')}
                    query={{_id: {$in: comparedBeers}}}
                />
                {/* <Container>
                    <Segment basic>
                        <Button.Group>
                            <Button as={Link} to="/beers">{i18n.__('All Beers')}</Button>
                        </Button.Group>
                    </Segment>
                </Container> */}
                <BeersList
                    constView="grid"
                    hasFilters={false}
                    maxCount={8}
                    header={i18n.__('Similar')}
                    query={{ similar: comparedBeers }}
                />
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    const { app: { comparedBeers } } = state
    return ({ comparedBeers })
}

const mapDispatchToProps = (dispatch, { item: { _id } = {} }) => ({
    compareBeer(event) {
        event.preventDefault()
        dispatch(compareBeer(_id))
    },
    clearCompared() { dispatch(clearCompared()) },
})

export default connect(mapStateToProps, mapDispatchToProps)(ComparePage)
