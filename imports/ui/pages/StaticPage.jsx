import React, { Component, Fragment } from 'react'
import HelmetHead from '../components/HelmetHead'
import { withTracker } from 'meteor/react-meteor-data'
import {
    Segment, Header, Container,
} from 'semantic-ui-react'
import Pages from 'meteor/mcore/api/pages'
import Corner from '/imports/ui/components/Corner'

class StaticPage extends Component {
    render() {
        const { item = {} } = this.props
        const{ title = 'title', text = 'text' } = item
        return (
            <Fragment>
                <HelmetHead title={i18n.__(title)} />
                <Segment className="skew-block skew-block--white next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container text>
                        <Header as="h2" className="sectionHeader darkText">{i18n.__(title)}</Header>
                        <Container text className="staticText">
                            <div dangerouslySetInnerHTML={{ __html: text }} />
                        </Container>
                    </Container>
                </Segment>
            </Fragment>

        )
    }
}


export default withTracker((props) => {
    const { location: { pathname } } = props

    const subscribers = [
        Meteor.subscribe('pages.single', pathname)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias: pathname })

    return {
        ...props,
        loading,
        item,
    }
})(StaticPage)
