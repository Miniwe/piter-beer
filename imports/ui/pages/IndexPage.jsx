import React, { Fragment } from 'react'
import Cover from '../components/Cover'
import { ListServices as PagesListServices } from 'meteor/mcore/api/pages/services'
import { withTracker } from 'meteor/react-meteor-data'
import Pages from 'meteor/mcore/api/pages'
import HelmetHead from '../components/HelmetHead'
import BeersList from '../beers/components/BeersList'
// import BrandsList from '../brands/components/BrandsList'
import LocationsList from '../locations/components/LocationsList'

const IndexPage = ({ item }) => (
    <Fragment>
        {item ? <HelmetHead {...{ title: i18n.__('Landing'), ...PagesListServices.getHeadData('/', item) }} /> : null}
        <Cover />
        <BeersList
            constView="grid"
            maxCount={8}
            hasFilters={false}
            header={i18n.__('BeersInPiter')}
            query={{ random: 8 }}
        />
        {/* <BrandsList
            constView="grid"
            maxCount={4}
            hasFilters={false}
            header={i18n.__('Our Brands')}
            params={{ sort: { title: 1 } }}
        /> */}
        <LocationsList
            constView="grid"
            maxCount={8}
            hasFilters={false}
            header={i18n.__('PlacesList')}
            params={{ sort: { title: 1 } }}
            query={{ random: 8 }}
        />
    </Fragment>
)

export default withTracker((props) => {
    const alias = '/'
    const subscribers = [
        Meteor.subscribe('pages.single', alias)
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const item = Pages.findOne({ alias })
    return {
        ...props,
        loading,
        item,
    }
})(IndexPage)
