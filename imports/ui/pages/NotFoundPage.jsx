import React, { Component, Fragment } from 'react'
import HelmetHead from '../components/HelmetHead'
import {
    Segment, Header, Container,
} from 'semantic-ui-react'
import Corner from '/imports/ui/components/Corner'

class NotFoundPage extends Component {
    render() {
        return (
            <Fragment>
                <HelmetHead title={i18n.__('404: Page Not Found')} />
                <Segment className="skew-block skew-block--white next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container>
                        <Header as="h2" className="sectionHeader darkText">{'404'}</Header>
                        <Container text>
                            <p>{i18n.__('Ops.. Something wrong')}</p>
                            <p>{i18n.__('Page Not Found')}</p>
                        </Container>
                    </Container>
                </Segment>
            </Fragment>

        )
    }
}

export default NotFoundPage
