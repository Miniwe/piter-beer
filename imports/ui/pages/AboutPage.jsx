import React, { Component, Fragment } from 'react'
import HelmetHead from '../components/HelmetHead'
import {
    Segment, Header, Container,
} from 'semantic-ui-react'
import Corner from '/imports/ui/components/Corner'

class AboutPage extends Component {
    render() {
        return (
            <Fragment>
                <HelmetHead title={i18n.__('About')} />
                <Segment className="skew-block skew-block--white next-skew mb-0">
                    <Corner type="headerCorner" />
                    <Container>
                        <Header as="h2" className="sectionHeader darkText">{i18n.__('About')}</Header>
                        <Container text className="staticText">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere itaque similique sit placeat voluptate? Dolorum velit debitis corrupti enim, atque quibusdam, labore reprehenderit dicta recusandae praesentium doloribus eveniet autem inventore? Dignissimos fugit eaque impedit ipsa atque rerum molestias cumque sed odio tempore quasi libero excepturi nisi ullam adipisci nemo ex eius iure maxime nulla, dolores accusamus. Porro sint, molestias dignissimos reiciendis consectetur illum exercitationem laboriosam quaerat fugiat possimus, recusandae deserunt necessitatibus sed ullam nemo in aperiam similique, eius harum quas error iusto! Vero ad aliquid, facilis expedita explicabo, maiores unde possimus accusantium sunt officiis minima nesciunt consectetur adipisci magnam deserunt!</p>
                            <p>Aspernatur saepe eum quasi sequi incidunt. Quis magni vero natus voluptatum voluptas facere, commodi amet dolorum suscipit est deserunt. Quam a illo ea recusandae facilis sunt iure, alias id veritatis velit dolores autem dicta labore voluptatum provident eligendi nostrum veniam modi mollitia. Officia, quos sit qui error itaque aut nulla quas, doloremque maiores quasi quod excepturi sint rerum atque ex illum mollitia esse aliquid reprehenderit vel possimus sunt accusantium. Quaerat corporis fugiat eveniet repudiandae voluptatibus consequatur et vero aspernatur dolore sunt ipsum maiores fugit voluptas animi qui quasi, sint ab delectus blanditiis commodi itaque quidem adipisci. Sunt atque libero quis?</p>
                            <p>Consectetur sit totam, nisi esse soluta explicabo omnis beatae sint, quas error quia magni deserunt animi quis quasi in! Exercitationem unde dignissimos saepe, nesciunt omnis excepturi amet harum aut hic quam neque facilis id provident atque dicta? Totam numquam nobis voluptate. Veniam, esse! Necessitatibus saepe fugiat non perferendis quasi enim illo esse fugit. Facere, numquam nihil autem ipsum eligendi doloribus! Vero quo cumque repellat itaque molestiae molestias obcaecati assumenda omnis eum earum laudantium deserunt, minus aliquid quae quasi? Sequi hic aperiam error sed suscipit ad alias quod excepturi dolor veniam iure at pariatur libero, porro laudantium modi laboriosam debitis officia.</p>
                            <p>Optio esse molestiae ducimus reiciendis natus fugit, nihil nostrum, odio nemo hic minima corporis architecto magni consequuntur est deserunt consectetur voluptate pariatur delectus maxime, cum ratione eveniet harum! Rem officia perspiciatis cupiditate ea, iste quasi inventore aut id nihil, iure possimus alias tempora laborum nulla a fugit quo? Molestias vero, eos rem quidem ipsa deleniti necessitatibus facere sequi incidunt quos, laboriosam esse odit? Quos consequuntur aut aperiam id est ullam, repudiandae delectus ut quia a libero nesciunt, porro, laudantium recusandae reiciendis dolor quis repellendus illum ducimus? Ut aut nulla laborum, dolores molestias explicabo facilis inventore temporibus, commodi praesentium sint fuga.</p>
                        </Container>
                    </Container>
                </Segment>
            </Fragment>

        )
    }
}

export default AboutPage
