import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ConnectedRouter } from 'connected-react-router'
import { Switch, Route } from 'react-router-dom'
import { withTracker } from 'meteor/react-meteor-data'
import { renderRoutes } from 'react-router-config'
import { animateScroll as scroll } from 'react-scroll'
import { Provider } from 'react-redux'
import { AppLoader } from 'meteor/mcore/ui/components'
import './semantic-ui/index.less'
// import 'react-datepicker/dist/react-datepicker.css'
import 'animate.css'
// import adminRoutes from './admin/routes'
import store, { history } from '/imports/redux/store'
import { setCurrentUser } from '/imports/redux/actions'
import {
    UI, routes as adminRoutes
} from 'meteor/mcore'
import routes from './routes'
import * as collections from '../api/collections'


const { Auth } = UI
const collectionsList = Object.keys(collections).join('|')

class App extends Component {
    state = {
    }

    static propTypes = {
        currentUser: PropTypes.object,
        loggingIn: PropTypes.bool.isRequired,
    }

    static defaultProps = {
        currentUser: {},
    }

    componentDidMount() {
        scroll.scrollToTop()
    }

    shouldComponentUpdate(nextProps) {
        const { currentUser } = this.props
        return currentUser !== nextProps.currentUser
    }

    userHasAccess = () => {
        const { currentUser } = this.props
        return currentUser && '_id' in currentUser
    }

    handleLogin = () => {
        history.push('/')
    }

    render() {
        const { loggingIn, currentUser } = this.props
        const appLoading = loggingIn || (currentUser && !currentUser.roles)
        return appLoading ? <AppLoader /> : (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Switch>
                            <Route path="/admin">
                                {this.userHasAccess() ? renderRoutes(adminRoutes(collectionsList), { history }) : <Auth.Wrapper path="/login" component={Auth.Accounts.ui.LoginForm} />}
                            </Route>
                            <Route path="/">
                                {renderRoutes(routes, { history })}
                            </Route>
                        </Switch>
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default withTracker(() => {
    const subscribers = [
        { ready: () => !Meteor.loggingIn() },
    ]
    const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
    const currentUser = Meteor.user()
    if (currentUser) {
        delete currentUser.services
        store.dispatch(setCurrentUser(currentUser))
    }
    return {
        loggingIn: loading,
        currentUser,
    }
})(App)
