import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import {
    Icon,
    Dropdown,
    Menu,
} from 'semantic-ui-react'

class MainMenu extends Component {
    renderSubMenu = (title, to, items, key, icon = 'angle right') => (
        'flat' in this.props ? (
            <Menu.Item key={key}>
                <Menu.Header as={NavLink} to={to}>
                    <Icon name={icon} />
                    <span className="caption">{i18n.__(title)}</span>
                </Menu.Header>
                <Menu.Menu>
                    {this.renderMenu(items)}
                </Menu.Menu>
            </Menu.Item>
        ) : (
                <Dropdown item text={i18n.__(title)} key={key}>
                <Dropdown.Menu>
                    {this.renderMenu(items)}
                </Dropdown.Menu>
            </Dropdown>
        )
    )

    renderMenu = (items) => {
        return items.map(({to, title, items, icon = 'angle right', onClick}, index) => (items
            ? this.renderSubMenu(title, to, items, index)
            : (
                <Menu.Item key={index} as={NavLink} to={to} onClick={onClick}>
                    <Icon name={icon} />
                    <span className="caption">{i18n.__(title)}</span>
                </Menu.Item>
            )) )
    }

    render() {
        const { items } = this.props
        return this.renderMenu(items)
    }
}

export default MainMenu
