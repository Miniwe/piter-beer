import React, { Component } from 'react'
import { Segment } from 'semantic-ui-react'

/**
 * @todo Move here add widget script - an on load call Func init
 */
class Comments extends Component {
    wpac_ajax_init = () => {
        if (typeof WPac !== 'undefined') {
            WPac.init([
                {
                    widget: 'Comment',
                    id: Meteor.settings.public.widgets
                },
                // { widget: 'CommentCount', id: 14482 }
            ])
        }
    }

    componentDidMount() {
        this.wpac_ajax_init()
    }

    render() {
        return (
            <Segment basic padded>
                <div id="wpac-comment"></div>
            </Segment>
        )
    }
}

export default Comments
