import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const AppLoader = () => (
    <Segment className="app-loader">
        <Dimmer active inverted>
            <Loader inverted>
                {' '}
            </Loader>
        </Dimmer>
    </Segment>
)

export default AppLoader
