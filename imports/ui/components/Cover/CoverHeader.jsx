import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { isMobile } from 'meteor/mcore/utils'
import { setSearchMode } from '/imports/redux/actions'
import {
    Container,
    Menu,
    Icon,
    Header,
} from 'semantic-ui-react'
import { menuItems } from '../MainHeader'


const { public: { siteName } } = Meteor.settings

class CoverHeader extends Component {
    state = {}

    render() {
        // const { setSearchMode } = this.props
        // const items = Object.values(menuItems)

        const mainMenuOpts = {
            className: 'mainMenu',
            inverted: true,
            secondary: true
        }
        if (isMobile()) {
            mainMenuOpts.fixed = 'top'
        }
        return (
            <Header className="coverHeader">
                <Container>
                    <Menu inverted secondary className="headerMenu">
                        <Menu.Item header>
                            <div className="logo">
                                {siteName}
                            </div>
                            <div className="slogan">
                                {i18n.__('Your beer navigator in Saint-Petersburg')}
                            </div>
                        </Menu.Item>
                    </Menu>
                    <Menu {...mainMenuOpts}>
                        <Menu.Menu position="right" className="siteMenu">
                            {/* <Menu.Item name="search" as="a" onClick={setSearchMode}>
                                <Icon name="search" />
                                <span className="caption">{i18n.__('Search')}</span>
                            </Menu.Item> */}
                            <Menu.Item name="beers" as={Link} to="/beers">
                                <Icon name="bar" />
                                <span className="caption">{i18n.__('Beers')}</span>
                            </Menu.Item>
                            <Menu.Item name="brands" as={Link} to="/brands">
                                <Icon name="trademark" />
                                <span className="caption">{i18n.__('Brands')}</span>
                            </Menu.Item>
                            <Menu.Item name="locations" as={Link} to="/locations">
                                <Icon name="map marker alternate" />
                                <span className="caption">{i18n.__('Locations')}</span>
                            </Menu.Item>
                            {/* <Menu.Item name="about" as={Link} to='/about'>
                                <span className="caption">{i18n.__('About')}</span>
                            </Menu.Item> */}

                            {/* <Menu.Item name="compare" as={Link} to="/compare">
                                <Icon name="balance scale" />
                                Compare
                            </Menu.Item>
                            <Menu.Item name="admin" as={Link} to="/admin">
                                <Icon name={Meteor.userId() ? 'user' : 'sign in'} />
                                Administration
                            </Menu.Item> */}
                        </Menu.Menu>
                    </Menu>
                </Container>
            </Header>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    setSearchMode(event, { name }) {
        event.preventDefault()
        dispatch(setSearchMode(name))
    },
})

export default connect(null, mapDispatchToProps)(CoverHeader)
