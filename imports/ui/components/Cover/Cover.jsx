import React, { Component, createElement } from 'react'
import { connect } from 'react-redux'
import { Container, Segment } from 'semantic-ui-react'
import CoverHeader from './CoverHeader'
import * as Dialogs from '../Dialogs'

class Cover extends Component {
    render() {
        const { searchMode } = this.props
        return (
            <Segment padded inverted className="landingCover next-skew mb-0">
                <CoverHeader />
                <Container className="action-field-container">
                    <Container text className="staticText">
                        {createElement(Dialogs[searchMode])}
                    </Container>

                </Container>
            </Segment>
        )
    }
}

const mapStateToProps = ({ app: { searchMode } }) => ({ searchMode })

export default connect(mapStateToProps)(Cover)
