import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Menu, Icon, Header, Container,
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import MainMenu from './MainMenu'

const { public: { siteName } } = Meteor.settings
const menuItems = {
    beers: {
        to: '/beers',
        title: 'Beers',
        icon: 'bar'
    },
    brands: {
        to: '/brands',
        title: 'Brands',
        icon: 'trademark'
    },
    locations: {
        to: '/locations',
        title: 'Locations',
        icon: 'map marker alternate'
    },
    contacts: {
        to: '/contacts',
        title: 'Contacts',
        icon: 'angle right'
    },
    about: {
        to: '/about',
        title: 'About',
        icon: 'angle right'
    },
}

class MainHeader extends Component {
    state = {
        items: Object.values(menuItems)
    }

    render() {
        const { items } = this.state
        const { hasCompare } = this.props
        return (
            <Header className="mainHeader next-skew mb-0">
                <Container>
                    <Menu inverted secondary className="mainMenu innerMenu">
                        <Menu.Item header as={Link} to="/">
                            <div className="logo">
                                {siteName}
                            </div>
                        </Menu.Item>
                        <Menu.Menu className="siteMenu">
                            <MainMenu items={items} />
                            {hasCompare && <Menu.Item name="compare" as={Link} to="/compare">
                                <Icon name="balance scale" />
                                <span className="caption">{i18n.__('Compare')}</span>
                            </Menu.Item>}
                        </Menu.Menu>
                    </Menu>
                </Container>
            </Header>
        )
    }
}

export {
    menuItems
}

const mapStateToProps = ({ app: { comparedBeers = [] } }) => ({
    hasCompare: comparedBeers.length > 0,
})

export default connect(mapStateToProps)(MainHeader)
