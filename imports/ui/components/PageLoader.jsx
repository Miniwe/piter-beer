import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const PageLoader = () => (
    <Segment basic className="page-loader">
        <Dimmer active inverted>
            <Loader inverted>
                {i18n.__('Loading Page Data')}
            </Loader>
        </Dimmer>
    </Segment>
)

export default PageLoader
