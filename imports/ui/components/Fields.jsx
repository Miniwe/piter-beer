import React from 'react'
import { Link } from 'react-router-dom'
import { List, Image } from 'semantic-ui-react'
import Files from 'meteor/mcore/api/files'
import Locations from '/imports/api/locations'
import Brands from '/imports/api/brands'
import * as collections from '../../api/collections'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'
import InlineList from './InlineList'

export const mainImageLink = {
    label: 'Image',
    itemKey: 'images',
    renderer({ imageUrl = '', images = [] }) {
        if (imageUrl) {
            return imageUrl
        }
        const imgFile = images.length ? Files.findOne({ _id: { $in: images } }) : false
        return imgFile ? imgFile.link() : false
    }
}
export const logoLink = {
    label: 'Image',
    itemKey: 'logo',
    renderer({ logo = [] }) {
        const imgFile = logo.length ? Files.findOne({ _id: { $in: logo } }) : false
        return imgFile ? imgFile.link() : false
    }
}

export const mainImage = {
    label: 'Image',
    itemKey: 'images',
    renderer({ _id, title, images = [] }) {
        const mainImg = mainImageLink   .renderer({ images })
        return (
            <Image fluid src={mainImg ? mainImg : 'http://img.piterbeer.com/img/no-img.svg'} alt={title} />
        )
    }
}

export const mainBeerImage = {
    label: 'Image',
    itemKey: 'images',
    renderer({ _id, title, images = [] }) {
        const mainImg = mainImageLink   .renderer({ images })
        return (
            <Image fluid src={mainImg ? mainImg : 'http://img.piterbeer.com/img/no-beer.svg'} alt={title} className="compareImage" />
        )
    }
}

export const description = {
    label: 'Description',
    itemKey: 'description',
    renderer({ description }) {
        return description
    }
}

export const locations = {
    label: 'Locations',
    itemKey: 'locations',
    renderer({ locations = [] }) {
        return Locations.find({ _id: { $in: locations } }).fetch()
    }
}

export const locationsList = {
    label: 'Locations',
    itemKey: 'locations',
    renderer({ locations = [] }) {
        const locationsList = Locations.find({ _id: { $in: locations } }).fetch()
        return (
            <List celled horizontal>
                {locationsList.map(({ _id, title }) => (
                    <List.Item as={Link} to={`/locations/${alias}`} key={_id}>{title}</List.Item>
                ))}
            </List>
        )
    }
}

export const website = {
    label: 'Website',
    itemKey: 'website',
    renderer({ website }) {
        return website
    }
}

export const brand = {
    label: 'Brand',
    itemKey: 'brandId',
    renderer({ brandId }) {
        return Object.assign({}, Brands.findOne(brandId))
    }
}

export const imagesList = {
    label: 'Images',
    itemKey: 'images',
    renderer: ({ images = [] }) => images.map(_id => {
        const image = Files.findOne({ _id })
        return image ? {
            ...image,
            imageUrl: image.link()
        } : null
    }).filter(item => item).sort((a, b) => a.meta.order - b.meta.order)
}


export const price = {
    label: 'Price',
    itemKey: 'attributes',
    renderer({ attributes = [], order = {} }) {
        const priceAttr = AttributesListServices.getPriceAttr()
        const beerPrice = attributes.find(({ attributeId }) => attributeId === priceAttr._id )
        const units = priceAttr && priceAttr.params.paramsData ? priceAttr.params.paramsData.units : ''
        if (beerPrice && beerPrice > 0) {
            return {
                value: beerPrice.attributeValue,
                units
            }
        }
        return {
            value: order && order.price > 0 ? order.price : 0,
            units
        }
    }
}

export const color = {
    label: 'Color',
    itemKey: 'attributes',
    renderer({ attributes = [] }) {
        const colorAttr = AttributesListServices.getAttr('beer-color')
        if (colorAttr) {
            const { params: { paramsData: { items } } } = colorAttr
            const beerColor = attributes.find(({ attributeId }) => attributeId === colorAttr._id)
            return beerColor
                ? items.find(({ title }) => title === beerColor.attributeValue)
                : { title: '', color: '#000000' }
        } else {
            return { title: '', color: '#000000' }
        }
    }
}

export const brandTitle = {
    label: 'Brand',
    itemKey: 'brandId',
    renderer({ brandId }) {
        const brandModel = brand.renderer( {brandId } )
        return brandModel ? brandModel.title : brandId
    }
}

export const attributes = {
    label: 'Attributes',
    itemKey: 'attributes',
    renderer({ attributes }) {
        return JSON.stringify(attributes)
    }
}

export const attributeRenderer = (attribute) => {
    if (!attribute) {
        return value => {
            return JSON.stringify(value)
        }
    }
    const { params: { paramsType, paramsData } = {} } = attribute
    switch (paramsType) {
    case 'boolean': return (value) => {
        const { yes, no } = paramsData
        return value ? yes : no
    }
    case 'string': return (value) => {
        return value
    }
    case 'directory': return (value) => {
        return <InlineList directory={paramsData.directory} items={value ? value.split(',') : value} />
    }
    case 'number': return (value) => {
        const { units } = paramsData
        return `${value} ${units}`
    }
    case 'set': return (value) => {
        return value
    }
    case 'setAndColor': return (value) => {
        return value
    }
    case 'unknown': return () => {
        const { comment } = paramsData
        return comment
    }
    default: return (value) => {
        return JSON.stringify(value)
    }
    }
}

