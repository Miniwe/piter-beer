import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    Segment, Menu, Container, Icon,
} from 'semantic-ui-react'
import Corner from './Corner'
import MainMenu from './MainMenu'
import { menuItems } from './MainHeader'

const { public: { siteName } } = Meteor.settings

class MainFooter extends Component {
    state = {
        items: Object.values(menuItems)
    }

    componentDidMount() {
        const innerHTML = "<a href='//www.liveinternet.ru/click' " +
            "target=_blank><img src='//counter.yadro.ru/hit?t26.15;r" +
            escape(document.referrer) + ((typeof (screen) == "undefined") ? "" :
            ";s" + screen.width + "" + screen.height + "" + (screen.colorDepth ?
            screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
            ";h" + escape(document.title.substring(0, 150)) + ";" + Math.random() +
            "' alt='' title='LiveInternet: показано число посетителей за" +
            " сегодня' " +
            "border='0' width='88' height='15'><\/a>"
        this.liveinternet.innerHTML = innerHTML
    }

    render() {
        const { items } = this.state

        return (
            <Segment className="skew-block skew-block--gray mb-0">
                <Corner type="headerCorner" />
                <Container>
                    <Menu inverted secondary stackable className="mainMenu footerMenu">
                        <Menu.Item className="copyright">
                            <div ref={node => this.liveinternet = node} />
                            <small>© PiterBeer, 2019</small>
                        </Menu.Item>
                        <Menu.Menu position="right">
                            <Menu.Item header as={Link} to="/">
                                <div className="logo">
                                    {siteName}
                                </div>
                            </Menu.Item>
                            <MainMenu items={items} />
                            {Meteor.userId() ? <Menu.Item name="admin" as={Link} to="/admin">
                                <Icon name={Meteor.userId() ? 'user' : 'sign in'} />
                                {i18n.__('Administration')}
                            </Menu.Item> : null}
                        </Menu.Menu>
                    </Menu>
                </Container>
            </Segment>
        )
    }
}

export default MainFooter