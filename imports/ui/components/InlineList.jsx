import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { List } from 'semantic-ui-react'

class InlineList extends Component {
    state = {
        models: []
    }

    componentDidMount() {
        const { items, directory } = this.props
        const query = { _id: { $in: items } }
        const params = {}

        Meteor.call(`${directory}.staticList`, {query, params}, (err, res) => {
            if (err) {
                throw new Meteor.Error(err)
            }
            this.setState({ models: res })
        })
    }

    render() {
        const { directory } = this.props
        const { models } = this.state
        return models.length > 0 ? (
            <List horizontal divided>
                {models.map(({ _id, alias, title }, key) => (
                    <List.Item key={`${_id}_${key}`} as={Link} to={`/${directory}/${alias}`}>{title}</List.Item>
                ))}
            </List>
        ) : null
    }
}

export default InlineList
