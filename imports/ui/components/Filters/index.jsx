import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'
import React, { Fragment, Component, createElement } from 'react'
import { compose } from 'recompose'
import { withTracker } from 'meteor/react-meteor-data'
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import {
    Form, Accordion, Divider, Icon, Button,
} from 'semantic-ui-react'
import AutoForm from 'uniforms-semantic/AutoForm'
import AutoFields from 'uniforms-semantic/AutoFields'
import Attributes from 'meteor/mcore/api/attributes'
import * as filtersControls from './filtersControls'
import {
    setFilters, setFilter, unsetFilter, resetFilters, populateFilters
} from '/imports/redux/actions'

class Filters extends Component {
    onChangeModel = model => {
        const { setFilters } = this.props
        const filters = {}
        for (let key in model) {
            if (model[key] !== undefined) {
                filters[key] = model[key]
            }
        }
        setFilters(filters)
    }

    render() {
        const { resetFilters, schema, filters, mode = 'dark' } = this.props

        return (
            <AutoForm
                className="mini"
                schema={schema}
                onSubmit={() => false}
                onChangeModel={this.onChangeModel}
                model={filters}
            >
                <AutoFields className="accordion ui" />
                {!isEmpty(filters) ? (
                    <Fragment>
                        <Divider hidden />
                        <Button icon="stop" fluid size="mini" color="red" inverted={mode === 'light'} content={i18n.__('ResetFilters')} onClick={resetFilters} />
                    </Fragment>
                ) : null}
            </AutoForm>
        )
    }
}

const mapDispatchToProps = (dispatch, { group }) => ({
    setFilters(filters) { dispatch(setFilters(`${group}List`, filters)) },
    setFilter(filter) { dispatch(setFilter(`${group}List`, filter)) },
    unsetFilter(filter) { dispatch(unsetFilter(`${group}List`, filter)) },
    resetFilters() { dispatch(resetFilters(`${group}List`)) },
})

const mapStateToProps = ({ app: { filters } }, { group = 'beers' }) => {
    return {
        filters: filters[`${group}List`]
    }
}

const makeSchema = (mode, attributes, group, textSearch) => {
    const filtersSchema = {}

    if (textSearch) {
        filtersSchema.keyword = filtersControls.string(i18n.__('Search by name'), {
            mode,
            group
        })
    }

    attributes.forEach(({
        _id,
        title,
        params: {
            paramsData,
            paramsType
        }
    }) => {
        filtersSchema[_id] = filtersControls[paramsType](`${title}`, {
            ...paramsData,
            mode,
            group
        })
    })
    return new SimpleSchema(filtersSchema)
}
export default compose(
    withTracker((props) => {
        const { textSearch, group, mode = 'dark' } = props
        const query = { isFilter: true, isActive: true }
        const params = { sort: { order: 1 } }
        const subscribers = [
            Meteor.subscribe('attributes.list', query)
        ]
        const loading = !subscribers.reduce((prev, subscriber) => prev && subscriber.ready(), true)
        const attributes = Attributes.find({ ...query, group }, params).fetch()
        return {
            ...props,
            loading,
            attributes,
            schema: makeSchema(mode, attributes, group, textSearch)
        }
    }),
    connect(mapStateToProps, mapDispatchToProps),
)(Filters)
