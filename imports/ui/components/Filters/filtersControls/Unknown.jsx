import React, { Component } from 'react'
import connectField from 'uniforms/connectField'
import ControlWrapper from './ControlWrapper'

class Control extends Component {
    render() {
        return (
            <div>
                Unknown Filter Control
            </div>
        )
    }
}

export default (title, params) => ({
    type: String,
    label: title,
    uniforms: {
        component: connectField(props => <ControlWrapper control={Control} {...props} />, { includeInChain: false }),
        ...params
    }
})
