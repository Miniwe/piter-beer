import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import connectField from 'uniforms/connectField'
import ControlWrapper from './ControlWrapper'

class Control extends Component {
    state = {
        items: [],
        keyword: ''
    }

    componentDidMount() {
        this.getOptions()
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.keyword !== this.state.keyword) {
            this.getOptions()
        }
    }

    getOptions = () => {
        const { directory } = this.props
        const { keyword } = this.state
        const query = {}
        const params = {
            limit: 20
        }
        if (keyword) {
            query.keyword = keyword
        }
        Meteor.call(`${directory}.staticList`, {query, params}, (err, res) => {
            if (err) {
                throw new Meteor.Error(err)
            }
            this.setState({
                items: res.map(({ title, _id }) => ({ text: title, value: _id }))
            })
        })
    }

    setFilter = (event, { value }) => {
        const { onChange } = this.props
        onChange(value)
    }

    onSearchChange = (event, data) => {
        const { searchQuery } = data
        this.setState({ keyword: searchQuery })
    }

    render() {
        const { title, value = [] } = this.props
        const { id, items } = this.state
        return (
            <Dropdown
                id={id}
                options={items}
                multiple
                selection
                fluid
                onSearchChange={this.onSearchChange}
                onChange={this.setFilter}
                value={value.length ? value : []}
            />
        )
    }
}

export default (title, params) => ({
    type: String,
    label: title,
    uniforms: {
        component: connectField(props => <ControlWrapper control={Control} {...props} />, { includeInChain: false }),
        ...params
    }
})
