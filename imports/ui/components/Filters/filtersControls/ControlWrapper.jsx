import React, { createElement, Component, Fragment } from 'react'
import classnames from 'classnames'
import filterDOMProps from 'uniforms/filterDOMProps'
import { connect } from 'react-redux'
import {
    Accordion, Label, Button, Icon
} from 'semantic-ui-react'
import { setFilter } from '/imports/redux/actions'

class ControlWrapper extends Component {
    state = {
        isOpen: this.props.value !== undefined && this.props.value !== '' && this.props.value !== []
    }

    toggleOpen = (event) => {
        event.preventDefault()
        const { name, setFilter } = this.props
        const newState = !this.state.isOpen
        this.setState({
            isOpen: newState
        })
        if (!newState) {
            setFilter({ [name]: undefined })
        }
    }

    render() {
        const {
            id,
            className,
            error,
            required,
            errorMessage,
            label,
            disabled,
            showInlineError,
            isActive,
            control,
            mode,
            setFilter,
            ...props
        } = this.props

        const {
            isOpen
        } = this.state

        return (
            <Fragment>
                <Accordion.Title
                    active={isOpen}
                    as={Button}
                    basic
                    fluid
                    index={id}
                    inverted={mode === 'light'}
                    onClick={this.toggleOpen}
                    primary={isActive}
                    size="mini"
                    style={{ textAlign: 'left' }}
                >
                    <Icon name="dropdown" />
                    {label}
                </Accordion.Title>
                <Accordion.Content className={classnames(className, { disabled, error, required }, 'field', isOpen ? 'active' : '')} {...filterDOMProps(props)}>
                    {createElement(control, props)}
                    {!!(error && showInlineError) && <Label color="red" basic pointing>{errorMessage}</Label>}
                </Accordion.Content>
            </Fragment>
        )
    }
}

const mapDispatchToProps = (dispatch, { group }) => ({
    setFilter(filter) { dispatch(setFilter(`${group}List`, filter)) },
})

export default connect(null, mapDispatchToProps)(ControlWrapper)
