
import React, { Component } from 'react'
import connectField from 'uniforms/connectField'
import {
    Input
} from 'semantic-ui-react'
import ControlWrapper from './ControlWrapper'

class Control extends Component {
    render() {
        const {
            id,
            name,
            disabled,
            placeholder,
            onChange,
            inputRef,
            value
        } = this.props
        return (
            <Input
                id={id}
                name={name}
                disabled={disabled}
                placeholder={placeholder}
                onChange={(e, { value }) => onChange(value)}
                ref={inputRef}
                value={value}
            />
        )
    }
}

export default (title, params) => ({
    type: String,
    label: title,
    uniforms: {
        component: connectField(props => <ControlWrapper control={Control} {...props} />, { includeInChain: false }),
        ...params
    }
})
