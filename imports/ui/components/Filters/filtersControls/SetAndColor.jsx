import React, { Component } from 'react'
import connectField from 'uniforms/connectField'
import { Dropdown } from 'semantic-ui-react'
import { Random } from 'meteor/random'
import ControlWrapper from './ControlWrapper'

class Control extends Component {
    state = {
        items: []
    }

    componentDidMount() {
        const { items } = this.props
        this.setState({
            id: Random.id(),
            items: items.map((item, index) => ({
                text: item.title,
                value: item.title,
                key: index,
            })),
        })
    }

    setFilter = (event, { value }) => {
        const { onChange } = this.props
        onChange(value)
    }

    render() {
        const { title, value = [] } = this.props
        const { id, items } = this.state
        return (
            <Dropdown
                id={id}
                multiple
                selection
                fluid
                options={items}
                placeholder={title}
                onChange={this.setFilter}
                value={value.length ? value : []}
            />
        )
    }
}

export default (title, params) => ({
    type: String,
    label: title,
    uniforms: {
        component: connectField(props => <ControlWrapper control={Control} {...props} />, { includeInChain: false }),
        ...params
    }
})
