import React, { Component } from 'react'
import connectField from 'uniforms/connectField'
import { Button } from 'semantic-ui-react'
import ControlWrapper from './ControlWrapper'

class Control extends Component {
    setFilter = (event, data) => {
        event.preventDefault()
        const { onChange, value } = this.props
        onChange((data.value !== value) ? data.value : undefined)
    }

    render() {
        const { title, value, yes, no } = this.props
        return (
            <Button.Group fluid size="mini">
                <Button value onClick={this.setFilter} primary={ value === true }>{yes}</Button>
                <Button value={false} onClick={this.setFilter} primary={ value === false }>{no}</Button>
            </Button.Group>
        )
    }
}

export default (title, params) => ({
    type: String,
    label: title,
    uniforms: {
        component: connectField(props => <ControlWrapper control={Control} {...props} />, { includeInChain: false }),
        ...params
    }
})
