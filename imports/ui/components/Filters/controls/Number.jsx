import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react'
import times from 'lodash/times'
import { Random } from 'meteor/random'
import { Form, Dropdown } from 'semantic-ui-react'

const DEFAULT_MIN = 0
const DEFAULT_MAX = 1e3
const DEFAULT_STEP = 1
class Number extends Component {
    state = {
        id: null,
        result: { from: this.props.min || DEFAULT_MIN, to: this.props.max || DEFAULT_MAX },
        gradations: [],
    }

    static defaultProps = {
        value: {
            from: DEFAULT_MIN,
            to: DEFAULT_MAX,
        }
    }

    componentDidMount() {
        const {
            min,
            max,
            step = 1,
            gradation,
            value
        } = this.props

        if (gradation > 1) {
            const gradations = []
            const presision = step > 0 && step < 1 ? step.toString().split('\.').pop().length : 0
            const grStep = (max - min) / gradation
            times(gradation + 1, (num) => {
                const gMin = Math.max(min, (num * grStep).toFixed(presision) )
                const gMax = Math.min(max, ((num + 1) * grStep).toFixed(presision) )
                if (gMin != gMax) {
                    gradations.push({
                        text: `${gMin}-${gMax}`,
                        from: gMin,
                        to: gMax,
                        value: num,
                    })
                }
            })
            this.setState({ gradations })
        }
        this.setState({ id: `num_${Random.id()}` })
    }

    setBound = (event, { name, value }) => {
        const { setFilter } = this.props
        const { result } = this.state
        result[name] = parseFloat(value)
        this.setState({ result })
        setFilter(result)
    }

    setBounds = (event, { value }) => {
        const { setFilter } = this.props
        const { gradations } = this.state
        const gradation = gradations[value]
        const result = {
            from: parseFloat(gradation.from),
            to: parseFloat(gradation.to),
        }
        this.setState({ result })
        setFilter(result)
    }

    renderGradations = () => {
        const { id, gradations } = this.state
        const { units = '', value } = this.props
        const currentIndex = gradations.findIndex(gradation => (
            value.from === gradation.from && value.min === gradation.min
        ))
        return (
            <Dropdown
                id={id}
                fluid
                selection
                options={gradations}
                placeholder={units}
                onChange={this.setBounds}
                value={currentIndex}
            />
        )
    }

    renderFromTo = () => {
        const { id } = this.state
        const { min = DEFAULT_MIN, max = DEFAULT_MAX, step = DEFAULT_STEP, units = '', value } = this.props
        return (
            <Form.Group widths="equal">
                <Form.Input
                    id={id}
                    type="number"
                    fluid
                    label={i18n.__('From')}
                    name="from"
                    min={min}
                    max={max}
                    step={step}
                    placeholder={`min: ${min}`}
                    defaultValue={value.from ? value.from : min}
                    onChange={this.setBound}
                />
                <Form.Input
                    type="number"
                    fluid
                    label={i18n.__('To')}
                    name="to"
                    min={min}
                    max={max}
                    step={step}
                    placeholder={`max: ${max}`}
                    defaultValue={value.to ? value.to : max}
                    onChange={this.setBound}
                />
                {units ? (
                    <Form.Field>
                        <br />
                        {units}
                    </Form.Field>
                ) : null}
            </Form.Group>
        )
    }

    render() {
        const { id } = this.state
        const {
            title,
            gradation
        } = this.props
        return gradation > 1 ? this.renderGradations() : this.renderFromTo()
    }
}

export default Number
