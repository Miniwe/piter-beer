import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import { Random } from 'meteor/random'

class SetAndColor extends Component {
    state = {
        items: []
    }

    componentDidMount() {
        const { items } = this.props
        this.setState({
            id: Random.id(),
            items: items.map((item, index) => ({
                text: item.title,
                value: item.title,
                key: index,
            })),
        })
    }

    setFilter = (event, data) => {
        const { setFilter } = this.props
        const { value } = data
        setFilter(value)
    }

    render() {
        const { title, value } = this.props
        const { id, items } = this.state
        return (
            <Dropdown
                id={id}
                multiple
                selection
                fluid
                options={items}
                placeholder={title}
                onChange={this.setFilter}
                value={value}
            />
        )
    }
}

export default SetAndColor
