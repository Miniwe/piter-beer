import React, { Component } from 'react'
import { Button } from 'semantic-ui-react'

class Boolean extends Component {
    setFilter = (event, data) => {
        event.preventDefault()
        const { setFilter } = this.props
        const { value } = data
        setFilter(value)
    }
    render() {
        const { title, value = '', yes, no } = this.props
        return (
            <Button.Group fluid size="mini">
                <Button value onClick={this.setFilter} primary={ value === true }>{yes}</Button>
                <Button value={false} onClick={this.setFilter} primary={ value === false }>{no}</Button>
            </Button.Group>
        )
    }
}

export default Boolean
