import React, { Component } from 'react'
import { Dropdown } from 'semantic-ui-react'
import { Random } from 'meteor/random'

class Directory extends Component {
    state = {
        items: [],
        keyword: ''
    }

    componentDidMount() {
        this.getOptions()
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.keyword !== this.state.keyword) {
            this.getOptions()
        }
    }

    getOptions = () => {
        const { directory } = this.props
        const { keyword } = this.state
        const query = {}
        const params = {
            limit: 20
        }
        if (keyword) {
            query.keyword = keyword
        }
        Meteor.call(`${directory}.staticList`, {query, params}, (err, res) => {
            if (err) {
                throw new Meteor.Error(err)
            }
            this.setState({
                items: res.map(({ title, _id }) => ({ text: title, value: _id }))
            })
        })
    }

    setFilter = (event, data) => {
        const { setFilter } = this.props
        const { value } = data

        setFilter(value)
    }

    onSearchChange = (event, data) => {
        const { searchQuery } = data
        this.setState({ keyword: searchQuery })
    }

    render() {
        const { title, value } = this.props
        const { id, items } = this.state
        return (
            <Dropdown
                id={id}
                options={items}
                multiple
                selection
                fluid
                onSearchChange={this.onSearchChange}
                onChange={this.setFilter}
                value={value}
            />
        )
    }
}

export default Directory
