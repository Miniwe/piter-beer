import React, { Component } from 'react'
import { Input } from 'semantic-ui-react'

class String extends Component {
    setFilter = (event, data) => {
        const { setFilter } = this.props
        const { value } = data
        setFilter(value)
    }
    render() {
        const { title, value = '' } = this.props
        return (
            <Input fluid placeholder={title} onChange={this.setFilter} defaultValue={value} />
        )
    }
}

export default String
