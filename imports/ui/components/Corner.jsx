import React, { Component } from 'react'

class Corner extends Component {
    state = {
        w: 400,
        h: 100,
    }

    componentDidMount() {
        this.setWidth()
        window.addEventListener('resize', this.setWidth)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.setWidth)
    }

    setWidth = () => {
        this.setState({ w: window.screen.width })
    }

    render() {
        const { type, fill = '#fadc44' } = this.props
        const { w, h } = this.state
        const style = {
            stroke: 'none',
            fillRule: 'evenodd',
            fill,
            fillOpacity: 1
        }
        return (
            <svg version="1.1" width={w} height={h} className={type}>
                <path id="shapePath1" d={`M${w},0 C${w},0 0,${h} 0,${h} C0,${h} ${w},${h} ${w},${h} C${w},${h} ${w},0 ${w},0 Z`} style={style} />
            </svg>
        )
    }
}

export default Corner

