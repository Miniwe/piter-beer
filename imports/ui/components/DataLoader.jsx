import React from 'react'
import { Dimmer, Loader, Segment } from 'semantic-ui-react'

const DataLoader = () => (
    <Segment basic className="data-loader">
        <Dimmer active inverted>
            <Loader inverted>
                {i18n.__('Loading Data')}
            </Loader>
        </Dimmer>
    </Segment>
)

export default DataLoader
