import React, { Component } from 'react'
import { history } from '/imports/redux/store'
import {
    YMaps, Map, Clusterer, ZoomControl, Placemark,
} from 'react-yandex-maps'

class YandexMap extends Component {
    clickMaker = (id) => {
        history.push(`/locations/${id}`)
    }

    render() {
        const {
            center,
            zoom,
            markers = [],
            width = '100%',
            height = 500
        } = this.props
        return (
            <YMaps>
                <Map
                    width={width}
                    height={`${Math.min(window.screen.availHeight, height)}px`}
                    defaultState={{ center, zoom }}
                    controls
                >
                    <ZoomControl
                        options={{
                            size: 'small',
                            zoomDuration: 400,
                        }}
                    />
                    <Clusterer
                        options={{
                            // preset: 'islands#invertedVioletClusterIcons',
                            groupByCoordinates: false,
                        }}
                    >
                        {markers.map(({ id, title, body = '', coordinates }, index) => (
                            <Placemark
                                key={index}
                                geometry={coordinates}
                                properties={{
                                    balloonContentHeader: title,
                                    hintContent: title,
                                    iconCaption: title
                                }}
                                options={{
                                    preset: 'islands#greenDotIconWithCaption',
                                    iconColor: '#225501',
                                    clustersIconColor: '#4d7830',
                                    controls: []
                                }}
                                onClick={() => this.clickMaker(id)}
                            />
                        ))}
                    </Clusterer>
                </Map>
            </YMaps>
        )
    }
}

export default YandexMap
