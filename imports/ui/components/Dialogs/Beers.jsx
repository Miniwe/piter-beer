import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import isEqual from 'lodash/isEqual'
import { connect } from 'react-redux'
import {
    Grid, Card, Input, Icon, Button, Divider, Segment,
} from 'semantic-ui-react'
import { isMobile } from 'meteor/mcore/utils'
import { ListServices as AttributesListServices } from 'meteor/mcore/api/attributes/services'
import Locations from '/imports/api/locations'
import BeersCards from '/imports/ui/beers/components/BeersCards'
import LocationsMap from '/imports/ui/locations/components/LocationsMap'
import Filters from '../Filters'
import { setFilter, setSearchMode } from '/imports/redux/actions'
import DataLoader from '../DataLoader'

class BeersDialog extends Component {
    state = {
        loading: false,
        items: [],
        mode: 'cards',
    }

    componentDidMount() {
        this.startSearch()
        this.rowCount = isMobile() ? 8 : 12
    }

    toggleMode = () => {
        const { mode } = this.state
        this.setState({
            mode: mode === 'map' ? 'cards' : 'map'
        })
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(prevProps.filters, this.props.filters)) {
            this.startSearch()
        }
    }

    getData = () => {
        const { filters: { keyword, ...attributes } } = this.props
        const query = {
            keyword,
            attributes,
            isActive: true
        }

        const params = {
            limit: this.rowCount,
            sort: 'order.price',
            order: 1
        }
        Meteor.call('search.beers', {query, params}, (err, res) => {
            if (err) {
                throw new Meteor.Error(err)
            }
            const { beers } = res
            this.setState({
                items: beers,
                loading: false
            })

        })
    }

    onChange = (e, { value }) => {
        const { setFilter } = this.props
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }

        this.searchTimeout = setTimeout(() => {
            setFilter({ keyword: value })
        }, 400)
    }

    onChangePrice = (priceId, value) => {
        const { setFilter } = this.props

        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }

        this.searchTimeout = setTimeout(() => {
            setFilter({ [priceId]: value })
        }, 400)
    }

    startSearch = () => {
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }
        this.setState({ loading: true })

        this.searchTimeout = setTimeout(() => {
            this.getData()
        }, 400)
    }

    getLocationsItems = (items) => {
        const locationsIds = items.reduce((list, { locations = [] }) => [...list, ...locations], [])
        return Locations.find({ _id: { $in: locationsIds } }).fetch()
    }

    handleValueChange(e, { value }) {
        this.setState({ value })
    }

    renderSelectRange = () => {
        // get price value
        // set middle value on Slider
        // const priceAttr = AttributesListServices.getPriceAttr()
        const priceAttr = {
            _id: 'XPgTC3DJ7FsAt479q',
            params: {
                paramsType: 'number',
                paramsData: {
                    min: 0,
                    max: 600,
                    step: 50,
                    units: '₽',
                    gradation: 3
                }
            }
        }
        if (priceAttr) {
            const { filters: { ...attributes } } = this.props
            const { _id, params: { paramsData: { min, max, step, gradation = 1, units } } } = priceAttr
            const gradations = []
            const delta = (max - min) / gradation
            for (let i = 0; i < gradation; i++ ) {
                const _from = Math.round(min + i * delta, 10)
                gradations.push({
                    from: _from === 0 ? 0 : _from + 1,
                    to: Math.min(Math.round(parseInt(min + (i + 1) * delta, 10)), max),
                })
            }
            return (
                <Segment basic>
                    <Grid stackable>
                        <Grid.Column width={4}>
                            <strong>
                                {i18n.__('SelectPrice')}
                            </strong>
                        </Grid.Column>
                        <Grid.Column width={12}>
                            <Button.Group fluid size="small" color="green" vertical={isMobile()} className="priceSelect">
                                {gradations.map((gr, index) => (
                                    <Button
                                        key={index}
                                        onClick={() => this.onChangePrice(_id, gr)}
                                    >
                                        {gr.from}-{gr.to}
                                        {' '}
                                        {units}
                                    </Button>
                                ))}
                            </Button.Group>
                        </Grid.Column>
                    </Grid>
                </Segment>
            )
        } return null
    }
    render() {
        const { mode, loading, items = [] } = this.state
        const { setSearchMode, filters: { keyword } = {} } = this.props

        return (
            <Card fluid className="action-field">
                <Card.Header>
                    <Input fluid size="large" icon={(
                        <div style={{ position: 'absolute', right: 0, top: '0.4rem' }}>
                            <Icon name={loading ? 'spinner' : 'bar'} loading={loading} link circular onClick={this.startSearch} />
                            <Icon name='chevron right' color="green" inverted link circular onClick={event => setSearchMode(event, {name: 'locations'} )} />
                        </div>
                    )} placeholder={i18n.__("Beers search")} onChange={this.onChange} onKeyPress={this.onKeyPress} defaultValue={keyword} />
                </Card.Header>
                <Card.Header>
                    {this.renderSelectRange()}
                </Card.Header>

                <Card.Content>
                    <Grid stackable>
                        {!isMobile() ? (
                            <Grid.Column width={4} className="filtersCol">
                                <Filters group="beers" />
                                <Divider hidden />
                                {/* <Button fluid color="yellow" active={mode === 'map'} icon="map marker alternate" size="small" content={i18n.__(mode === 'map' ? 'Hide Map' : 'Show Map')} onClick={this.toggleMode} /> */}
                                <Button fluid primary size="small" as={Link} to="/beers" content={i18n.__('All Beers')} />
                            </Grid.Column>
                        ) : null}
                        <Grid.Column width={12}>
                            {loading
                                ? <DataLoader />
                                : mode === 'map'
                                    ? <LocationsMap height={300} items={this.getLocationsItems(items)} />
                                    : <BeersCards items={items} itemsPerRow={this.rowCount / 4} />
                            }
                            { isMobile() ? (
                                <Fragment>
                                    <Divider hidden />
                                    <Button fluid primary size="small" as={Link} to="/beers" content={i18n.__("All Beers")} />
                                </Fragment>
                            ) : null}
                        </Grid.Column>
                    </Grid>
                </Card.Content>
            </Card>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    setFilter(filter) { dispatch(setFilter('beersList', filter)) },
    setSearchMode(event, { name }) {
        event.preventDefault()
        dispatch(setSearchMode(name))
    },

})

const mapStateToProps = ({ app: { filters: { beersList } } }) => ({
    filters: beersList,
})

export default connect(mapStateToProps, mapDispatchToProps)(BeersDialog)
