import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import {
    Card, Input, Icon, Button, Divider, Segment
} from 'semantic-ui-react'
import isEmpty from 'lodash/isEmpty'
import BrandsCards from '/imports/ui/brands/components/BrandsCards'
import { isMobile } from 'meteor/mcore/utils'
import DataLoader from '../DataLoader'

class BrandsDialog extends Component {
    state = {
        loading: false,
        keyword: '',
        results: null,
    }

    componentDidMount() {
        this.getData()
    }

    getData = () => {
        const { loading, keyword } = this.state
        if (!loading) {
            this.setState({ results: null })
            const query = {
                keyword,
                isActive: true
            }
            const params = {
                limit: 4,
            }
            Meteor.call('search.brands', {query, params}, (err, res) => {
                if (err) {
                    throw new Meteor.Error(err)
                }
                this.setState({ loading: false, results: res })
            })
        }
    }

    onChange = (e, { value }) => {
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }

        this.searchTimeout = setTimeout(() => {
            this.setState({ keyword: value })
        }, 400)
    }

    onKeyPress = (event) => {
        if (event.which === 13) {
            this.startSearch()
        } else {
            const { keyword } = this.state
            if (!keyword) {
                this.setState({ loading: false, results: null })
            }
        }
    }

    startSearch = (event) => {
        event && event.stopPropagation()
        if (this.getDataTimeout) { clearTimeout(this.getDataTimeout) }
        this.setState({ loading: true })

        this.getDataTimeout = setTimeout(() => {
            this.getData()
        }, 500)
    }

    renderResults = (results = {}) => {
        const { brands = [] } = results
        return (
            <Segment basic>
                <BrandsCards itemsPerRow={4} items={brands} />
                <Divider hidden />
                <Button fluid primary size="small" as={Link} to="/brands" content={i18n.__("All Brands")} />
            </Segment>
        )
    }

    render() {
        const { loading, results } = this.state

        return (
            <Card fluid className="action-field">
                <Card.Header>
                    <Input fluid size="large" icon={<Icon name={loading ? 'spinner' : 'trademark'} loading={loading} inverted circular onClick={this.startSearch} />} placeholder={i18n.__("Brands search")} onChange={this.onChange} onKeyPress={this.onKeyPress} />
                </Card.Header>
                {loading
                    ? <DataLoader />
                    : !isEmpty(results) ? (
                        <Card.Content>
                            {this.renderResults(results)}
                        </Card.Content>
                    ) : null
                }
            </Card>
        )
    }
}

export default BrandsDialog
