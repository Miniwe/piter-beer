import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
    Grid, Card, Input, Icon, Button, List, Segment, Header,
} from 'semantic-ui-react'
import { connect } from 'react-redux'
import isEmpty from 'lodash/isEmpty'
import NoItems from '../NoItems'
import { setFilter, setSearchMode } from '/imports/redux/actions'

class SearchDialog extends Component {
    state = {
        loading: false,
        keyword: '',
        results: null,
    }

    getData = () => {
        const { loading, keyword } = this.state
        if (!loading && keyword) {
            this.setState({ loading: true, results: null })
            const query = {
                keyword,
            }
            const params = {
                limit: 12,
                fields: {
                    alias: 1,
                    title: 1,
                }
            }
            Meteor.call('search.global', {query, params}, (err, res) => {
                if (err) {
                    throw new Meteor.Error(err)
                }
                this.setState({ loading: false, results: res })
            })
        } else if (!keyword) {
            this.setState({ loading: false, results: null })
        }
    }

    onChange = (e, { value }) => {
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }

        this.searchTimeout = setTimeout(() => {
            this.setState({ keyword: value })
            this.startSearch()
        }, 400)
    }

    onKeyPress = (event) => {
        if (event.which === 13) {
            this.startSearch()
        } else {
            const { keyword } = this.state
            if (!keyword) {
                this.setState({ loading: false, results: null })
            }
        }
    }

    startSearch = (event) => {
        event && event.stopPropagation()
        if (this.getDataTimeout) { clearTimeout(this.getDataTimeout) }

        this.getDataTimeout = setTimeout(() => {
            this.getData()
        }, 500)
    }

    renderResults = (results = {}) => {
        const { beers = [], locations = [], brands = [] } = results
        const hasResults = beers.length || locations.length || brands.length
        return (
            <Grid>
                <Grid.Column>
                {beers.length ? (
                    <Segment basic>
                        <Header as='h3'>
                            <Icon name='bar' />
                            <Header.Content as={Link} to='/beers'>{i18n.__('Beers')}</Header.Content>
                        </Header>
                        <List horizontal>
                            {beers.map(({ _id, title, alias }) => (
                                <List.Item key={_id} as={Link} to={`/beers/${alias}`}>{title}</List.Item>
                            ))}
                        </List>
                    </Segment>
                ) : null}
                {brands.length ? (
                    <Segment basic>
                        <Header as='h3'>
                            <Icon name='trademark' />
                            <Header.Content as={Link} to='/brands'>{i18n.__('Brands')}</Header.Content>
                        </Header>
                        <List horizontal>
                            {brands.map(({ _id, title, alias }) => (
                                <List.Item key={_id} as={Link} to={`/brands/${alias}`}>{title}</List.Item>
                            ))}
                        </List>
                    </Segment>
                ) : null}
                {locations.length ? (
                    <Segment basic>
                        <Header as='h3'>
                            <Icon name='map marker alternate' />
                            <Header.Content as={Link} to='/locations'>{i18n.__('Locations')}</Header.Content>
                        </Header>
                        <List horizontal>
                            {locations.map(({ _id, title, alias }) => (
                                <List.Item key={_id} as={Link} to={`/locations/${alias}`}>{title}</List.Item>
                            ))}
                        </List>
                    </Segment>
                ) : null}
                {/* hasResults ? <Button primary size="small" as={Link} to="/search" content="All Results" /> : <NoItems /> */}
                </Grid.Column>
            </Grid>
        )
    }

    render() {
        const { setSearchMode } = this.props
        const { loading, results } = this.state

        return (
            <Card fluid className="action-field">
                <Card.Header>
                    <Input
                        fluid
                        size="large"
                        icon={(
                            <div style={{ position: 'absolute', right: 0, top: '0.4rem' }}>
                                <Icon name={loading ? 'spinner' : 'search'} loading={loading} inverted circular onClick={this.startSearch} />
                                <Icon name="chevron right" color="green" inverted link circular onClick={event => setSearchMode(event, { name: 'beers' })} />
                            </div>
                        )}
                        placeholder={i18n.__('Search')}
                        onChange={this.onChange} onKeyPress={this.onKeyPress}
                    />
                </Card.Header>
                {!loading && !isEmpty(results) ? (
                    <Card.Content>
                        {this.renderResults(results)}
                    </Card.Content>
                ) : null}
            </Card>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    setSearchMode(event, { name }) {
        event.preventDefault()
        dispatch(setSearchMode(name))
    },
})

export default connect(null, mapDispatchToProps)(SearchDialog)
