import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import isEqual from 'lodash/isEqual'
import { connect } from 'react-redux'
import {
    Grid, Card, Input, Icon, Button, Divider,
} from 'semantic-ui-react'
import LocationsCards from '/imports/ui/locations/components/LocationsCards'
import LocationsMap from '/imports/ui/locations/components/LocationsMap'
import Filters from '../Filters'
import { setFilter, setSearchMode } from '/imports/redux/actions'
import { isMobile } from 'meteor/mcore/utils'
import DataLoader from '../DataLoader'

class LocationsDialog extends Component {
    state = {
        loading: false,
        items: [],
        mode: 'cards'
    }

    componentDidMount() {
        this.startSearch()
        this.rowCount = isMobile() ? 8 : 12
    }

    componentDidUpdate(prevProps) {
        if (!isEqual(prevProps.filters, this.props.filters)) {
            this.startSearch()
        }
    }

    toggleMode = () => {
        const { mode } = this.state
        this.setState({
            mode: mode === 'map' ? 'cards' : 'map'
        })
    }

    getData = () => {
        const { filters: { keyword, ...attributes } } = this.props
        const query = {
            keyword,
            attributes,
            isActive: true
        }
        const params = {
            limit: this.rowCount
        }

        Meteor.call('search.locations', { query, params }, (err, res) => {
            if (err) {
                throw new Meteor.Error(err)
            }
            const { locations } = res
            this.setState({
                items: locations,
                loading: false
            })

        })
    }

    onChange = (e, { value }) => {
        const { setFilter } = this.props
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }

        this.searchTimeout = setTimeout(() => {
            setFilter({ keyword: value })
        }, 400)
    }

    startSearch = () => {
        if (this.searchTimeout) { clearTimeout(this.searchTimeout) }
        this.setState({ loading: true })

        this.searchTimeout = setTimeout(() => {
            this.getData()
        }, 400)
    }

    render() {
        const { mode, loading, items = [] } = this.state
        const { setSearchMode, filters: { keyword } = {} } = this.props

        return (
            <Card fluid className="action-field">
                <Card.Header>
                    <Input fluid size="large" icon={(
                        <div style={{ position: 'absolute', right: 0, top: '0.4rem' }}>
                            <Icon name={loading ? 'spinner' : 'map marker alternate'} loading={loading} link circular onClick={this.startSearch} />
                            <Icon name="chevron right" color="green" inverted link circular onClick={event => setSearchMode(event, { name: 'search' })} />
                        </div>
                    )} placeholder={i18n.__("Locations search")} onChange={this.onChange} onKeyPress={this.onKeyPress} defaultValue={keyword} />
                </Card.Header>

                <Card.Content>
                    <Grid stackable>
                        {!isMobile() ? (
                            <Grid.Column width={4} className="filtersCol">
                                <Filters group="locations" />
                                <Divider hidden />
                                <Button fluid color="yellow" active={mode === 'map'} icon="map marker alternate" size="small" content={i18n.__(mode === 'map' ? 'Hide Map' : 'Show Map')} onClick={this.toggleMode} />
                                <Button fluid primary size="small" as={Link} to="/locations" content={i18n.__("All Locations")} />
                            </Grid.Column>
                        ) : null}
                        <Grid.Column width={12}>
                            {loading
                                ? <DataLoader />
                                : mode === 'map'
                                    ? <LocationsMap height={300} items={items} />
                                    : <LocationsCards items={items} itemsPerRow="4" className="no-description" mode="dark" />
                            }
                            {isMobile() ? (
                                <Fragment>
                                    <Divider hidden />
                                    <Button fluid primary size="small" as={Link} to="/locations" content={i18n.__("All Locations")} />
                                </Fragment>
                            ) : null}
                        </Grid.Column>
                    </Grid>
                </Card.Content>
            </Card>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    setFilter(filter) { dispatch(setFilter('locationsList', filter)) },
    setSearchMode(event, { name }) {
        event.preventDefault()
        dispatch(setSearchMode(name))
    },
})

const mapStateToProps = ({ app: { filters: { locationsList } } }) => ({
    filters: locationsList,
})

export default connect(mapStateToProps, mapDispatchToProps)(LocationsDialog)
