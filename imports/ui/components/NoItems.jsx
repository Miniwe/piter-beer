import React from 'react'
import { Message } from 'semantic-ui-react'

const NoItems = ({ message = i18n.__('No Items') }) => (
    <Message color="red" className="animated fadeIn">
        {message}
    </Message>
)

export default NoItems
