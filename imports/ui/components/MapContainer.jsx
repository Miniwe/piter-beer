import { Meteor } from 'meteor/meteor'
import React from 'react'
import isEqual from 'lodash/isEqual'
import { compose, withProps, lifecycle } from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from 'react-google-maps'
import {MAP_STYLE} from '/imports/configs/constants'
import { MarkerWithLabel } from 'react-google-maps/lib/components/addons/MarkerWithLabel'

const {googleMapKey} = Meteor.settings.public

const MapContainer = compose(
    withProps(({ height = '250px', ...ownerProps }) => ({
        googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${googleMapKey}&v=3.exp&libraries=geometry,drawing,places`,
        loadingElement: <div style={{ width: '100%', height }} />,
        containerElement: <div style={{ width: '100%', height }} />,
        mapElement: <div style={{ width: '100%', height }} />,
    })),
    lifecycle({
        componentDidMount() {
            const refs = {}
            const { value, geoposition } = this.props
            // if (window && window.google)
            {
                this.setState({
                    bounds: null,
                    zoom: value.geolocation.zoom,
                    center: value.geolocation.center,
                    markers: this.props.markers,
                    directions: this.props.directions,
                    onMapMounted: ref => {
                        refs.map = ref
                        if (geoposition) {
                            const DirectionsService = new google.maps.DirectionsService()
                            const origin = new google.maps.LatLng(geoposition.coords.latitude, geoposition.coords.longitude)
                            const destination = new google.maps.LatLng(value.geolocation.center.lat, value.geolocation.center.lng)
                            DirectionsService.route({
                                origin,
                                destination,
                                travelMode: google.maps.TravelMode.DRIVING,
                            }, (result, status) => {
                                if (status === google.maps.DirectionsStatus.OK) {
                                    this.setState({
                                        directions: result,
                                    })
                                } else {
                                    throw new Meteor.Error('no route', result)
                                }
                            })
                        }

                    }
                })
            }
        },
        componentDidUpdate(prevProps, prevState) {
            const { value = { label: '', geolocation: {} }, } = this.props
            if (!prevState || !isEqual(prevState.markers, this.state.markers)) {
                this.setState({
                    zoom: value.geolocation.zoom,
                    center: value.geolocation.center,
                    markers: this.props.markers,
                })
            }
        },
    }),
    withScriptjs,
    withGoogleMap,
)((props) => {
    const {
        value = { label: '', geolocation: {} },
        hideMap = false
    } = props
    return !hideMap ? (
        <GoogleMap
            ref={props.onMapMounted}
            defaultZoom={props.zoom}
            center={props.center}
            defaultOptions={{ styles: MAP_STYLE }}
            onBoundsChanged={props.onBoundsChanged}
        >
            {props.directions
                ? <DirectionsRenderer directions={props.directions} />
                : props.markers.map((marker, index) => (
                    <MarkerWithLabel key={index}
                        position={marker.position}
                        labelAnchor={new google.maps.Point(0, 0)}
                        labelStyle={{ backgroundColor: '#fadc44', fontSize: '0.8rem', fontWeight: '300', padding: '.4rem' }}
                    >
                        <div>{marker.label}</div>
                    </MarkerWithLabel>
                ))
            }
        </GoogleMap>
    ) : null
})

MapContainer.defaultProps = {
    value: {
        label: '',
        geolocation: {
            zoom: 15,
            center: { lat: 41.9, lng: -87.624 }
        }
    },
    markers: []
}
export default MapContainer
