import { Meteor } from 'meteor/meteor'
import { onPageLoad } from 'meteor/server-render'
import {
    noquotes,
    stripTags
} from 'meteor/mcore/utils'
import Pages from 'meteor/mcore/api/pages/services'
import Beers from '/imports/api/beers/services'
import Locations from '/imports/api/locations/services'
import Brands from '/imports/api/brands/services'


const { domain, siteName } = Meteor.settings.public

const renderHead = ({
    title = siteName,
    description = 'dev site',
    keywords = '',
    url = ''
}) => {
    return `
        <title>${noquotes(title)} | ${noquotes(siteName)}</title>
        <meta name="description" content="${noquotes(description)}" />
        <meta name="keywords" content="${noquotes(keywords)}" />
        <meta property="og:description" content="${noquotes(description)}" />
        <meta property="og:title" content="${noquotes(title)}" />
        <meta property="og:url" content="${url}" />
        <link rel="canonical" href="${url}" />
    `
}


onPageLoad((sink) => {
    let pageData = `Server time: ${new Date}`
    let headData = {}

    const renderPage = (url) => {
        let pageData = ''
        const page = Pages.ListServices.getPage(url)
        if (page) {
            headData = Pages.ListServices.getHeadData(path, page)
            pageData += `<h1>${page.title} | ${siteName}</h1>`
            pageData += `<main>${stripTags(page.description)}<br />${stripTags(page.text)}</main>`
        } else {
            sink.setStatusCode(404)
        }
        return pageData
    }

    const path = sink.request.url.path.split('?').shift()

    const beerPath = path.match(new RegExp(/\/beers\/([_-\w]+)/))
    const brandsPath = path.match(new RegExp(/\/brands\/([_-\w]+)/))
    const locationsPath = path.match(new RegExp(/\/locations\/([_-\w]+)/))

    if (beerPath) {
        const model = Beers.ListServices.getModel(beerPath[1])
        if (model) {
            headData = Beers.ListServices.getHeadData(path, model)
            pageData += `<h1>${model.title} | Пиво | ${siteName}</h1>`
            pageData += `<main>${stripTags(model.description)}<br />${stripTags(model.overview)}<main>`
            pageData += `<div>${model.address ? Object.values(model.address).join(', ') : ''}<div>`
        } else {
            pageData += renderPage('/beers')
        }
    } else if (locationsPath) {
        const model = Locations.ListServices.getModel(locationsPath[1])
        if (model) {
            // const locationsList = ((model.locationsList || []).map(item => item.title)).join(', ')
            headData = Locations.ListServices.getHeadData(path, model)
            pageData += `<h1>${model.title} | Места.1 | ${siteName}</h1>`
            pageData += `<main>${stripTags(model.description)} <br /> ${stripTags(model.fullDescription)}<main>`
        } else {
            pageData += renderPage('/locations')
        }
    } else if (brandsPath) {
        const model = Brands.ListServices.getModel(brandsPath[1])
        if (model) {
            // const brandsList = ((model.brandsList || []).map(item => item.title)).join(', ')
            headData = Brands.ListServices.getHeadData(path, model)
            pageData += `<h1>${model.title} | Марки | ${siteName}</h1>`
            pageData += `<main>${stripTags(model.description)}<main>`
        } else {
            pageData += renderPage('/brands')
        }
    } else {
        pageData += renderPage(path)
    }
    pageData += `<a href="${domain}">${siteName}</a>`

    sink.appendToHead(renderHead(headData))
    sink.renderIntoElementById("server-render-target", pageData)
})
