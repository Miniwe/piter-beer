
import { Meteor } from 'meteor/meteor'
import { ValidatedMethod } from 'meteor/mdg:validated-method'
import { check } from 'meteor/check'
import { rateLimit } from 'meteor/mcore/utils'

import attributesFixtures from 'meteor/mcore/api/attributes/server/fixtures'
import beersFixtures from '../../api/beers/server/fixtures'
import locationsFixtures from '../../api/locations/server/fixtures'
import townsFixtures from '../../api/towns/server/fixtures'
import brandsFixtures from '../../api/brands/server/fixtures'
import tagsFixtures from '../../api/tags/server/fixtures'

// import settingsFixtures from '../../api/settings/server/fixtures'
// import usersFixtures from '../../api/users/server/fixtures'


Meteor.startup(() => {
    // attributesFixtures.fill(58)
    // brandsFixtures.fill(10)
    // tagsFixtures.fill(20)
    // townsFixtures.fill(33)
    // // settingsFixtures(1)
    // // usersFixtures(1)
    // locationsFixtures.fill(37)
    // beersFixtures.fill(54)
})

const resetTable = new ValidatedMethod({
    name: 'resetTable',
    validate({ table, count }) {
        check(table, String)
        check(count, Number)
    },
    run({ table, count }) {
        switch (table) {
        case 'beers':
            beersFixtures.fill(count)
            break
        case 'towns':
            townsFixtures.fill(count)
            break
        case 'tags':
            tagsFixtures.fill(count)
            break
        case 'attributes':
            attributesFixtures.fill(count)
            break
        case 'brands':
            brandsFixtures.fill(count)
            break
        case 'locations':
            locationsFixtures(count)
            break
    //     // case 'users':
    //     //     usersFixtures(count)
    //     //     break
    //     // case 'settings':
    //     //     settingsFixtures(count)
    //     //     break
        default:
            throw new Meteor.Error('resetTable', 'Not defined table')
        }
    }
})

rateLimit({
    methods: [
        resetTable,
    ],
    limit: 5,
    timeRange: 1000
})
