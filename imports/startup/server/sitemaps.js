// import sitemaps from 'meteor/gadicohen:sitemaps'
import times from 'lodash/times'
import Beers from '/imports/api/beers'
import Brands from '/imports/api/brands'
import Locations from '/imports/api/locations'

sitemaps.config('gzip', true) // default to false

const SITEMAP_COUNT = Meteor.settings.public.sitemapCount

const collectionList = (collection, path) => collection
    .find({ isActive: true }, {
        fields: { _id: 1, alias: 1, updatedAt: 1 },
        sort: { updatedAt: -1 },
    })
    .fetch()
    .map(({ alias, updatedAt }) => ({
        page: `/${path}/${alias}`,
        lastmod: (new Date(updatedAt) || new Date()).toISOString()
    }))

const getPages = () => [
    ...collectionList(Beers, 'beers'),
    ...collectionList(Brands, 'brands'),
    ...collectionList(Locations, 'locations'),
]

const getCounts = () => Math.ceil(
    (Beers.find({
            isActive: true
        }).count() +
        Brands.find({
            isActive: true
        }).count() +
        Locations.find({
            isActive: true
        }).count()) / SITEMAP_COUNT)

const pages = getPages()

for (let index = 0; index < pages.length / SITEMAP_COUNT; index++) {
    sitemaps.add(`/sitemap-${index}.xml`, [...pages].splice(index * SITEMAP_COUNT, SITEMAP_COUNT))
}


WebApp.connectHandlers.use('/sitemap.xml', (req, res, next) => {
    res.writeHead(200, {
        'Content-Type': 'application/xml; charset=UTF-8',
    })
    let data = '<?xml version="1.0" encoding="UTF-8"?>\n<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n'

    const date = new Date().toISOString()
    times(getCounts(), (index) => {
        data += `<sitemap><loc>${Meteor.settings.public.domain}/sitemap-${index}.xml</loc><lastmod>${date}</lastmod></sitemap>\n`
    })

    data += '</sitemapindex>\n'
    res.end(data)
})
