import { Meteor } from 'meteor/meteor'
import { SyncedCron } from 'meteor/littledata:synced-cron'
import '../../configs/accounts'
import '../../api/server'
import './fixtures'
import './browser-policy'
import './sitemaps'
import './server-render'
// import {
//     CountsServices
// } from '../../api/admin/services'
// import './migrations'

Meteor.startup(() => {
    // CountsServices.register()
    SyncedCron.start()
})
