import { Migrations } from 'meteor/percolate:migrations'

import './1'

Migrations.config({
    log: true, // Log job run details to console
    logger: null, // Use a custom logger function (defaults to Meteor's logging package)
    logIfLatest: true, // Enable/disable logging "Not migrating, already at version {number}"
    collectionName: 'migrations', // migrations collection name to use in the database
})

Meteor.startup(() => {
    Migrations.migrateTo('1,rerun')
})
