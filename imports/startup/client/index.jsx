import React from 'react'
import { Meteor } from 'meteor/meteor'
import { render } from 'react-dom'
import i18n from 'meteor/universe:i18n'
import '../../configs/accounts'
import App from '/imports/ui/App'
import { ruLocale, enLocale } from '/both/i18n'

i18n.addTranslations('ru-RU', ruLocale)
i18n.addTranslations('en-US', enLocale)
i18n.setLocale('ru-RU')

Meteor.startup(() => {
    render(<App />, document.getElementById('react-root'))
})
