import React from 'react'
import {
    applyMiddleware,
    compose,
    createStore
} from 'redux'
import { animateScroll as scroll } from 'react-scroll'
import throttle from 'lodash/throttle'
import { createBrowserHistory } from 'history'
import { routerMiddleware } from 'connected-react-router'
// import thunk from 'redux-thunk'
// import promise from 'redux-promise'
// import { createLogger } from 'redux-logger'
import { loadState, saveState } from './localStorage'
import createRootReducer from '../reducers'

const history = createBrowserHistory()
history.listen(() => {
    scroll.scrollToTop()
})
const persistedState = loadState()

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose

// const logger = Meteor.isDevelopment ? createLogger() : null
const store = createStore(
    createRootReducer(history), // new root reducer with router state
    persistedState,
    composeEnhancers(
        applyMiddleware(
            routerMiddleware(history), // for dispatching history actions
            // thunk,
            // promise,
            // logger
        )
    ),
)

store.subscribe(throttle(() => {
    saveState(store.getState())
}), 1000)

export {
    history
}

export default store
