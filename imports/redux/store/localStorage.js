import APP_INITIAL from '../reducers/app_initial'

const stateKey = 'piter-beer-state'

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem(stateKey)
        if (serializedState === null) {
            return {
                app: APP_INITIAL
            }
        }
        return {
            app: {
                ...JSON.parse(serializedState),
                pages: {
                    beersList: 0,
                    locationsList: 0,
                    brandsList: 0,
                },
                filters: {
                    globalList: {},
                    beersList: {},
                    locationsList: {},
                    brandsList: {},
                },
                searchMode: 'beers',
                activeSlide: 0,
            },
        }
    } catch (err) {
        throw new Meteor.Error(err)
    }
}

export const saveState = (state) => {
    try {
        const { app } = state
        const serializedState = JSON.stringify(app)
        localStorage.setItem(stateKey, serializedState)
    } catch (err) {
        // No write errors
    }
}
