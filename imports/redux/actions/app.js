import {
    CURRENT_USER,
    HANDLE_PAGE,
    HANDLE_VIEW,
    COMPARE_BEER,
    CLEAR_COMPARED,
    OPEN_MODAL,
    CLOSE_MODAL,
    SEARCH_MODE,
    SET_FILTER,
    SET_FILTERS,
    UNSET_FILTER,
    RESET_FILTERS,
    ACTIVE_SLIDE,
} from '../constants'

export function setCurrentUser(currentUser = null) {
    return {
        type: CURRENT_USER,
        payload: { currentUser },
    }
}

export function handleView(key, mode) {
    return {
        type: HANDLE_VIEW,
        payload: { key, mode },
    }
}

export function handlePage(key, page) {
    return {
        type: HANDLE_PAGE,
        payload: { key, page },
    }
}

export function compareBeer(beerId) {
    return {
        type: COMPARE_BEER,
        payload: { beerId },
    }
}

export function clearCompared() {
    return {
        type: CLEAR_COMPARED,
        payload: { },
    }
}

export function setSearchMode(searchMode) {
    return {
        type: SEARCH_MODE,
        payload: { searchMode },
    }
}

export function setFilter(key, filter) {
    return {
        type: SET_FILTER,
        payload: { key, filter },
    }
}

export function setFilters(group, filters) {
    return {
        type: SET_FILTERS,
        payload: { group, filters },
    }
}

export function setActiveSlide(activeSlide) {
    return {
        type: ACTIVE_SLIDE,
        payload: { activeSlide },
    }
}

export function unsetFilter(key, filter) {
    return {
        type: UNSET_FILTER,
        payload: { key, filter },
    }
}

export function openModal(url) {
    const [key, model] = url.split('/').filter(item => item)
    return {
        type: OPEN_MODAL,
        payload: { key, model },
    }
}

export function closeModal() {
    return {
        type: CLOSE_MODAL
    }
}

export function resetFilters(key) {
    return {
        type: RESET_FILTERS,
        payload: { key },
    }
}
