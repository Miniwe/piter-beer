import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import app from './app'
import core from './core'

export default history => combineReducers({
    router: connectRouter(history),
    app,
    core,
})
