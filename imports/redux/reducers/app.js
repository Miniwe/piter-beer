import React from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import {
    CURRENT_USER,
    HANDLE_VIEW,
    HANDLE_PAGE,
    COMPARE_BEER,
    SEARCH_MODE,
    SET_FILTER,
    SET_FILTERS,
    ACTIVE_SLIDE,
    UNSET_FILTER,
    CLEAR_COMPARED,
    RESET_FILTERS,
    OPEN_MODAL,
    CLOSE_MODAL,
} from '../constants'
import APP_INITIAL from './app_initial'

const app = (state = APP_INITIAL, action) => {
    switch (action.type) {
    case CURRENT_USER: {
        return {
            ...state,
            ...action.payload,
        }
    }
    case HANDLE_VIEW: {
        const { key, mode } = action.payload
        const { views } = state
        return {
            ...state,
            views: Object.assign(views, {
                [key]: mode
            }),
        }
    }
    case HANDLE_PAGE: {
        const { key, page } = action.payload
        const { pages } = state
        return {
            ...state,
            pages: Object.assign(pages, {
                [key]: page,
            }),
        }
    }
    case SET_FILTER: {
        const { key, filter } = action.payload
        const { filters } = state
        const newFilters = Object.assign(filters, {
            [key]: {
                ...filters[key],
                ...filter
            }
        })
        return {
            ...state,
            filters: newFilters,
        }
    }
    case SET_FILTERS: {
        const { group, filters } = action.payload
        const newFilters = state.filters
        newFilters[group] = filters
        return {
            ...state,
            filters: newFilters,
        }
    }
    case UNSET_FILTER: {
        const { filters } = state
        const { key, filter } = action.payload
        const newFilters = Object.assign({}, filters)
        delete newFilters[key][filter]
        return {
            ...state,
            filters: newFilters,
        }
    }
    case RESET_FILTERS: {
        const { filters } = state
        const { key } = action.payload
        filters[key] = APP_INITIAL.filters[key]
        return {
            ...state,
            filters,
        }
    }
    case COMPARE_BEER: {
        let { comparedBeers } = state
        const { beerId } = action.payload
        const beerIndex = comparedBeers.indexOf(beerId)
        let toastMessage
        if (beerIndex > -1) {
            comparedBeers = [
                ...comparedBeers.slice(0, beerIndex),
                ...comparedBeers.slice(1 + beerIndex),
            ]
        } else {
            comparedBeers.push(beerId)
        }
        toast.dismiss()
        comparedBeers.length && toast.info(
            <Link to="/compare" className="go-compare">
                {i18n.__('GoCompare')}
            </Link>
        )
        return {
            ...state,
            comparedBeers,
            toastMessage,
        }
    }
    case CLEAR_COMPARED: {
        return {
            ...state,
            comparedBeers: []
        }
    }
    case SEARCH_MODE: {
        const { searchMode } = action.payload
        return {
            ...state,
            searchMode,
        }
    }
    case ACTIVE_SLIDE: {
        const { activeSlide } = action.payload
        return {
            ...state,
            activeSlide,
        }
    }
    case OPEN_MODAL: {
        const { key, model } = action.payload
        return {
            ...state,
            modal: {
                key,
                model
            },
        }
    }
    case CLOSE_MODAL: {
        return {
            ...state,
            modal: false,
        }
    }
    default:
        return state
    }
}

export default app
