import CORE_INITIAL from './core_initial'

const core = (state = CORE_INITIAL, action) => {
    switch (action.type) {
    default:
        return state
    }
}

export default core
