export default {
    currentUser: null,
    toastMessage: null,
    activeSlide: 0,
    modal: false,
    views: {
        beersList: 'grid',
        locationsList: 'grid',
        brandsList: 'grid',
        newsList: 'grid',
    },
    pages: {
        beersList: 0,
        locationsList: 0,
        brandsList: 0,
        newsList: 0,
    },
    filters: {
        globalList: {},
        beersList: {},
        locationsList: {},
        brandsList: {},
        newsList: {},
    },
    comparedBeers: [],
    searchMode: 'beers',
}
