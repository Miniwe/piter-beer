import * as collections from '../../api/collections'
// import Files from '../../api/files'
import { PAGE_SIZE } from '../../configs/constants'

export default {
    pageSize: PAGE_SIZE,
    collections,
    actions: [
        {
            key: 'updateCounters',
            title: 'UpdateCounters',
            icon: 'check'
        },
        // {
        //     key: 'updateSitemap',
        //     title: 'UpdateSitemap',
        //     icon: 'globe'
        // },
        {
            key: 'updateOrders',
            title: 'UpdateOrders',
            icon: 'sort amount down'
        },
        // {
        //     key: 'updateColors',
        //     title: 'UpdateColors',
        //     icon: 'image'
        // },
        // {
        //     key: 'updateSitemap',
        //     title: 'UpdateSitemap',
        //     icon: 'sitemap'
        // }
    ]
}
