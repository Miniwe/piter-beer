import {
    Meteor
} from 'meteor/meteor'

export const PAGE_SIZE = 20
export const TRESHOLD = 480

export const mobileBreakpoint = 320
export const tabletBreakpoint = 768
export const computerBreakpoint = 992
export const largeMonitorBreakpoint = 1200
export const widescreenMonitorBreakpoint = 1920

export const largestMobileScreen = tabletBreakpoint - 1
export const largestTabletScreen = computerBreakpoint - 1
export const largestSmallMonitor = largeMonitorBreakpoint - 1
export const largestLargeMonitor = widescreenMonitorBreakpoint - 1

export const ATTRS_GROUP = {
    beers: 'Beers',
    brands: 'Brands',
    locations: 'Locations',
}

const PLACEHOLDERS = [
    'Letter Header',
    'Letter Footer',
    'User Name',
    'Last Posts',
    'Unsubscribe Link'
]

export const EDITOR_CONFIG = {
    toolbarCanCollapse: false,
    resize_enabled: true,
    customConfig: '/ckeditor/custom.js',
    extraPlugins: 'imageuploader,pagebreak,placeholder_select',
    toolbar: [{
            name: 'tools',
            items: ['Maximize', 'ShowBlocks']
        },
        {
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup'],
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
        },
        {
            name: 'colors',
            items: ['TextColor', 'BGColor']
        },
        {
            name: 'styles',
            items: ['Styles', 'Format', 'Font', 'FontSize']
        },
        // { name: 'editing', groups: ['find', 'selection', 'spellchecker'], items: ['Find', 'Replace', '-', 'SelectAll', '-', 'jQuerySpellChecker'] },
        {
            name: 'insert',
            items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']
        },
        '/',
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
        },
        {
            name: 'links',
            items: ['Link', 'Unlink', 'Anchor']
        },
        {
            name: 'clipboard',
            groups: ['clipboard', 'undo'],
            items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']
        },
        {
            name: 'others',
            items: ['placeholder_select', '-', 'Source']
        },
        // { name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
        // { name: 'about', items: ['About'] }
    ],
    toolbarGroups: [{
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup']
        },
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align']
        },
        {
            name: 'styles'
        },
        {
            name: 'colors'
        },
        {
            name: 'clipboard',
            groups: ['clipboard', 'undo']
        },
        // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
        {
            name: 'links'
        },
        {
            name: 'insert'
        },
        {
            name: 'forms'
        },
        {
            name: 'tools'
        },
        // { name: 'document', groups: ['mode', 'document', 'doctools'] },
        {
            name: 'others'
        },
        '/',
        {
            name: 'about'
        }
    ],
    placeholder_select: {
        placeholders: PLACEHOLDERS,
        format: '%%placeholder%%'
    },
    removeButtons: ''
}

export const SHORT_EDITOR_CONFIG = {
    toolbar: [{
            name: 'tools',
            items: ['Maximize', 'ShowBlocks']
        },
        {
            name: 'basicstyles',
            groups: ['basicstyles', 'cleanup'],
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']
        },
        {
            name: 'colors',
            items: ['TextColor']
        },
        {
            name: 'insert',
            items: ['Image', 'Table', 'HorizontalRule', 'Smiley']
        },
        {
            name: 'paragraph',
            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
        },
        {
            name: 'links',
            items: ['Link', 'Unlink']
        },
        {
            name: 'others',
            items: ['placeholder_select', '-', 'Source']
        },
    ],
    toolbarGroups: [{
        name: 'oneline',
        groups: [
            'tools',
            'basicstyles',
            'colors',
            'insert',
            'paragraph',
            'links',
            'others',
        ]
    }],
}

const MAP_STYLE = [{
    "featureType": "landscape.natural",
    "elementType": "geometry.fill",
    "stylers": [{
        "visibility": "on"
    }, {
        "color": "#e0efef"
    }]
}, {
    "featureType": "poi",
    "elementType": "geometry.fill",
    "stylers": [{
        "visibility": "on"
    }, {
        "hue": "#1900ff"
    }, {
        "color": "#c0e8e8"
    }]
}, {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [{
        "lightness": 100
    }, {
        "visibility": "simplified"
    }]
}, {
    "featureType": "road",
    "elementType": "labels",
    "stylers": [{
        "visibility": "off"
    }]
}, {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [{
        "visibility": "on"
    }, {
        "lightness": 700
    }]
}, {
    "featureType": "water",
    "elementType": "all",
    "stylers": [{
        "color": "#65a4d7"
    }]
}]

export const pageSwiperParams = {
    direction: 'horizontal',
    slidesPerView: 5,
    slidesPerColumn: 3,
    containerClass: 'hSwiper',
    grabCursor: true,
    pagination: {
        el: '.swiper-pagination.swiper-pagination-v',
        dynamicBullets: true,
        clickable: true,
    },
    breakpoints: {
        425: {
            slidesPerView: 1,
            slidesPerColumn: 1,
        },
        768: {
            slidesPerView: 2,
            slidesPerColumn: 1,
        },
        1024: {
            slidesPerView: 3,
            slidesPerColumn: 2,
        },
        1440: {
            slidesPerView: 4,
            slidesPerColumn: 2,
        },
        1920: {
            slidesPerView: 5,
            slidesPerColumn: 3,
        },
    },
    ...(Meteor.isClient && (Meteor.isCordova || window.innerWidth < largestTabletScreen) ? {
        effect: 'coverflow',
        centeredSlides: true,
        coverflowEffect: {
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true
        }
    } : null)
}
export const siteSwiperParams = {
    direction: 'vertical',
    slidesPerView: 1,
    spaceBetween: 0,
    containerClass: 'pageSwiper',
    mousewheel: true,
    pagination: {
        el: '.swiper-pagination.swiper-pagination-1',
        clickable: true,
    },
}
export const imagesSwiperParams = {
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
    containerClass: 'imagesSwiper',
    mousewheel: false,
    centeredSlides: true,
    autoHeight: true,
    autoplay: {
        delay: 4000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination.swiper-pagination-images',
        clickable: true,
    },
}
