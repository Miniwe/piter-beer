import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/std:accounts-ui'
// import './email-templates'

Accounts.config({
  sendVerificationEmail: false,
  // forbidClientAccountCreation: false,
  restrictCreationByEmailDomain: ''
})

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL',
  loginPath: '/login',
  signUpPath: '/signup',
  resetPasswordPath: '/reset-password',
  profilePath: '/admin',
  homeRoutePath: '/',
  requestPermissions: {},
  requestOfflineToken: {},
  minimumPasswordLength: 6
})

if (Meteor.isServer) {
  Accounts.onCreateUser((options, user) => {
    user.profile = options.profile || {}
    // user.profile.username = user.profile.username.trim()
    user.roles = []
    return user
  })
}
